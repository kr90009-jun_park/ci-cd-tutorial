package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResendMailForCitizenInvitationCommand extends CqrsUserCommand {
    //
    private String citizenId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ResendMailForCitizenInvitationCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ResendMailForCitizenInvitationCommand.class);
    }

    public static ResendMailForCitizenInvitationCommand sample() {
        //
        ResendMailForCitizenInvitationCommand resendMailForCitizenInvitationCommand = new ResendMailForCitizenInvitationCommand("r2p8@nea-m5");
        resendMailForCitizenInvitationCommand.setRequester(Requester.sample());
        return resendMailForCitizenInvitationCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
