package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddJoinedMetroForActiveCitizenCommand extends CqrsUserCommand {
    //
    private String castellanId;
    private String joinedMetro;
    private IdName citizen;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static AddJoinedMetroForActiveCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AddJoinedMetroForActiveCitizenCommand.class);
    }

    public static AddJoinedMetroForActiveCitizenCommand sample() {
        //
        return sample(9);
    }

    public static AddJoinedMetroForActiveCitizenCommand sample(int sequence) {
        //
        AddJoinedMetroForActiveCitizenCommand addJoinedMetroForActiveCitizenCommand = new AddJoinedMetroForActiveCitizenCommand(
                UUID.randomUUID().toString(),
                "nea-m" + sequence,
                new IdName("r2p8@nea-m" + sequence, "Steve Jobs" + sequence)
        );
        addJoinedMetroForActiveCitizenCommand.setRequester(Requester.sample());
        return addJoinedMetroForActiveCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
