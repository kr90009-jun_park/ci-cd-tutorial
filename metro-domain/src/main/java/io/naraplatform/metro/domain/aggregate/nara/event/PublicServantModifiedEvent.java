package io.naraplatform.metro.domain.aggregate.nara.event;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublicServantModifiedEvent extends CqrsDomainEvent {
    //
    private String naraId;
    private NameValueList nameValues;

    public PublicServantModifiedEvent(IdName aggregate, CommandIdentity commandIdentity, String naraId, NameValueList nameValues) {
        //
        super(aggregate, commandIdentity);
        this.naraId = naraId;
        this.nameValues = nameValues;
    }
}
