package io.naraplatform.metro.domain.aggregate.citizen.querymodel;

import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.metro.domain.aggregate.OptionalSettings;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenLanguageSettings;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Base64;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCitizen extends DomainEntity {
    //
    private CitizenIdentity identity;
    private CitizenSettings settings;
    private boolean identityModified;
    private byte[] profilePhoto;

    protected AbstractCitizen(String id) {
        //
        super(id);
    }

    protected AbstractCitizen(CitizenIdentity identity) {
        //
        super(identity.getId());
        this.identity = identity;
    }

    protected AbstractCitizen(AbstractCitizen sourceCitizen) {
        //
        super(sourceCitizen);
        this.identity = sourceCitizen.getIdentity();
        this.settings = sourceCitizen.getSettings();
        this.profilePhoto = sourceCitizen.getProfilePhoto();
    }

    public void setValues(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "profilePhoto":
                    this.profilePhoto = Base64.getDecoder().decode(value);
                    break;
                case "usid":
                    identity.setUsid(value);
                    identityModified = true;
                    break;
                case "displayName":
                    identity.setDisplayName(value);
                    identityModified = true;
                    break;
                case "languageSettings":
                    settings.setLanguageSettings(CitizenLanguageSettings.fromJson(value));
                    break;
                case "optionalSettings":
                    settings.setOptionalSettings(OptionalSettings.fromJson(value));
                    break;
                default:
                    throw new NaraException(MetroExceptionMessage.cannotUpdateEntityField, this.getClass().getSimpleName(), nameValue.getName());
            }
        }
    }

    public String toString() {
        //
        return toJson();
    }
}
