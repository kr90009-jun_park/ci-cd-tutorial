package io.naraplatform.metro.domain.facade.client;

public interface NaraClient extends NaraClientCommandFacade, NaraClientQueryFacade {
    //
}
