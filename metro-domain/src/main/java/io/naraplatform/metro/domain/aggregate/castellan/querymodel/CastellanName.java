package io.naraplatform.metro.domain.aggregate.castellan.querymodel;

import io.naradrama.prologue.domain.lang.LangString;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.domain.granule.EmailList;
import io.naradrama.prologue.domain.patron.AudienceKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CastellanName extends DomainEntity {
    // Read model for an administrator
    private String displayName;
    private EmailList emails;
    private String castellanId;

    public CastellanName(String id) {
        //
        super(id);
    }

    public CastellanName(String displayName,
                         EmailList emails,
                         String castellanId) {
        //
        super();
        this.displayName = displayName;
        this.emails = emails;
        this.castellanId = castellanId;
    }

    public static List<CastellanName> newInstances(Castellan castellan, AudienceKey audienceKey) {
        //
        List<CastellanName> castellanNames = new ArrayList<>();
//        for (String name : castellan.getNames().getLangStringMap().values()) {
        for (LangString langString : castellan.getNames().getLangStrings()) {
            CastellanName castellanName = new CastellanName(
                    langString.getString(),
                    castellan.getEmails(),
                    castellan.getId()
            );
            castellanNames.add(castellanName);
        }

        return castellanNames;
    }

    public static List<CastellanName> samples() {
        //
        Castellan castellan = Castellan.sample();
        return newInstances(castellan, AudienceKey.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(samples());
    }
}
