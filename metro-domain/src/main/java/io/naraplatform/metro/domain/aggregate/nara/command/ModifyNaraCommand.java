package io.naraplatform.metro.domain.aggregate.nara.command;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ModifyNaraCommand extends CqrsUserCommand {
    //
    private String naraId;
    private NameValueList nameValues;

    public static ModifyNaraCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ModifyNaraCommand.class);
    }

    public static ModifyNaraCommand sample() {
        //
        Nara nara = Nara.sample();
        NameValueList nameValueList = NameValueList.newInstance("name", "Nextree Nara");
        ModifyNaraCommand modifyNaraCommand = new ModifyNaraCommand(nara.getId(), nameValueList);
        modifyNaraCommand.setRequester(Requester.sample());
        return modifyNaraCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
