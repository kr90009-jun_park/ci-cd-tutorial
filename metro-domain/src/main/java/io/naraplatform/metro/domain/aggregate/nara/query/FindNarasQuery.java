package io.naraplatform.metro.domain.aggregate.nara.query;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class FindNarasQuery extends CqrsUserQuery<List<Nara>> {
    //
}
