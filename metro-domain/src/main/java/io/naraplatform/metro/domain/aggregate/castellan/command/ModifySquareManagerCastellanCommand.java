package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ModifySquareManagerCastellanCommand extends CqrsUserCommand {
    //
    private String castellanId;
    private String squareId;
    private NameValueList nameValues;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ModifySquareManagerCastellanCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ModifySquareManagerCastellanCommand.class);
    }

    public static ModifySquareManagerCastellanCommand sample() {
        //
        NameValueList nameValueList = NameValueList.newInstance("names", LangString.sample().toJson());

        ModifySquareManagerCastellanCommand command = new ModifySquareManagerCastellanCommand(
                UUID.randomUUID().toString(),
                "nea",
                nameValueList
        );
        command.setRequester(Requester.sample());
        return command;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
