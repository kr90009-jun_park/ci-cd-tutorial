package io.naraplatform.metro.domain.aggregate.castellan.querymodel;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.domain.granule.Phone;
import io.naradrama.prologue.domain.granule.PhoneList;
import io.naradrama.prologue.domain.patron.AudienceKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CastellanPhone extends DomainEntity {
    //
    private String phone;           // index and query key, just phone number like 01091170989
    private String displayName;
    private String castellanId;

    public CastellanPhone(String id) {
        //
        super(id);
    }

    public CastellanPhone(AudienceKey audienceKey,
                          String phone,
                          String displayName,
                          String castellanId) {
        //
        super();
        this.phone = phone;
        this.displayName = displayName;
        this.castellanId = castellanId;
    }

    public static List<CastellanPhone> newInstances(Castellan castellan, AudienceKey audienceKey) {
        //
        List<CastellanPhone> castellanPhones = new ArrayList<>();
        PhoneList phones = castellan.getPhones();
        for(Phone phone : phones.list()) {
            String phoneString = phone.genFullNumber();
            CastellanPhone castellanPhone = new CastellanPhone(
                    audienceKey,
                    phoneString,
                    castellan.genDisplayName(),
                    castellan.getId()
            );
            castellanPhones.add(castellanPhone);
        }

        return castellanPhones;
    }

    public static List<CastellanPhone> samples() {
        //
        Castellan castellan = Castellan.sample();
        return newInstances(castellan, AudienceKey.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(samples());
    }
}
