package io.naraplatform.metro.domain.aggregate.castellan.query;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class FindCastellansQuery extends CqrsUserQuery<List<Castellan>> {
    //

}
