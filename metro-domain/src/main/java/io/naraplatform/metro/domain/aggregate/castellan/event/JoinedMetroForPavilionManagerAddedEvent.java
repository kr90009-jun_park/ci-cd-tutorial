package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinedMetroForPavilionManagerAddedEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private String joinedMetroId;
    private String citizenId;
    private String displayName;
    //private String email;
    //private String usid;

    public JoinedMetroForPavilionManagerAddedEvent(Castellan castellan, CommandIdentity commandIdentity, String joinedMetroId, String citizenId, String displayName) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.joinedMetroId = joinedMetroId;
        this.citizenId = citizenId;
        this.displayName = displayName;
    }
}
