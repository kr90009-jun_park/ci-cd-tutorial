package io.naraplatform.metro.domain.aggregate.castellan.entity;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.ddd.ValueObject;
import io.naraplatform.share.domain.tenant.CitizenKey;
import io.naraplatform.share.domain.tenant.MetroKey;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JoinedMetro implements ValueObject {
    //
    private String metroId;
    private IdName citizen;
    private long time;

    public JoinedMetro(String metroId, IdName citizen) {
        //
        this.metroId = metroId;
        this.citizen = citizen;
    }

    public String toString() {
        //
        return toJson();
    }

    public static JoinedMetro fromJson(String json) {
        //
        return JsonUtil.fromJson(json, JoinedMetro.class);
    }

    public static JoinedMetro sample() {
        //
        String metro = MetroKey.sample().getKeyString();
        IdName citizen = new IdName(CitizenKey.sample().getKeyString(), "Steve Jobs");

        return new JoinedMetro(
                metro,
                citizen,
                System.currentTimeMillis()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
