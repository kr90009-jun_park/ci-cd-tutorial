package io.naraplatform.metro.domain.aggregate.citizen.query;

import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.AbstractCitizen;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindCitizensByMetroIdAndStateQuery extends CqrsUserQuery<List<AbstractCitizen>> {
    //
    private String metroId;
    private CitizenState state;
}
