package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;


@Deprecated
@Getter
@Setter
public class ManagingWorkspaceAddedEvent extends CqrsDomainEvent {
    //
    private String castellanId;

    public ManagingWorkspaceAddedEvent(Castellan castellan, CommandIdentity commandIdentity
    ) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
    }
}
