package io.naraplatform.metro.domain.aggregate.citizen.event;

import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreliminaryCitizenActivatedEvent extends CqrsDomainEvent {
    //
    private String citizenId;
    private String encryptedPassword;

    public PreliminaryCitizenActivatedEvent(Citizen citizen, CommandIdentity commandIdentity, String encryptedPassword) {
        //
        super(citizen.genEntityIdName(), commandIdentity);
        this.citizenId = citizen.getId();
        this.encryptedPassword = encryptedPassword;
    }
}
