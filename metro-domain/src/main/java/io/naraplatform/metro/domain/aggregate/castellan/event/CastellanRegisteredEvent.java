package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CastellanRegisteredEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private String email;
    private String name;

    // Auto Setup
    private String encryptedPassword;

    public CastellanRegisteredEvent(Castellan castellan, CommandIdentity commandIdentity, String email,
                                    String name, String encryptedPassword) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.email = email;
        this.name = name;
        this.encryptedPassword = encryptedPassword;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }
}
