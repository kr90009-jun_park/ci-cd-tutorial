package io.naraplatform.metro.domain.aggregate.metro.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeactivateMetroCommand extends CqrsUserCommand {
    //
    private String metroId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static DeactivateMetroCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DeactivateMetroCommand.class);
    }

    public static DeactivateMetroCommand sample() {
        //
        DeactivateMetroCommand deactivateMetroCommand = new DeactivateMetroCommand("nea-m5");
        deactivateMetroCommand.setRequester(Requester.sample());
        return deactivateMetroCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
