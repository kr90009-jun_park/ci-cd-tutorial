package io.naraplatform.metro.domain.aggregate.citizen.entity;

import io.naradrama.prologue.domain.ddd.ValueObject;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmailVerifedInfo implements ValueObject {

    String sentEmail;
    String secretCode;
    boolean verified;

    public EmailVerifedInfo(String sentEmail, String secretCode) {
        //
        this.sentEmail = sentEmail;
        this.secretCode = secretCode;
        this.verified = false;
    }

    public String toString() {
        //
        return toJson();
    }

    public static EmailVerifedInfo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, EmailVerifedInfo.class);
    }

    public static EmailVerifedInfo sample() {
        //
        return new EmailVerifedInfo("jobs@nextree.io", "123456");
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
