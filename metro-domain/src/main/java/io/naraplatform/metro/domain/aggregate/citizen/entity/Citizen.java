package io.naraplatform.metro.domain.aggregate.citizen.entity;

import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.metro.domain.aggregate.OptionalSettings;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenLanguageSettings;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.domain.workspace.WorkspaceList;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import lombok.Getter;
import lombok.Setter;

import java.util.Base64;

@Getter
@Setter
public class Citizen extends DomainEntity implements DomainAggregate {
    //
    private CitizenIdentity identity;
    private CitizenSettings settings;
    private byte[] profilePhoto;
    private EmailVerifedInfo emailVerifedInfo;

    public Citizen(String id) {
        //
        super(id);
    }

    public Citizen(CitizenIdentity identity, CitizenSettings settings) {
        //
        super(identity.getId());
        this.identity = identity;
        this.settings = settings;
    }

    public void setValues(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "profilePhoto":
                    this.profilePhoto = Base64.getDecoder().decode(value);
                    break;
                case "usid":
                    identity.setUsid(value);
                    break;
                case "displayName":
                    identity.setDisplayName(value);
                    break;
                case "languageSettings":
                    settings.setLanguageSettings(CitizenLanguageSettings.fromJson(value));
                    break;
                case "optionalSettings":
                    settings.setOptionalSettings(OptionalSettings.fromJson(value));
                    break;

                default:
                    throw new NaraException(MetroExceptionMessage.cannotUpdateEntityField, this.getClass().getSimpleName(), nameValue.getName());
            }
        }
    }

    public static  Citizen sample() {
        //
        Citizen citizen = new Citizen(
                CitizenIdentity.sample(),
                CitizenSettings.sample());

        return citizen;
    }

    public static void main(String[] args) {
        //
        System.out.println(Citizen.sample());
    }
}
