package io.naraplatform.metro.domain.facade.aggregate.citizen;

import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.citizen.command.*;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="CitizenCommand",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CitizenCommandFacade {
    //
    ActivatePreliminaryCitizenCommand activatePreliminary(ActivatePreliminaryCitizenCommand activatePreliminaryCitizenCommand);
    ActivatePreliminaryCitizenByManagerCommand activatePreliminaryByManager(
            ActivatePreliminaryCitizenByManagerCommand activatePreliminaryCitizenByManagerCommand);
    ModifyCitizenCommand modifyCitizen(ModifyCitizenCommand modifyCitizenCommand);

    ActivateCitizenCommand activateCitizen(ActivateCitizenCommand activateCitizenCommand);
    DeactivateCitizenCommand deactivateCitizen(DeactivateCitizenCommand deactivateCitizenCommand);
    RemoveCitizenCommand removeCitizen(RemoveCitizenCommand removeCitizenCommand);

    ResendMailForCitizenInvitationCommand sendMailForInvitation(ResendMailForCitizenInvitationCommand resendMailForCitizenInvitationCommand);

    // flow
    RegisterCitizenCommand registerCitizen(RegisterCitizenCommand registerCitizenCommand);
}
