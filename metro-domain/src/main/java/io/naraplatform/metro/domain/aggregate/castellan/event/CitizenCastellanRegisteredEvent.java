package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CitizenCastellanRegisteredEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private String email;
    private String name;
    private String joinedMetro;
    private String citizenId;

    // Auto Setup
    private String encryptedPassword;
    private boolean activating;

    public CitizenCastellanRegisteredEvent(Castellan castellan, CommandIdentity commandIdentity, String email, String name, String joinedMetro, String citizenId, String encryptedPassword, boolean activating) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.email = email;
        this.name = name;
        this.joinedMetro = joinedMetro;
        this.citizenId = citizenId;
        this.encryptedPassword = encryptedPassword;
        this.activating = activating;
    }
}
