package io.naraplatform.metro.domain.aggregate.metro.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActivatePreliminaryMetroCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String secretCode;
    private String officialEmail;
    private String officialPassword;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ActivatePreliminaryMetroCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ActivatePreliminaryMetroCommand.class);
    }

    public static ActivatePreliminaryMetroCommand sample() {
        //
        ActivatePreliminaryMetroCommand activatePreliminaryMetroCommand = new ActivatePreliminaryMetroCommand(
                "nea-m5",
                "12345",
                "jobs@nextree.io",
                "password");
        activatePreliminaryMetroCommand.setRequester(Requester.sample());
        return activatePreliminaryMetroCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
