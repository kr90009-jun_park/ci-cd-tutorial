package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterPavilionManagerCastellanCommand extends CqrsUserCommand {
    //
    private String email;
    private LangString name;
    private String encryptedPassword;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterPavilionManagerCastellanCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterPavilionManagerCastellanCommand.class);
    }

    public static RegisterPavilionManagerCastellanCommand sample() {
        //
        return sample(5);
    }

    public static RegisterPavilionManagerCastellanCommand sample(int sequence) {
        //
        RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellanCommand = new RegisterPavilionManagerCastellanCommand(
                sequence + "jobs@nextree.io",
                LangString.newString("ko", "Mr Soo"),
                "$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei"
        );
        registerPavilionManagerCastellanCommand.setRequester(Requester.sample());
        return registerPavilionManagerCastellanCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
