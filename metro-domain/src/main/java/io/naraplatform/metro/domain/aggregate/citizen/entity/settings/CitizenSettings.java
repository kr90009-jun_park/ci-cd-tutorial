package io.naraplatform.metro.domain.aggregate.citizen.entity.settings;

import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.metro.domain.aggregate.OptionalSettings;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import io.naraplatform.share.domain.tenant.CitizenKey;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CitizenSettings extends DomainEntity {
    //
    private CitizenLanguageSettings languageSettings;
    private OptionalSettings optionalSettings;          // Nullable
    private long time;

    public CitizenSettings(String id) {
        //
        super(id);
    }

    public CitizenSettings(CitizenKey citizenKey,
                           CitizenLanguageSettings languageSettings) {
        //
        super(citizenKey.getKeyString());
        this.languageSettings = languageSettings;
        this.optionalSettings = new OptionalSettings();
        this.time = System.currentTimeMillis();
    }

    public String toString() {
        //
        return toJson();
    }

    public static CitizenSettings fromJson(String json) {
        //
        return JsonUtil.fromJson(json, CitizenSettings.class);
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
//                case "workspaces":
//                    this.workspaces = WorkspaceList.fromJson(value);
//                    break;
                case "languageSettings":
                    this.languageSettings = CitizenLanguageSettings.fromJson(value);
                    break;
                case "optionalSettings":
                    this.optionalSettings = OptionalSettings.fromJson(value);
                    break;

                default:
                    throw new NaraException(MetroExceptionMessage.cannotUpdateEntityField, this.getClass().getSimpleName(), nameValue.getName());
            }
        }
    }

    public static CitizenSettings sample() {
        //
        CitizenIdentity citizenIdentity = CitizenIdentity.sample();
        CitizenLanguageSettings languageSettings = CitizenLanguageSettings.sample();

        return new CitizenSettings(
                citizenIdentity.genKey(),
                languageSettings
        );
    }

    public static CitizenSettings sample2() {
        //
        CitizenIdentity citizenIdentity = CitizenIdentity.sample2();
        CitizenLanguageSettings languageSettings = CitizenLanguageSettings.sample2();

        return new CitizenSettings(
                citizenIdentity.genKey(),
                languageSettings
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
