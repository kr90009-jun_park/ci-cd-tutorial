package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ModifyMidtownMemberCastellanCommand extends CqrsUserCommand {
    //
    private String castellanId;
    private Member member;
    private String previousEmail;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ModifyMidtownMemberCastellanCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ModifyMidtownMemberCastellanCommand.class);
    }

    public static ModifyMidtownMemberCastellanCommand sample() {
        //
        return sample(1);
    }

    public static ModifyMidtownMemberCastellanCommand sample(int sequence) {
        //
        Member member = Member.sample();
        member.setEmail(sequence + "jobs@nextree.io");
        member.setName("ko", "Steve Jobs " + sequence);

        ModifyMidtownMemberCastellanCommand modifyMidtownMemberCastellanCommand = new ModifyMidtownMemberCastellanCommand(
                UUID.randomUUID().toString(),
                member,
                member.getEmail()
        );
        modifyMidtownMemberCastellanCommand.setRequester(Requester.sample());
        return modifyMidtownMemberCastellanCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
