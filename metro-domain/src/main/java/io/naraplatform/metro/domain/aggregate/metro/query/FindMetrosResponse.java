package io.naraplatform.metro.domain.aggregate.metro.query;

import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naradrama.prologue.util.json.JsonSerializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindMetrosResponse implements JsonSerializable {
    //
    List<Metro> metros;
}
