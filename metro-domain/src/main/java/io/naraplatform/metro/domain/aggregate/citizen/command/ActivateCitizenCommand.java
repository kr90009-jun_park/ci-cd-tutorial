package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActivateCitizenCommand extends CqrsUserCommand {
    //
    private String citizenId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ActivateCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ActivateCitizenCommand.class);
    }

    public static ActivateCitizenCommand sample() {
        //
        return sample(5);
    }

    public static ActivateCitizenCommand sample(int sequence) {
        //
        ActivateCitizenCommand activateCitizenCommand = new ActivateCitizenCommand("r2p8@nea-m" + sequence);
        activateCitizenCommand.setRequester(Requester.sample());
        return activateCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
