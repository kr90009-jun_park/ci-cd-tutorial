package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActivatePreliminaryCitizenByManagerCommand extends CqrsUserCommand {
    //
    private String citizenId;
    private String password;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ActivatePreliminaryCitizenByManagerCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ActivatePreliminaryCitizenByManagerCommand.class);
    }

    public static ActivatePreliminaryCitizenByManagerCommand sample() {
        //
        return sample(5);
    }

    public static ActivatePreliminaryCitizenByManagerCommand sample(int sequence) {
        //
        ActivatePreliminaryCitizenByManagerCommand command = new ActivatePreliminaryCitizenByManagerCommand(
                "r2p8@nea-m" + sequence,
                "$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei");
        command.setRequester(Requester.sample());
        return command;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
