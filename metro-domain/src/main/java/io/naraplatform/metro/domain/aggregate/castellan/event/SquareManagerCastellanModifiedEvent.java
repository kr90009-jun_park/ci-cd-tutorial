package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SquareManagerCastellanModifiedEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private String squareId;
    private NameValueList nameValues;

    public SquareManagerCastellanModifiedEvent(Castellan castellan, CommandIdentity commandIdentity, String squareId, NameValueList nameValues) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.squareId = squareId;
        this.nameValues = nameValues;
    }
}
