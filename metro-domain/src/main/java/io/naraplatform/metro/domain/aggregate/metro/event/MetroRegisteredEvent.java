package io.naraplatform.metro.domain.aggregate.metro.event;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetroRegisteredEvent extends CqrsDomainEvent {
    //
    private String metroId;
    private String name;
    private String officialId;

    public MetroRegisteredEvent(IdName aggregate, CommandIdentity commandIdentity, String metroId, String name, String officialId) {
        //
        super(aggregate, commandIdentity);
        this.metroId = metroId;
        this.name = name;
        this.officialId = officialId;
    }
}
