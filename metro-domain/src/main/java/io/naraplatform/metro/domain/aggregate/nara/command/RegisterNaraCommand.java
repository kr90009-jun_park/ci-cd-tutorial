package io.naraplatform.metro.domain.aggregate.nara.command;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naraplatform.share.domain.tenant.NaraType;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterNaraCommand extends CqrsUserCommand {
    //
    private String name;            // Identity
    private NaraType type;          // Identity

    private String zoneId;

    private String baseLanguage;
    private List<String> supportLanguages;

    private String publicServantName;
    private String publicServantEmail;

    public String toString() {
        //
        return toJson();
    }

    public static RegisterNaraCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterNaraCommand.class);
    }

    public static RegisterNaraCommand sample() {
        //
        Nara nara = Nara.sample();
        RegisterNaraCommand registerNaraCommand = new RegisterNaraCommand(
                nara.getIdentity().getName(),
                nara.getIdentity().getType(),
                nara.getSettings().getZoneId(),
                nara.getSettings().getLanguageSettings().getBaseLanguage(),
                nara.getSettings().getLanguageSettings().getSupportLanguages(),
                nara.getSettings().getServant().getDisplayName(),
                nara.getSettings().getServant().getLoginId()
        );
        registerNaraCommand.setRequester(Requester.sample());
        return registerNaraCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
