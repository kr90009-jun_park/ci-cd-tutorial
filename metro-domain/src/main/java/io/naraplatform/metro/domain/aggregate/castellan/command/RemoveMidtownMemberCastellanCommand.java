package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naraplatform.share.domain.tenant.CitizenKey;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RemoveMidtownMemberCastellanCommand extends CqrsUserCommand {
    //
//    private String castellanId;
//    private Member member;
//    private String citizenId;


    private Member member;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RemoveMidtownMemberCastellanCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RemoveMidtownMemberCastellanCommand.class);
    }

    public static RemoveMidtownMemberCastellanCommand sample() {
        //
//        RemoveMidtownMemberCastellanCommand removeMidtownMemberCastellanCommand = new RemoveMidtownMemberCastellanCommand(
//                UUID.randomUUID().toString(),
//                Member.sample(),
//                CitizenKey.sample().getKeyString()
//        );

        RemoveMidtownMemberCastellanCommand removeMidtownMemberCastellanCommand = new RemoveMidtownMemberCastellanCommand(
                Member.sample()
        );
        removeMidtownMemberCastellanCommand.setRequester(Requester.sample());
        return removeMidtownMemberCastellanCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
