package io.naraplatform.metro.domain.aggregate.metro.query;

import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class FindMetrosQuery extends CqrsUserQuery<List<Metro>> {
    //

}
