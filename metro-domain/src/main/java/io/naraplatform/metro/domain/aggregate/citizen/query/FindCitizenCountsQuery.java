package io.naraplatform.metro.domain.aggregate.citizen.query;

import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindCitizenCountsQuery extends CqrsUserQuery<CitizenCountResposne> {
    //
    private String metroId;
}

//@Getter
//@Setter
//@NoArgsConstructor
//@AllArgsConstructor
//class CitizenCounts implements JsonSerializable {
//    //
//    private int activeCount;
//    private int preliminaryCount;
//    private int dormantCount;
//    private int removedCount;
//}
