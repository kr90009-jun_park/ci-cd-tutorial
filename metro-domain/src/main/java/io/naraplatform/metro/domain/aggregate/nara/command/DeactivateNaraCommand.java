package io.naraplatform.metro.domain.aggregate.nara.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeactivateNaraCommand extends CqrsUserCommand {
    //
    private String naraId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static DeactivateNaraCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DeactivateNaraCommand.class);
    }

    public static DeactivateNaraCommand sample() {
        //
        DeactivateNaraCommand command = new DeactivateNaraCommand("nea");
        command.setRequester(Requester.sample());
        return command;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
