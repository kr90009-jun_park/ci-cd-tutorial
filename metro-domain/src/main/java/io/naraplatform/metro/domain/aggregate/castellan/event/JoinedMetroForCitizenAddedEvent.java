package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinedMetroForCitizenAddedEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private String joinedMetroId;
    private IdName citizen;

    public JoinedMetroForCitizenAddedEvent(Castellan castellan, CommandIdentity commandIdentity, String joinedMetroId, IdName citizen) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.joinedMetroId = joinedMetroId;
        this.citizen = citizen;
    }
}
