package io.naraplatform.metro.domain.aggregate.citizen.query;

import io.naradrama.prologue.util.json.JsonSerializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CitizenCountResposne implements JsonSerializable {
    //
    private int activeCount;
    private int preliminaryCount;
    private int dormantCount;
    private int removedCount;
}
