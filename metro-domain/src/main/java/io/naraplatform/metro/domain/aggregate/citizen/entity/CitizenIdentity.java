package io.naraplatform.metro.domain.aggregate.citizen.entity;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.domain.granule.Email;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.share.domain.tenant.CitizenKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class CitizenIdentity extends DomainEntity {
    //
    private String usid;                // nullable
    private String displayName;
    private String email;
    private CitizenState state;         // mutable
    private String metroId;
    private String castellanId;

    public CitizenIdentity(String id) {
        //
        super(id);
    }

    public CitizenIdentity(String citizenId,
                           String displayName,
                           String email,
                           String castellanId) {
        //
        super(citizenId);
        this.displayName = displayName;
        this.email = email;
        this.state = CitizenState.Preliminary;
        this.castellanId = castellanId;
    }

    public String toString() {
        //
        return toJson();
    }

    public static CitizenIdentity fromJson(String json) {
        //
        return JsonUtil.fromJson(json, CitizenIdentity.class);
    }

    public CitizenKey genKey() {
        //
        return new CitizenKey(getId());
    }

    public IdName genIdName() {
        //
        return new IdName(getId(), getDisplayName());
    }

    public static CitizenIdentity sample() {
        //
        CitizenKey citizenKey = CitizenKey.sample();

        return new CitizenIdentity(
                citizenKey.getKeyString(),
                "Kim, cheolsoo",
                Email.sample().getEmail(),
                Castellan.sample().getId()
        );
    }

    public static CitizenIdentity sample2() {
        //
        CitizenKey citizenKey = CitizenKey.sample();
        Locale locale = Locale.KOREA;

        return new CitizenIdentity(
                citizenKey.getKeyString(),
                "Lee, youngmi",
                "2" + Email.sample().getEmail(),
                Castellan.sample().getId()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
