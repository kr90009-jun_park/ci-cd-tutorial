package io.naraplatform.metro.domain.aggregate.citizen.event;

import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CitizenModifiedEvent extends CqrsDomainEvent {
    //
    private String citizenId;
    private NameValueList nameValues;

    public CitizenModifiedEvent(Citizen citizen, CommandIdentity commandIdentity, NameValueList nameValues) {
        //
        super(citizen.genEntityIdName(), commandIdentity);
        this.citizenId = citizen.getId();
        this.nameValues = nameValues;
    }
}
