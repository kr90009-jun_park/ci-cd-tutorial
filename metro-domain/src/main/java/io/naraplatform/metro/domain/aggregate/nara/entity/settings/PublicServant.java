package io.naraplatform.metro.domain.aggregate.nara.entity.settings;

import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.ValueObject;
import io.naradrama.prologue.util.exception.NaraException;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PublicServant implements ValueObject {
    //
    private String loginId;     // email
    private String displayName;

    public PublicServant(String loginId, String displayName) {
        //
        this.loginId = loginId;
        this.displayName = displayName;
    }

    public void setValues(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "loginId":
                    loginId = value;
                    break;
                case "displayName":
                    displayName = value;
                    break;
                default:
                    throw new NaraException(MetroExceptionMessage.cannotUpdateEntityField, this.getClass().getSimpleName(), nameValue.getName());
            }
        }
    }

    public String toString() {
        //
        return toJson();
    }

    public static PublicServant fromJson(String json) {
        //
        return JsonUtil.fromJson(json, PublicServant.class);
    }

    public static PublicServant sample() {
        //
        return new PublicServant("test@test.com", "public servant name");
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }


}
