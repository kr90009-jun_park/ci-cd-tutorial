package io.naraplatform.metro.domain.aggregate;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class OptionalSettings implements JsonSerializable {
    //
    private String profileId;
    private String tinyAlbumId;

    private NameValueList optionalSettings;

    public OptionalSettings() {
        //
        this.profileId = null;
        this.tinyAlbumId = null;
        this.optionalSettings = NameValueList.newEmptyInstance();
    }

    public String toString() {
        //
        return toJson();
    }

    public static OptionalSettings fromJson(String json) {
        //
        return JsonUtil.fromJson(json, OptionalSettings.class);
    }

    public static OptionalSettings sample() {
        //
        OptionalSettings sample = new OptionalSettings();
        sample.setProfileId(UUID.randomUUID().toString());
        sample.setTinyAlbumId(UUID.randomUUID().toString());

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
