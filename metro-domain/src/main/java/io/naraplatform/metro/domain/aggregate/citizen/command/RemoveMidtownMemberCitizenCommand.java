package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RemoveMidtownMemberCitizenCommand extends CqrsUserCommand {
    //
    private String citizenId;
    private String castellanId;
    private Member member;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RemoveMidtownMemberCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RemoveMidtownMemberCitizenCommand.class);
    }

    public static RemoveMidtownMemberCitizenCommand sample() {
        //
        RemoveMidtownMemberCitizenCommand removeMidtownMemberCitizenCommand = new RemoveMidtownMemberCitizenCommand(
                "r2p8@nea-m5",
                UUID.randomUUID().toString(),
                Member.sample()
        );
        removeMidtownMemberCitizenCommand.setRequester(Requester.sample());
        return removeMidtownMemberCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
