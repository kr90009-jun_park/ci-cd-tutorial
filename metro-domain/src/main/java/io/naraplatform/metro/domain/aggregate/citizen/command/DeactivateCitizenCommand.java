package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeactivateCitizenCommand extends CqrsUserCommand {
    //
    private String citizenId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static DeactivateCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DeactivateCitizenCommand.class);
    }

    public static DeactivateCitizenCommand sample() {
        //
        DeactivateCitizenCommand deactivateCitizenCommand = new DeactivateCitizenCommand("r2p8@nea-m5");
        deactivateCitizenCommand.setRequester(Requester.sample());
        return deactivateCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
