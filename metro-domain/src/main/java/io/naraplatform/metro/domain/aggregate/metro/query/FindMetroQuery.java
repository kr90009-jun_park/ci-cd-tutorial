package io.naraplatform.metro.domain.aggregate.metro.query;

import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindMetroQuery extends CqrsUserQuery<Metro> {
    //
    private String metroId;
}
