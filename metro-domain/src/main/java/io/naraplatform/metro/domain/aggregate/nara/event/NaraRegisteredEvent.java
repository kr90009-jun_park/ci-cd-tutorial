package io.naraplatform.metro.domain.aggregate.nara.event;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NaraRegisteredEvent extends CqrsDomainEvent {
    //
    private String naraId;
    private String name;            // Identity

    private String zoneId;

    private String baseLanguage;
    private List<String> supportLanguages;

    private String publicServantName;
    private String publicServantEmail;

    public NaraRegisteredEvent(IdName aggregate,
                               CommandIdentity commandIdentity,
                               String naraId,
                               String name,
                               String zoneId,
                               String baseLanguage,
                               List<String> supportLanguages,
                               String publicServantName,
                               String publicServantEmail) {
        //
        super(aggregate, commandIdentity);
        this.naraId = naraId;
        this.name = name;
        this.zoneId = zoneId;
        this.baseLanguage = baseLanguage;
        this.supportLanguages = supportLanguages;
        this.publicServantName = publicServantName;
        this.publicServantEmail = publicServantEmail;
    }
}
