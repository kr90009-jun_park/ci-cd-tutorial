package io.naraplatform.metro.domain.facade.aggregate.castellan;

import io.naradrama.prologue.domain.drama.AuthorizedRole;
import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailAndNameQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellansByDisplayNameQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellansQuery;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="CastellanQuery",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CastellanQueryFacade {
    //
    @AuthorizedRole(isPublic = true)
    FindCastellanEmailByEmailQuery findCastellanEmailByEmail(FindCastellanEmailByEmailQuery findCastellanEmailByEmailQuery);
    @AuthorizedRole(isPublic = true)
    FindCastellanEmailByEmailAndNameQuery findCastellanByEmailAndName(FindCastellanEmailByEmailAndNameQuery findCastellanEmailByEmailAndNameQuery);
    @AuthorizedRole(isPublic = true)
    FindCastellansQuery findCastellans(FindCastellansQuery findCastellansQuery);
    @AuthorizedRole(isPublic = true)
    FindCastellansByDisplayNameQuery findCastellansByDisplayName(FindCastellansByDisplayNameQuery findCastellansByDisplayNameQuery);
}
