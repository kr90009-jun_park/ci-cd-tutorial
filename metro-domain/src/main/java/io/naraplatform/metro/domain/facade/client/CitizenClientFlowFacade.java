package io.naraplatform.metro.domain.facade.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterPavilionManagerCitizenCommand;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="command.CitizenCommandProto",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CitizenClientFlowFacade {
    //
    RegisterCitizenCommand registerCitizen(RegisterCitizenCommand command, CommandIdentity callerCommandIdentity);
    RegisterPavilionManagerCitizenCommand registerPavilionManagerCitizen(RegisterPavilionManagerCitizenCommand command,
                                                                         CommandIdentity callerCommandIdentity);
}
