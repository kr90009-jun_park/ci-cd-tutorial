package io.naraplatform.metro.domain.aggregate.citizen.query;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindCitizenIdentitiesQuery extends CqrsUserQuery<List<CitizenIdentity>> {
    //
    private String metroId;
}
