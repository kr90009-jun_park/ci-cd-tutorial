package io.naraplatform.metro.domain.facade.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.drama.AuthorizedRole;
import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="command.CastellanCommandProto",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CastellanClientCommandFacade {
    //
    @AuthorizedRole(isPublic = true)
    RegisterManagerCastellanCommand registerManagerCastellan(RegisterManagerCastellanCommand command, CommandIdentity callerCommandIdentity);
    RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellan(RegisterPavilionManagerCastellanCommand command, CommandIdentity callerCommandIdentity);

}
