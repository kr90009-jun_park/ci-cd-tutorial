package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ModifyMidtownMemberCitizenCommand extends CqrsUserCommand {
    //
    private String citizenId;
    private String castellanId;
    private Member member;
    private String previousEmail;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ModifyMidtownMemberCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ModifyMidtownMemberCitizenCommand.class);
    }

    public static ModifyMidtownMemberCitizenCommand sample() {
        //
        ModifyMidtownMemberCitizenCommand modifyMidtownMemberCitizenCommand = new ModifyMidtownMemberCitizenCommand(
                "r2p8@nea-m5",
                UUID.randomUUID().toString(),
                Member.sample(),
                "jobs@nextree.io"
        );
        modifyMidtownMemberCitizenCommand.setRequester(Requester.sample());
        return modifyMidtownMemberCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
