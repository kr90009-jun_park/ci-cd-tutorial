package io.naraplatform.metro.domain.aggregate.citizen.event;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailForCitizenInvitationResentEvent extends CqrsDomainEvent {
    //
    private String citizenId;
    private String secretCode;

    public MailForCitizenInvitationResentEvent(IdName aggregate, CommandIdentity commandIdentity, String secretCode) {
        //
        super(aggregate, commandIdentity);
        this.citizenId = aggregate.getId();
        this.secretCode = secretCode;
    }
}
