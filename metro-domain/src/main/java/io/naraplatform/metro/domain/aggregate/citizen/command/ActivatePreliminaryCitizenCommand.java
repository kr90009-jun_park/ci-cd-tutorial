package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActivatePreliminaryCitizenCommand extends CqrsUserCommand {
    //
    private String citizenId;
    private String secretCode;
    private String password;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ActivatePreliminaryCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ActivatePreliminaryCitizenCommand.class);
    }

    public static ActivatePreliminaryCitizenCommand sample() {
        //
        ActivatePreliminaryCitizenCommand activatePreliminaryCitizenCommand = new ActivatePreliminaryCitizenCommand(
                "r2p8@nea-m5",
                "12345",
                "1");
        activatePreliminaryCitizenCommand.setRequester(Requester.sample());
        return activatePreliminaryCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
