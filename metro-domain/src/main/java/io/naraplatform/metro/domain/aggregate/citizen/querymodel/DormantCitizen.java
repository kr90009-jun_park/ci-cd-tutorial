package io.naraplatform.metro.domain.aggregate.citizen.querymodel;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DormantCitizen extends AbstractCitizen {
    //
    public DormantCitizen(String id) {
        //
        super(id);
    }

    public DormantCitizen(ActiveCitizen activeCitizen) {
        //
        super(activeCitizen);
        getIdentity().setState(CitizenState.Dormant);
    }

    public static DormantCitizen fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DormantCitizen.class);
    }

    public static DormantCitizen sample() {
        //
        ActiveCitizen activeCitizen = ActiveCitizen.sample();

        return new DormantCitizen(
                activeCitizen
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
