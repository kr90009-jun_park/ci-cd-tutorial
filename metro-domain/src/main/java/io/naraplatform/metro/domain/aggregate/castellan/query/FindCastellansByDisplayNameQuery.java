package io.naraplatform.metro.domain.aggregate.castellan.query;

import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanName;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindCastellansByDisplayNameQuery extends CqrsUserQuery<List<CastellanName>> {
    //
    private String displayName;
}
