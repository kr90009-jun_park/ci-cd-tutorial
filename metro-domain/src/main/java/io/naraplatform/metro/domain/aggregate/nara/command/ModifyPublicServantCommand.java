package io.naraplatform.metro.domain.aggregate.nara.command;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ModifyPublicServantCommand extends CqrsUserCommand {
    //
    private String naraId;
    private NameValueList nameValues;

    public static ModifyPublicServantCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ModifyPublicServantCommand.class);
    }

    public static ModifyPublicServantCommand sample() {
        //
        NameValueList nameValueList = NameValueList.newInstance("loginId", "kspark@nextree.io");
        nameValueList.add("displayName", "Park Nara");
        ModifyPublicServantCommand modifyPublicServantCommand
                = new ModifyPublicServantCommand("nea", nameValueList);
        modifyPublicServantCommand.setRequester(Requester.sample());
        return modifyPublicServantCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
