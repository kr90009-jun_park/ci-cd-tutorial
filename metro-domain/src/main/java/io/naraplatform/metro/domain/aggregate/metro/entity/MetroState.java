package io.naraplatform.metro.domain.aggregate.metro.entity;

import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;

public enum MetroState {
    //
    Preliminary,
    Active,
    Dormant,
    Removed,
    Destroyed;     // after RemoveRemovedMetro. Only triggerd by RemoveRemovedMetro. Not by modify state.

    MetroState() {
        //
    }

    public MetroState next() {
        //
        switch (this) {
            case Preliminary:
                return Active;
            case Active:
                return Dormant;
            case Dormant:
                return Removed;
            case Removed:
                return Destroyed;

        }

        return null;
    }

    public MetroState previous() {
        //
        if (this.equals(Dormant)) {
            return Active;
        }
        if(this.equals(Destroyed)){
            return Removed;
        }

        throw new NaraException(MetroExceptionMessage.cannotChangeMetroStateToPrevious, this);
    }

    public static void main(String[] args) {
        //
        System.out.println(MetroState.Active.next());
        System.out.println(MetroState.Dormant.previous());
        System.out.println(MetroState.Removed.next());
    }
}
