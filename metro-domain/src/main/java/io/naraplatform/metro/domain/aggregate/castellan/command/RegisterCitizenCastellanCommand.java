package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterCitizenCastellanCommand extends CqrsUserCommand {
    //
    private String castellanId;
    private String email;
    private String name;
    private String joinedMetro;
    private String citizenId;

    // Auto Setup
    private String encryptedPassword;
    private boolean activating;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterCitizenCastellanCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterCitizenCastellanCommand.class);
    }

    public static RegisterCitizenCastellanCommand sample() {
        //
        return sample(7);
    }

    public static RegisterCitizenCastellanCommand sample(int sequence) {
        //
        RegisterCitizenCastellanCommand registerCitizenCastellanCommand = new RegisterCitizenCastellanCommand(
                UUID.randomUUID().toString(),
                sequence + "jobs@nextree.io",
                "Steve Jobs" + sequence,
                "nea-m" + sequence,
                "r2p8@nea-m" + sequence,
                "$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei",
                true
        );
        registerCitizenCastellanCommand.setRequester(Requester.sample());
        return registerCitizenCastellanCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
