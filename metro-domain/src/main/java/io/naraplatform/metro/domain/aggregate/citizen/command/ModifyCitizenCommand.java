package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ModifyCitizenCommand extends CqrsUserCommand {
    //
    private String citizenId;
    private NameValueList nameValues;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ModifyCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ModifyCitizenCommand.class);
    }

    public static ModifyCitizenCommand sample() {
        //
        Citizen citizen = Citizen.sample();
        NameValueList nameValueList = NameValueList.newInstance("displayName", "Steve Jobs");
        ModifyCitizenCommand modifyCitizenCommand = new ModifyCitizenCommand(citizen.getId(), nameValueList);
        modifyCitizenCommand.setRequester(Requester.sample());
        return modifyCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
