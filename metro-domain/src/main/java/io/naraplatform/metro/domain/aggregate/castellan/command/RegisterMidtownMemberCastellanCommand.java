package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterMidtownMemberCastellanCommand extends CqrsUserCommand {
    //
    private Member member;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterMidtownMemberCastellanCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterMidtownMemberCastellanCommand.class);
    }

    public static RegisterMidtownMemberCastellanCommand sample() {
        //
        return sample(1);
    }

    public static RegisterMidtownMemberCastellanCommand sample(int sequence) {
        //
        Member member = Member.sample();
        member.setEmail(sequence + "jobs@nextree.io");
        member.setName("ko","Steve Jobs " + sequence);
        RegisterMidtownMemberCastellanCommand registerMidtownMemberCastellanCommand = new RegisterMidtownMemberCastellanCommand(member);
        registerMidtownMemberCastellanCommand.setRequester(Requester.sample());
        return registerMidtownMemberCastellanCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
