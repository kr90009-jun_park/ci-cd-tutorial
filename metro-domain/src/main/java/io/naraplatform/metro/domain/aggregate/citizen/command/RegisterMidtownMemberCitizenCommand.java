package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterMidtownMemberCitizenCommand extends CqrsUserCommand {
    //
//    private String citizenId;
//    private String castellanId;

    private String memberId;
    private Member member;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterMidtownMemberCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterMidtownMemberCitizenCommand.class);
    }

    //    public static RegisterMidtownMemberCitizenCommand sample() {
//        //
//        RegisterMidtownMemberCitizenCommand registerMidtownMemberCitizenCommand = new RegisterMidtownMemberCitizenCommand(
//                "r2p8@nea-m5",
//                UUID.randomUUID().toString(),
//                Member.sample()
//        );
//        registerMidtownMemberCitizenCommand.setRequester(Requester.sample());
//        return registerMidtownMemberCitizenCommand;
//    }
    public static RegisterMidtownMemberCitizenCommand sample() {
        //
        RegisterMidtownMemberCitizenCommand registerMidtownMemberCitizenCommand = new RegisterMidtownMemberCitizenCommand(
                UUID.randomUUID().toString(),
                Member.sample()
        );
        registerMidtownMemberCitizenCommand.setRequester(Requester.sample());
        return registerMidtownMemberCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
