package io.naraplatform.metro.domain.aggregate.nara.entity;

import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.metro.domain.aggregate.OptionalSettings;
import io.naraplatform.metro.domain.aggregate.nara.entity.settings.NaraLanguageSettings;
import io.naraplatform.metro.domain.aggregate.nara.entity.settings.NaraSettings;
import io.naraplatform.metro.domain.aggregate.nara.entity.settings.PublicServant;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Nara extends DomainEntity implements DomainAggregate {
    //
    private NaraIdentity identity;
    private NaraSettings settings;

    public Nara(String id) {
        //
        super(id);
    }

    public Nara(NaraIdentity identity, NaraSettings settings) {
        //
        super(identity.getId());
        this.identity = identity;
        this.settings = settings;
    }

    public static Nara sample() {
        //
        return new Nara(
                NaraIdentity.sampleForEnterprise(),
                NaraSettings.sample());
    }

    public void setValues(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
//                case "baseLanguage":
//                    this.baseLanguage = value;
//                    break;
                case "name":
                    this.identity.setName(value);
                    break;
                case "languageSettings":
                    this.settings.setLanguageSettings(NaraLanguageSettings.fromJson(value));
                    break;
                case "servant":
                    this.settings.setServant(PublicServant.fromJson(value));
                    break;
                case "optionalSettings":
                    this.settings.setOptionalSettings(OptionalSettings.fromJson(value));
                    break;
                default:
                    throw new NaraException(MetroExceptionMessage.cannotUpdateEntityField, this.getClass().getSimpleName(), nameValue.getName());
            }
        }
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
