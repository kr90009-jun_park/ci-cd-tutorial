package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterPreliminaryCitizenCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String name;
    private String email;

    private String usid;
//
//    private String castellanId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterPreliminaryCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterPreliminaryCitizenCommand.class);
    }

    public static RegisterPreliminaryCitizenCommand sample() {
        //
        return sample(5);
    }

    public static RegisterPreliminaryCitizenCommand sample(int sequence) {
        //
        RegisterPreliminaryCitizenCommand command = new RegisterPreliminaryCitizenCommand(
                "nea-m" + sequence,
                "Park Nara",
                "nara@nextree.io",
                UUID.randomUUID().toString()
//                UUID.randomUUID().toString()
        );
        command.setRequester(Requester.sample());
        return command;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
