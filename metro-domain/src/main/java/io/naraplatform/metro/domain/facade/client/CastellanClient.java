package io.naraplatform.metro.domain.facade.client;

public interface CastellanClient extends CastellanClientCommandFacade, CastellanClientFlowFacade, CastellanClientQueryFacade{
    //
}
