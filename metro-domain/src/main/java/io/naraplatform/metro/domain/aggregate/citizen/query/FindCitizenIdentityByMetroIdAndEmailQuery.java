package io.naraplatform.metro.domain.aggregate.citizen.query;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindCitizenIdentityByMetroIdAndEmailQuery extends CqrsUserQuery<CitizenIdentity> {
    //
    private String metroId;
    private String email;
}
