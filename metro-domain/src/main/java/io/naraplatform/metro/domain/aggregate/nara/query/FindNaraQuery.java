package io.naraplatform.metro.domain.aggregate.nara.query;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindNaraQuery extends CqrsUserQuery<Nara> {
    //
    private String naraId;
}
