package io.naraplatform.metro.domain.aggregate.citizen.event;

import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreliminaryCitizenRegisteredEvent extends CqrsDomainEvent {
    //
    private String citizenId;
    private String castellanId;
    private String metroId;
    private String name;
    private String email;
    private String secretCode;
    private CitizenSettings settings;

    // Auto Setup
    private String usid;

    public PreliminaryCitizenRegisteredEvent(Citizen citizen, CommandIdentity commandIdentity, String castellanId, String metroId, String name, String email, String secretCode, CitizenSettings settings, String usid) {
        //
        super(citizen.genEntityIdName(), commandIdentity);
        this.citizenId = citizen.getId();
        this.castellanId = castellanId;
        this.metroId = metroId;
        this.name = name;
        this.email = email;
        this.secretCode = secretCode;
        this.settings = settings;
        this.usid = usid;
    }
}
