package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterActiveCitizenCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String name;
    private String email;
    private String castellanId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterActiveCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterActiveCitizenCommand.class);
    }

    public static RegisterActiveCitizenCommand sample() {
        //
        return sample(3);
    }

    public static RegisterActiveCitizenCommand sample(int sequence) {
        //
        RegisterActiveCitizenCommand command = new RegisterActiveCitizenCommand(
                "nea-m" + sequence,
                "Steve Jobs",
                sequence + "jobs@nextree.io",
                UUID.randomUUID().toString());
        command.setRequester(Requester.sample());
        return command;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
