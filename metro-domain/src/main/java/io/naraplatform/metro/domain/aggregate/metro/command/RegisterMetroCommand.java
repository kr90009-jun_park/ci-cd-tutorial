package io.naraplatform.metro.domain.aggregate.metro.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterMetroCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String name;
    private String officialId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterMetroCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterMetroCommand.class);
    }

    public static RegisterMetroCommand sample() {
        //
        RegisterMetroCommand registerMetroCommand = new RegisterMetroCommand(
                "nea-m5",
                "Nextree Metro",
                "r2p8@nea-m5");
        registerMetroCommand.setRequester(Requester.sample());
        return registerMetroCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
