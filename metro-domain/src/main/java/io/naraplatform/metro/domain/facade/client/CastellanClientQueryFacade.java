package io.naraplatform.metro.domain.facade.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.drama.AuthorizedRole;
import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailQuery;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="query.CastellanQueryProto",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CastellanClientQueryFacade {
    //
    @AuthorizedRole(isPublic = true)
    FindCastellanEmailByEmailQuery findCastellanEmailByEmail(FindCastellanEmailByEmailQuery query, CommandIdentity callerCommandIdentity);
}
