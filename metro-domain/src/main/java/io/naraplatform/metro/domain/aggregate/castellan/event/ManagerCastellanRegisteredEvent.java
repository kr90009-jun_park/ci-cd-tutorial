package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import io.naradrama.prologue.domain.lang.LangString;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ManagerCastellanRegisteredEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private String email;
    private LangString name;
    private String encryptedPassword;
    private boolean stationManager;

    public ManagerCastellanRegisteredEvent(Castellan castellan, CommandIdentity commandIdentity, String email, LangString name, String encryptedPassword, boolean stationManager) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.email = email;
        this.name = name;
        this.encryptedPassword = encryptedPassword;
        this.stationManager = stationManager;
    }
}
