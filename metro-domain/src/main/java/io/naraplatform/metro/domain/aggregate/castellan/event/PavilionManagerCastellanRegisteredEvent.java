package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import io.naradrama.prologue.domain.lang.LangString;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PavilionManagerCastellanRegisteredEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private String email;
    private LangString name;
    private String joinedMetro;
    private String encryptedPassword;
    private String citizenId;

    public PavilionManagerCastellanRegisteredEvent(Castellan castellan, CommandIdentity commandIdentity, String email, LangString name, String joinedMetro, String encryptedPassword, String citizenId) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.email = email;
        this.name = name;
        this.joinedMetro = joinedMetro;
        this.encryptedPassword = encryptedPassword;
        this.citizenId = citizenId;
    }
}
