package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MidtownMemberCastellanRemovedEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private Member member;
    private String citizenId;

    public MidtownMemberCastellanRemovedEvent(IdName aggregate, CommandIdentity commandIdentity, Member member, String citizenId) {
        //
        super(aggregate, commandIdentity);
        this.castellanId = aggregate.getId();
        this.member = member;
        this.citizenId = citizenId;
    }
}
