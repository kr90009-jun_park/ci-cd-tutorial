package io.naraplatform.metro.domain.aggregate.metro.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterMetroOfficialCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String citizenId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterMetroOfficialCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterMetroOfficialCommand.class);
    }

    public static RegisterMetroOfficialCommand sample() {
        //
        RegisterMetroOfficialCommand registerMetroOfficialCommand = new RegisterMetroOfficialCommand("nea-m5", "r2p9@nea-m5");
        registerMetroOfficialCommand.setRequester(Requester.sample());
        return registerMetroOfficialCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
