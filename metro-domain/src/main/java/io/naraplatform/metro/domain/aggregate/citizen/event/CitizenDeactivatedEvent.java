package io.naraplatform.metro.domain.aggregate.citizen.event;

import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CitizenDeactivatedEvent extends CqrsDomainEvent {
    //
    private String citizenId;

    public CitizenDeactivatedEvent(Citizen citizen, CommandIdentity commandIdentity) {
        //
        super(citizen.genEntityIdName(), commandIdentity);
        this.citizenId = citizen.getId();
    }
}
