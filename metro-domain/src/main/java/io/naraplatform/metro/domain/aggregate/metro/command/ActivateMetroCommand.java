package io.naraplatform.metro.domain.aggregate.metro.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActivateMetroCommand extends CqrsUserCommand {
    //
    private String metroId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ActivateMetroCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ActivateMetroCommand.class);
    }

    public static ActivateMetroCommand sample() {
        //
        ActivateMetroCommand activateMetroCommand = new ActivateMetroCommand("nea-m5");
        activateMetroCommand.setRequester(Requester.sample());
        return activateMetroCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
