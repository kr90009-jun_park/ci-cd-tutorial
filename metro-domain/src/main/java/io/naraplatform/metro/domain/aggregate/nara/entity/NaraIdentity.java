package io.naraplatform.metro.domain.aggregate.nara.entity;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naraplatform.share.domain.tenant.NaraKey;
import io.naraplatform.share.domain.tenant.NaraType;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class NaraIdentity extends DomainEntity {
    //
    private String name;
    private NaraType type;
    private NaraState state;

    public NaraIdentity(String id) {
        //
        super(id);
    }

    public NaraIdentity(NaraKey key, String name) {
        //
        super(key.getKeyString());
        this.name = name;
        this.type = key.genNaraType();
        this.state = NaraState.Preliminary;
    }

    public String toString() {
        //
        return toJson();
    }

    public static NaraIdentity fromJson(String json) {
        //
        return JsonUtil.fromJson(json, NaraIdentity.class);
    }

    public NaraKey genKey() {
        //
        return new NaraKey(getId());
    }


    public IdName getIdName() {
        //
        return new IdName(getId(), getName());
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "name":
                    this.name = value;
                    break;
                case "state":
                    this.state = NaraState.valueOf(value);
                    break;
// updated with Nara
//                default:
//                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static NaraIdentity sampleForHome() {
        //
        String defaultLanguage = Locale.US.getLanguage();
        NaraKey naraKey = NaraKey.sampleForHome();

        return new NaraIdentity(
                naraKey,
                "Nextree Consulting");
    }

    public static NaraIdentity sampleForService() {
        //
        String defaultLanguage = Locale.US.getLanguage();
        NaraKey naraKey = NaraKey.sampleForService();

        return new NaraIdentity(
                naraKey,
                "Nextree Consulting");
    }

    public static NaraIdentity sampleForEnterprise() {
        //
        String defaultLanguage = Locale.US.getLanguage();
        NaraKey naraKey = NaraKey.sampleForEnterprise();

        return new NaraIdentity(
                naraKey,
                "Nextree Consulting");
    }

    public static void main(String[] args) {
        //
        System.out.println(sampleForHome());
        System.out.println(sampleForService());
        System.out.println(sampleForEnterprise());
    }
}
