package io.naraplatform.metro.domain.aggregate.metro.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssignMetroOfficialToPrimaryCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String citizenId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static AssignMetroOfficialToPrimaryCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AssignMetroOfficialToPrimaryCommand.class);
    }

    public static AssignMetroOfficialToPrimaryCommand sample() {
        //
        AssignMetroOfficialToPrimaryCommand assignMetroOfficialToPrimaryCommand = new AssignMetroOfficialToPrimaryCommand("nea-m5", "r2p8@nea-m5");
        assignMetroOfficialToPrimaryCommand.setRequester(Requester.sample());
        return assignMetroOfficialToPrimaryCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
