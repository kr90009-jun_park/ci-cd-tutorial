package io.naraplatform.metro.domain.aggregate.citizen.querymodel;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RemovedCitizen extends AbstractCitizen {
    //
    public RemovedCitizen(String id) {
        //
        super(id);
    }

    public RemovedCitizen(DormantCitizen dormantCitizen) {
        //
        super(dormantCitizen);
        getIdentity().setState(CitizenState.Removed);
    }

    public RemovedCitizen(PreliminaryCitizen preliminaryCitizen) {
        //
        super(preliminaryCitizen);
        getIdentity().setState(CitizenState.Removed);
    }

    public RemovedCitizen(ActiveCitizen activeCitizen) {
        //
        super(activeCitizen);
        getIdentity().setState(CitizenState.Removed);
    }

    public static RemovedCitizen fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RemovedCitizen.class);
    }

    public static RemovedCitizen sample() {
        //
        ActiveCitizen activeCitizen = ActiveCitizen.sample();

        return new RemovedCitizen(
                activeCitizen
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
