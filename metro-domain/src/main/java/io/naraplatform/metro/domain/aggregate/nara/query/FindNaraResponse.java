package io.naraplatform.metro.domain.aggregate.nara.query;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naradrama.prologue.util.json.JsonSerializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindNaraResponse implements JsonSerializable {
    //
    private Nara nara;
}
