package io.naraplatform.metro.domain.facade.client;

public interface CitizenClient extends CitizenClientFlowFacade, CitizenClientQueryFacade{
    //
}
