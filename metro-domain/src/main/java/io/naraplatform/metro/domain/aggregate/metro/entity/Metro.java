package io.naraplatform.metro.domain.aggregate.metro.entity;

import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naraplatform.share.domain.tenant.MetroKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Metro extends DomainEntity implements DomainAggregate {
    //
    private MetroState state;
//    private MetroOfficials officials;

    public Metro(String id) {
        //
        super(id);
    }

    public Metro(String id, MetroState state) {
        //
        super(id);
        this.state = state;
    }

    public static Metro sample() {
        //
        Metro metro = new Metro(MetroKey.sample().getKeyString());
        metro.setState(MetroState.Preliminary);
//        metro.setOfficials(MetroOfficials.sample());

        return metro;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
