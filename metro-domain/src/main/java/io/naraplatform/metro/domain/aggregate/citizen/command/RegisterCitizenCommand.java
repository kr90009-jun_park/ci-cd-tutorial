package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterCitizenCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String name;
    private String email;
    private String usid;
    private String encryptedPassword;
    private boolean activating;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterCitizenCommand.class);
    }

    public static RegisterCitizenCommand sample() {
        //
        return sample(1, true);
    }

    public static RegisterCitizenCommand sample(int sequence, boolean activating) {
        //
        RegisterCitizenCommand registerCitizenCommand = new RegisterCitizenCommand(
                "nea-m" + sequence,
                "Park Metro",
                sequence + "metro@nextree.io",
                UUID.randomUUID().toString(),
                "$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei",
                activating);

        return registerCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
