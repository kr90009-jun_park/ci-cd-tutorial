package io.naraplatform.metro.domain.aggregate.citizen.entity;

import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;

public enum CitizenState {
    //
    Preliminary,
    Active,
    Dormant,
    Removed;

    CitizenState() {
        //
    }

    public CitizenState next() {
        //
        switch (this) {
            case Preliminary:
                return Active;
            case Active:
                return Dormant;
            case Dormant:
                return Removed;
        }

        return null;
    }

    public CitizenState previous() {
        //
        if (this.equals(Dormant)) {
            return Active;
        }
        if (this.equals(Removed)) {
            return Dormant;
        }

        throw new NaraException(MetroExceptionMessage.cannotChangeCitizenStateToPrevious, this);
    }

    public static void main(String[] args) {
        //
        System.out.println(CitizenState.Active.next());
        System.out.println(CitizenState.Dormant.previous());
        System.out.println(CitizenState.Removed.previous());
    }
}
