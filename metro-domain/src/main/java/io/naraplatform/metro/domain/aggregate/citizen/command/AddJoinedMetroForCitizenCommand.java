package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddJoinedMetroForCitizenCommand extends CqrsUserCommand {
    //
    private String castellanId;
    private String joinedMetro;
    private IdName citizen;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static AddJoinedMetroForCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AddJoinedMetroForCitizenCommand.class);
    }

    public static AddJoinedMetroForCitizenCommand sample() {
        //
        return sample(9);
    }

    public static AddJoinedMetroForCitizenCommand sample(int sequence) {
        //
        AddJoinedMetroForCitizenCommand addJoinedMetroForCitizenCommand = new AddJoinedMetroForCitizenCommand(
                UUID.randomUUID().toString(),
                "nea-m" + sequence,
                new IdName("r2p8@nea-m" + sequence, "Steve Jobs" + sequence)
        );
        addJoinedMetroForCitizenCommand.setRequester(Requester.sample());
        return addJoinedMetroForCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
