package io.naraplatform.metro.domain.aggregate.citizen.querymodel;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PreliminaryCitizen extends AbstractCitizen {
    //
    public PreliminaryCitizen(String id) {
        //
        super(id);
    }

    public PreliminaryCitizen(CitizenIdentity identity) {
        //
        super(identity);
    }

    public String toString() {
        //
        return toJson();
    }

    public static PreliminaryCitizen fromJson(String json) {
        //
        return JsonUtil.fromJson(json, PreliminaryCitizen.class);
    }

    public static PreliminaryCitizen sample() {
        //
        CitizenIdentity identity = CitizenIdentity.sample();

        PreliminaryCitizen preliminaryCitizen
                 = new PreliminaryCitizen(identity);
        preliminaryCitizen.setSettings(CitizenSettings.sample());
        return preliminaryCitizen;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
