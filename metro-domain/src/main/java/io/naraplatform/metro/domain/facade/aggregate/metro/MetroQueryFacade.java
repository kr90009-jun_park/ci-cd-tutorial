package io.naraplatform.metro.domain.facade.aggregate.metro;

import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.metro.query.FindMetroQuery;
import io.naraplatform.metro.domain.aggregate.metro.query.FindMetrosQuery;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="MetroQuery",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface MetroQueryFacade {
    //
    FindMetroQuery findMetro(FindMetroQuery findMetroQuery);
    FindMetrosQuery findMetros(FindMetrosQuery findMetrosQuery);
}
