package io.naraplatform.metro.domain.aggregate.castellan.entity;

import io.naradrama.prologue.domain.ddd.ValueObject;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class JoinedMetroList implements ValueObject {
    //
    private List<JoinedMetro> joinedMetros;

    public JoinedMetroList() {
        //
        this.joinedMetros = new ArrayList<>();
    }

    public JoinedMetroList(JoinedMetro joinedMetro) {
        //
        this();
        this.joinedMetros.add(joinedMetro);
    }

    public void add(JoinedMetro joinedMetro) {
        //
        this.joinedMetros.add(joinedMetro);
    }

    public void addAll(List<JoinedMetro> joinedMetros) {
        //
        this.joinedMetros.addAll(joinedMetros);
    }

    public void remove(String metroId) {
        //
        JoinedMetro targetMetro = null;
        for(JoinedMetro joinedMetro : joinedMetros) {
            //
            if(joinedMetro.getMetroId().equals(metroId)) {
                targetMetro = joinedMetro;
                break;
            }
        }
        if(targetMetro != null) {
            joinedMetros.remove(targetMetro);
        }
    }

    public String toString() {
        //
        return toJson();
    }

    public static JoinedMetroList fromJson(String json) {
        //
        return JsonUtil.fromJson(json, JoinedMetroList.class);
    }

    public static JoinedMetroList sample() {
        //
        return new JoinedMetroList(JoinedMetro.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
