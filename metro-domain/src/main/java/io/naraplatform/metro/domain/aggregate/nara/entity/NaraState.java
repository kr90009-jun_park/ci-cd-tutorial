package io.naraplatform.metro.domain.aggregate.nara.entity;

import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;

public enum NaraState {
    //
    Preliminary,
    Active,
    Dormant,
    Removed,
    Destroyed;

    NaraState() {
        //
    }

    public NaraState next() {
        //
        switch (this) {
            case Preliminary:
                return Active;
            case Active:
                return Dormant;
            case Dormant:
                return Removed;
            case Removed:
                return Destroyed;
        }

        return null;
    }

    public NaraState previous() {
        //
        if (this.equals(Dormant)) {
            return Active;
        }
        if(this.equals(Destroyed)){
            return Removed;
        }

        throw new NaraException(MetroExceptionMessage.cannotChangeNaraStateToPrevious, this);
    }
}
