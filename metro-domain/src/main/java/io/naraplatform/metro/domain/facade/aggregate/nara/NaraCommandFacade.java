package io.naraplatform.metro.domain.facade.aggregate.nara;

import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.nara.command.DeactivateNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.ModifyNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="NaraCommand",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface NaraCommandFacade {
    //
    RegisterNaraCommand registerNara(RegisterNaraCommand registerNaraCommand);
    ModifyNaraCommand modifyNara(ModifyNaraCommand registerNaraCommand);
    DeactivateNaraCommand deactivateNara(DeactivateNaraCommand registerNaraCommand);
}
