package io.naraplatform.metro.domain.aggregate.nara.entity.settings;

import io.naraplatform.metro.domain.aggregate.OptionalSettings;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.ddd.ValueObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.TimeZone;

@Getter
@Setter
@NoArgsConstructor
//public class NaraSettings extends DomainEntity {
public class NaraSettings implements ValueObject {
    //
    private String zoneId;
    private NaraLanguageSettings languageSettings;
    private PublicServant servant;
    private OptionalSettings optionalSettings;
    private long time;

    public NaraSettings( String zoneId,
                        NaraLanguageSettings languageSettings,
                        String publicServantName,
                        String publicServantEmail) {
        //
        this.zoneId = zoneId;
        this.languageSettings = languageSettings;
        this.servant = new PublicServant(publicServantEmail, publicServantName);
        this.time = System.currentTimeMillis();
    }

    public void setValue(NameValue nameValue) {
        //
        String value = nameValue.getValue();
        switch (nameValue.getName()) {
            case "languageSettings":
                this.languageSettings = NaraLanguageSettings.fromJson(value);
                break;
            case "servant":
                this.servant = PublicServant.fromJson(value);
                break;
            case "optionalSettings":
                this.optionalSettings = OptionalSettings.fromJson(value);
                break;
        }
    }

    public static NaraSettings sample() {
        //
        NaraSettings settings = new NaraSettings();
        settings.setZoneId(TimeZone.getDefault().toZoneId().getId());
        settings.setLanguageSettings(NaraLanguageSettings.sample());
        settings.setServant(PublicServant.sample());
        settings.setOptionalSettings(OptionalSettings.sample());
        settings.setTime(System.currentTimeMillis());
        return settings;
    }
}
