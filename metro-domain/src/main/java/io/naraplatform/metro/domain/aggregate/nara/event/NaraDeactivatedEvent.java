package io.naraplatform.metro.domain.aggregate.nara.event;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NaraDeactivatedEvent extends CqrsDomainEvent {
    //
    private String naraId;

    public NaraDeactivatedEvent(IdName aggregate, CommandIdentity commandIdentity, String naraId) {
        //
        super(aggregate, commandIdentity);
        this.naraId = naraId;
    }
}
