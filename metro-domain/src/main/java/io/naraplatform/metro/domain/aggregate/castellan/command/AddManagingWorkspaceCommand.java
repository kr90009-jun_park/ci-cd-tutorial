package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;


@Deprecated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddManagingWorkspaceCommand extends CqrsUserCommand {
    //
    private String castellanId;
    private String managerId;
    private String workspaceId;
    private String workspaceName;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static AddManagingWorkspaceCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AddManagingWorkspaceCommand.class);
    }

    public static AddManagingWorkspaceCommand sample() {
        //
        return sample(5);
    }

    public static AddManagingWorkspaceCommand sample(int sequence) {
        //
        AddManagingWorkspaceCommand addManagingWorkspaceCommand = new AddManagingWorkspaceCommand(
                UUID.randomUUID().toString(),
                "r2p8@nea-m" + sequence,
                "nea-m" + sequence,
                "Nextree Metro"
        );
        addManagingWorkspaceCommand.setRequester(Requester.sample());
        return addManagingWorkspaceCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
