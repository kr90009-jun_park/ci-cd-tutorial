package io.naraplatform.metro.domain.constant;

public class MetroExceptionMessage {
    //
    public static final String failedToRegisterCastellanForPavilionsManager = "failed_to_register_castellan_for_pavilions_manager";
    public static final String failedToRegisterCitizenForPavilionsManager = "failed_to_register_citizen_for_pavilions_manager";
    public static final String failedToModifyLoginUser = "failed_to_modify_login_user";
    public static final String failedToRemoveLoginUser = "failed_to_remove_login_user";
    public static final String failedToRegisterUserWorkspace = "failed_to_register_user_workspace";
    public static final String cannotUpdateEntityField = "cannot_update_entity_field";
    public static final String emailIsAlreadyRegistered = "email_is_already_registered";
    public static final String cannotFindEntityById = "cannot_find_entity_by_id";
    public static final String cannotChangeCitizenStateToPrevious = "cannot_change_citizen_state_to_previous";
    public static final String failedToRegisterLoginUser = "failed_to_Register_login_user";
    public static final String citizenAlreadyRegistered = "citizen_already_registered";
    public static final String secretCodeDoesNotMatch = "secret_code_does_not_match";
    public static final String citizenSequenceGeneratorMaximumRetryCountExceeded = "citizen_sequence_generator_maximum_retry_count_exceeded";
    public static final String cannotFindCitizenIdentityByMetroIdAndEmail = "cannot_find_citizen_identity_by_metro_id_and_email";
    public static final String cannotChangeMetroStateToPrevious = "cannot_change_metro_state_to_previous";
    public static final String cannotChangeNaraStateToPrevious = "cannot_change_nara_state_to_previous";

}
