package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterPreliminaryCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String name;
    private String email;
    private String usid;

    // Auto Setup
    private String encryptedPassword;
    private boolean activating;
    private String castellanId;
//
//    public static RegisterPreliminaryCommand fromRegisterCommand(RegisterPreliminaryCitizenCommand command) {
//        //
//        RegisterPreliminaryCommand cmd = new RegisterPreliminaryCommand();
//        cmd.setMetroId(command.getMetroId());
//        cmd.setMetroName(command.getMetroName());
//        cmd.setName(command.getName());
//        cmd.setEmail(command.getEmail());
//        cmd.setUsid(command.getUsid());
//        cmd.setActivating(false);
//        cmd.setCastellanId(null);
//
//        return cmd;
//    }

    public static RegisterPreliminaryCommand afterRegisterCastellan(RegisterPreliminaryCitizenCommand command) {
        //
        RegisterPreliminaryCommand cmd = new RegisterPreliminaryCommand();
        cmd.setMetroId(command.getMetroId());
//        cmd.setName(command.getName());
        cmd.setEmail(command.getEmail());
//        cmd.setUsid(command.getUsid());
        cmd.setActivating(false);
//        cmd.setCastellanId(command.getCastellanId());

        return cmd;
    }
//
//    public static RegisterPreliminaryCommand fromSetupCommand(SetupPreliminaryCitizenCommand command) {
//        //
//        RegisterPreliminaryCommand cmd = new RegisterPreliminaryCommand();
//        cmd.setMetroId(command.getMetroId());
//        cmd.setMetroName(command.getMetroName());
//        cmd.setName(command.getName());
//        cmd.setEmail(command.getEmail());
//        cmd.setUsid(command.getUsid());
//        cmd.setEncryptedPassword(command.getEncryptedPassword());
//        cmd.setActivating(command.isActivating());
//        cmd.setCastellanId(null);
//
//        return cmd;
//    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterPreliminaryCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterPreliminaryCommand.class);
    }

    public static RegisterPreliminaryCommand sample() {
        //
        return sample(5);
    }

    public static RegisterPreliminaryCommand sample(int sequence) {
        //
        RegisterPreliminaryCommand command = new RegisterPreliminaryCommand(
                "nea-m" + sequence,
                "Steve Jobs",
                "jobs@nextree.io",
                UUID.randomUUID().toString(),
                "",
                false,
                UUID.randomUUID().toString());
        command.setRequester(Requester.sample());
        return command;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
