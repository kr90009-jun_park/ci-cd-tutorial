package io.naraplatform.metro.domain.aggregate.castellan.entity;

import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.domain.granule.Email;
import io.naradrama.prologue.domain.granule.EmailList;
import io.naradrama.prologue.domain.granule.Phone;
import io.naradrama.prologue.domain.granule.PhoneList;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.domain.lang.LangStrings;
import io.naradrama.prologue.util.exception.NaraException;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Locale;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Castellan extends DomainEntity implements DomainAggregate {
    // command and read model at the same time
    //
    private EmailList emails;
    private LangStrings names;
    private PhoneList phones;
    private long time;

    public Castellan(String id) {
        //
        super(id);
    }

    public Castellan(String id, Email email, LangString name) {
        //
        super(id);
        this.emails = new EmailList(email);
        this.names = LangStrings.newString(name);
        this.time = System.currentTimeMillis();
    }

    public Castellan(String id, Email email, LangStrings names) {
        //
        super(id);
        this.emails = new EmailList(email);
        this.names = names;
        this.time = System.currentTimeMillis();
    }

    public static Castellan fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Castellan.class);
    }

    public String genDisplayName() {
        //
        return names.genString();
    }

    public boolean hasEmail(String email) {
        //
        return emails.contains(email);
    }

    public boolean hasPhone(Phone phone) {
        //
        return phones.contains(phone.genFullNumber());
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.list()) {
            //
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "emails":
                    this.emails = EmailList.fromJson(value);
                    break;
                case "names":
                    this.names = LangStrings.fromJson(value);
                    break;
                case "phones":
                    this.phones = PhoneList.fromJson(value);
                    break;

                default:
                    throw new NaraException(MetroExceptionMessage.cannotUpdateEntityField, this.getClass().getSimpleName(), nameValue.getName());
            }
        }
    }

    public static Castellan sample() {
        //

        Email email = Email.newPrimary("steve@gmail.com");
        email.setVerified(true);

        LangString name = LangString.newString(Locale.KOREA.getLanguage(), "Steve Jobs");

        Castellan sample = new Castellan(
                UUID.randomUUID().toString(), email, name);

        sample.setPhones(PhoneList.sample());

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
