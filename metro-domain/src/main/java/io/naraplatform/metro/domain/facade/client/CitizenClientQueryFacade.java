package io.naraplatform.metro.domain.facade.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentitiesByIdsQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityByMetroIdAndEmailQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityQuery;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="query.CitizenQueryProto",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CitizenClientQueryFacade {
    //
    FindCitizenIdentityQuery findCitizenIdentity(FindCitizenIdentityQuery query, CommandIdentity callerCommandIdentity);
    FindCitizenIdentityByMetroIdAndEmailQuery findCitizenIdentityByMetroIdAndEmail(FindCitizenIdentityByMetroIdAndEmailQuery query, CommandIdentity callerCommandIdentity);
    FindCitizenIdentitiesByIdsQuery findCitizenIdentitiesByIds(FindCitizenIdentitiesByIdsQuery query, CommandIdentity callerCommandIdentity);
}
