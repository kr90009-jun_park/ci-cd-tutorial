package io.naraplatform.metro.domain.aggregate.nara.entity.settings;

import io.naradrama.prologue.domain.ddd.ValueObject;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.domain.lang.LangStrings;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class NaraLanguageSettings implements ValueObject {
    //
    private String baseLanguage;
    private Locale locale;
    private List<String> supportLanguages;
    private LangStrings langNames;

    public NaraLanguageSettings(Locale locale,
                                String naraName) {
        //
        this.baseLanguage = locale.getLanguage();
        this.locale = locale;
        this.supportLanguages = new ArrayList<>();
        this.supportLanguages.add(baseLanguage);
        this.langNames = LangStrings.newString(new LangString(baseLanguage, naraName));
    }

    public NaraLanguageSettings(String baseLanguage, Locale locale, List<String> supportLanguages, String naraName) {
        //
        this.baseLanguage = baseLanguage;
        this.locale = locale;
        this.supportLanguages = supportLanguages;
        this.langNames = langNames;
        this.langNames = LangStrings.newString(new LangString(baseLanguage, naraName));
    }

    public NaraLanguageSettings(String baseLanguage, Locale locale, List<String> supportLanguages, LangStrings langNames) {
        //
        this.baseLanguage = baseLanguage;
        this.locale = locale;
        this.supportLanguages = supportLanguages;
        this.langNames = langNames;
    }

    public String toString() {
        //
        return toJson();
    }

    public static NaraLanguageSettings fromJson(String json) {
        //
        return JsonUtil.fromJson(json, NaraLanguageSettings.class);
    }

    public static NaraLanguageSettings sample() {
        //
        Locale locale = Locale.KOREA;
        String name = "퍼블릭나라플랫폼";

        return new NaraLanguageSettings(
                locale,
                name
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
