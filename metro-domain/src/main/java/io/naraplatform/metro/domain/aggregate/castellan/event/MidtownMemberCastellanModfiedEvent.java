package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MidtownMemberCastellanModfiedEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private Member member;
    private String previousEmail;

    public MidtownMemberCastellanModfiedEvent(Castellan castellan, CommandIdentity commandIdentity, Member member, String previousEmail) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.member = member;
        this.previousEmail = previousEmail;
    }
}
