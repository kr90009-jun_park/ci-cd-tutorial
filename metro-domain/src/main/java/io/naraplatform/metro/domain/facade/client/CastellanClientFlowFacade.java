package io.naraplatform.metro.domain.facade.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="command.ManagerCastellanCommandProto",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CastellanClientFlowFacade {
    //
    RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellan(RegisterPavilionManagerCastellanCommand command, CommandIdentity callerCommandIdentity);
}
