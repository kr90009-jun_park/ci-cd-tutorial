package io.naraplatform.metro.domain.aggregate.castellan.querymodel;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.domain.granule.Email;
import io.naradrama.prologue.domain.granule.EmailList;
import io.naradrama.prologue.domain.patron.AudienceKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CastellanEmail extends DomainEntity {
    //
    private String email;           // index and query key
    private String displayName;
    private String castellanId;     // key for Castellan

    public CastellanEmail (String id) {
        //
        super(id);
    }

    public CastellanEmail(String email,
                          String displayName,
                          String castellanId) {
        //
        super();
        this.email = email;
        this.displayName = displayName;
        this.castellanId = castellanId;
    }

    public static List<CastellanEmail> newInstances(Castellan castellan, AudienceKey audienceKey) {
        //
        List<CastellanEmail> castellanEmails = new ArrayList<>();
        EmailList emails = castellan.getEmails();
        for(Email email : emails.getEmails()) {
            String emailString = email.getEmail();
            CastellanEmail castellanEmail = new CastellanEmail(
                    emailString,
                    castellan.genDisplayName(),
                    castellan.getId()
            );
            castellanEmails.add(castellanEmail);
        }

        return castellanEmails;
    }

    public static CastellanEmail sample() {
        //
        Castellan castellan = Castellan.sample();
        return new CastellanEmail(
                castellan.getEmails().genPrimaryEmail(),
                castellan.genDisplayName(),
                castellan.getId()
        );
    }

    public static List<CastellanEmail> samples() {
        //
        Castellan castellan = Castellan.sample();
        return newInstances(castellan, AudienceKey.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(samples());
    }
}
