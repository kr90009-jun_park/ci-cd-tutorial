package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RemoveCitizenCommand extends CqrsUserCommand {
    //
    private String citizenId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RemoveCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RemoveCitizenCommand.class);
    }

    public static RemoveCitizenCommand sample() {
        //
        RemoveCitizenCommand removeCitizenCommand = new RemoveCitizenCommand("r2p8@nea-m5");
        removeCitizenCommand.setRequester(Requester.sample());
        return removeCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
