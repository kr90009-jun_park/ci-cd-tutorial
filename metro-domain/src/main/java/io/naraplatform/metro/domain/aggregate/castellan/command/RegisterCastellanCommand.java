package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterCastellanCommand extends CqrsUserCommand {
    //
    private String email;
    private String name;
    private String metroId;
    private String encryptedPassword;

    @Override
    public String toString() {
        //
        return toJson();
    }
}
