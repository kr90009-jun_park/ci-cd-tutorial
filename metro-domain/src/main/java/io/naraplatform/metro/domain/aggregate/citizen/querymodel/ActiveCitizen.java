package io.naraplatform.metro.domain.aggregate.citizen.querymodel;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ActiveCitizen extends AbstractCitizen {
    //
    public ActiveCitizen(String id) {
        //
        super(id);
    }

    public ActiveCitizen(CitizenIdentity identity) {
        //
        super(identity);
    }

    public ActiveCitizen(PreliminaryCitizen preliminaryCitizen) {
        //
        super(preliminaryCitizen);
        getIdentity().setState(CitizenState.Active);

    }

    public ActiveCitizen(DormantCitizen dormantCitizen) {
        //
        super(dormantCitizen);
        getIdentity().setState(CitizenState.Active);
    }

    public String toString() {
        //
        return toJson();
    }

    public static ActiveCitizen fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ActiveCitizen.class);
    }

    public static ActiveCitizen sample() {
        //
        PreliminaryCitizen preliminaryCitizen = PreliminaryCitizen.sample();

        ActiveCitizen sample = new ActiveCitizen(
                preliminaryCitizen
        );

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
