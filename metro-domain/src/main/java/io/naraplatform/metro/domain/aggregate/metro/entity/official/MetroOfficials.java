package io.naraplatform.metro.domain.aggregate.metro.entity.official;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naradrama.prologue.domain.ddd.ValueObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class MetroOfficials implements ValueObject {
    //
    private String primaryId;
    private List<String> secondaryIds;

    public MetroOfficials(String primaryId) {
        //
        this.primaryId = primaryId;
    }

    public void add(String citizenId) {
        //
        if (primaryId == null) {
            primaryId = citizenId;
            return;
        }
        if (secondaryIds == null) {
            secondaryIds = new ArrayList<>();
        }
        secondaryIds.add(citizenId);
    }

    public void assignToPrimary(String citizenId) {
        //
        if(primaryId.equals(citizenId)) {
            return;
        }
        secondaryIds.add(primaryId);
        secondaryIds.remove(citizenId);

        primaryId = citizenId;
    }

    public void remove(String citizenId) {
        //
        secondaryIds.remove(citizenId);
    }

    public static MetroOfficials sample() {
        //
        MetroOfficials metroOfficials = new MetroOfficials();
        metroOfficials.setPrimaryId(CitizenIdentity.sample().getId());
        metroOfficials.setSecondaryIds(Arrays.asList(CitizenIdentity.sample2().getId()));

        return metroOfficials;
    }

}
