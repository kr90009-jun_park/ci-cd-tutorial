package io.naraplatform.metro.domain.aggregate.citizen.event;

import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MidtownMemberCitizenModifiedEvent extends CqrsDomainEvent {
    //
    private String citizenId;
    private String castellanId;
    private Member member;
    private String previousEmail;

    public MidtownMemberCitizenModifiedEvent(Citizen citizen, CommandIdentity commandIdentity, String castellanId, Member member, String previousEmail) {
        //
        super(citizen.genEntityIdName(), commandIdentity);
        this.citizenId = citizen.getId();
        this.castellanId = castellanId;
        this.member = member;
        this.previousEmail = previousEmail;
    }
}
