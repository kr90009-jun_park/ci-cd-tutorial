package io.naraplatform.metro.domain.aggregate.metro.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RemoveMetroCommand extends CqrsUserCommand {
    //
    private String metroId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RemoveMetroCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RemoveMetroCommand.class);
    }

    public static RemoveMetroCommand sample() {
        //
        RemoveMetroCommand removeMetroCommand = new RemoveMetroCommand("nea-m5");
        removeMetroCommand.setRequester(Requester.sample());
        return removeMetroCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
