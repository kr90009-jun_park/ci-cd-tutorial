package io.naraplatform.metro.domain.aggregate.nara.event;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NaraModifedEvent extends CqrsDomainEvent {
    //
    private String naraId;
    private NameValueList nameValueList;

    public NaraModifedEvent(IdName aggregate, CommandIdentity commandIdentity, String naraId, NameValueList nameValueList) {
        //
        super(aggregate, commandIdentity);
        this.naraId = naraId;
        this.nameValueList = nameValueList;
    }
}
