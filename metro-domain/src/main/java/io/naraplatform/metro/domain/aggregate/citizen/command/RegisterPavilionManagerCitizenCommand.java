package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterPavilionManagerCitizenCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String pavilionManagerEmail;
    private String pavilionManagerName;
    private String pavilionManagerEncryptedPassword;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterPavilionManagerCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterPavilionManagerCitizenCommand.class);
    }

    public static RegisterPavilionManagerCitizenCommand sample() {
        //
        return sample(8);
    }

    public static RegisterPavilionManagerCitizenCommand sample(int sequence) {
        //

        RegisterPavilionManagerCitizenCommand registerPavilionManagerCitizenCommand = new RegisterPavilionManagerCitizenCommand(
                "ne1-m1",
                "Steve",
                "jobs@nextree.io",
                "$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei");
        registerPavilionManagerCitizenCommand.setRequester(Requester.sample());
        return registerPavilionManagerCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
