package io.naraplatform.metro.domain.aggregate.citizen.event;

import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActiveCitizenRegisteredEvent extends CqrsDomainEvent {
    //
    private String citizenId;
    private String metroId;
    private String name;
    private String email;
    private String castellanId;
    private CitizenSettings settings;

    public ActiveCitizenRegisteredEvent(Citizen citizen, CommandIdentity commandIdentity, String metroId, String name, String email, String castellanId, CitizenSettings settings) {
        //
        super(citizen.genEntityIdName(), commandIdentity);
        this.citizenId = citizen.getId();
        this.metroId = metroId;
        this.name = name;
        this.email = email;
        this.castellanId = castellanId;
        this.settings = settings;
    }
}
