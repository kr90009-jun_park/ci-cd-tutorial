package io.naraplatform.metro.domain.aggregate.castellan.query;

import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naradrama.prologue.domain.cqrs.query.CqrsUserQuery;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindCastellanEmailByEmailQuery extends CqrsUserQuery<CastellanEmail> {
    //
    private String email;
}
