package io.naraplatform.metro.domain.aggregate.citizen.entity.settings;

import io.naradrama.prologue.domain.ddd.ValueObject;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.domain.lang.LangStrings;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class CitizenLanguageSettings implements ValueObject {
    //
    private String baseLanguage;
    //private Locale locale;
    private List<String> supportLanguages;
    private LangStrings names;
//    private LangStrings nicknames;
//    private LangStrings titles;
//    private LangStrings teamNames;

    public CitizenLanguageSettings(Locale locale, LangString name) {
        //
        setLanguages(locale);
        this.names = LangStrings.newString(name);
//        this.nicknames = LangStrings.emptyString();
//        this.titles = LangStrings.emptyString();
//        this.teamNames = LangStrings.emptyString();
    }

    public CitizenLanguageSettings(Locale locale, String name) {
        //
        setLanguages(locale);
        this.names = LangStrings.newString(locale.getLanguage(), name);
    }

    private void setLanguages(Locale locale) {
        //
        this.baseLanguage = locale.getLanguage();
        //this.locale = locale;
        this.supportLanguages = new ArrayList<>();
        this.supportLanguages.add(baseLanguage);
    }

    public String toString() {
        //
        return toJson();
    }

    public static CitizenLanguageSettings fromJson(String json) {
        //
        return JsonUtil.fromJson(json, CitizenLanguageSettings.class);
    }

    public String genName() {
        //
        return names.genString(baseLanguage);
    }

    public String genName(String language) {
        //
        return names.genString(language);
    }

//    public String getNickname() {
//        //
//        return nicknames.getString(baseLanguage);
//    }
//
//    public String getNickname(String language) {
//        //
//        return nicknames.getString(language);
//    }
//
//    public String getTitle() {
//        //
//        return titles.getString(baseLanguage);
//    }
//
//    public String getTitle(String language) {
//        //
//        return titles.getString(language);
//    }
//
//    public String getTeamName() {
//        //
//        return teamNames.getString(baseLanguage);
//    }
//
//    public String getTeamName(String language) {
//        //
//        return teamNames.getString(language);
//    }

    public static CitizenLanguageSettings sample() {
        //
        Locale locale = Locale.KOREA;

        CitizenLanguageSettings sample = new CitizenLanguageSettings(
                locale,
                "김철수"
        );

        //sample.getTitles().addString("ko", "부장").addString("en", "Manager");

        return sample;
    }

    public static CitizenLanguageSettings sample2() {
        //
        Locale locale = Locale.KOREA;

        CitizenLanguageSettings sample = new CitizenLanguageSettings(
                locale,
                "이영미"
        );

        //sample.getTitles().addString("ko", "과장").addString("en", "Manager");

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
