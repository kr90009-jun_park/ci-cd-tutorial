package io.naraplatform.metro.domain.aggregate.citizen.query;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naradrama.prologue.domain.OffsetElementList;
import io.naradrama.prologue.util.json.JsonSerializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OffsetCitizenIdentitiesResponse implements JsonSerializable {
    //
    private OffsetElementList<CitizenIdentity> identities;
}
