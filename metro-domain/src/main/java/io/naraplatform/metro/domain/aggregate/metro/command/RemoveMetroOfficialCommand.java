package io.naraplatform.metro.domain.aggregate.metro.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RemoveMetroOfficialCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String citizenId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RemoveMetroOfficialCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RemoveMetroOfficialCommand.class);
    }

    public static RemoveMetroOfficialCommand sample() {
        //
        RemoveMetroOfficialCommand removeMetroOfficialCommand = new RemoveMetroOfficialCommand("nea-m5", "r2p11@nea-m5");
        removeMetroOfficialCommand.setRequester(Requester.sample());
        return removeMetroOfficialCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
