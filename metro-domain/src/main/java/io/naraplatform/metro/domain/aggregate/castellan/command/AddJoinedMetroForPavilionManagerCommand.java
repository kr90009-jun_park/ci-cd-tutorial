package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddJoinedMetroForPavilionManagerCommand extends CqrsUserCommand {
    //
    private String castellanId;
    private String joinedMetroId;
    private String usid;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static AddJoinedMetroForPavilionManagerCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AddJoinedMetroForPavilionManagerCommand.class);
    }

    public static AddJoinedMetroForPavilionManagerCommand sample() {
        //
        return sample(5);
    }

    public static AddJoinedMetroForPavilionManagerCommand sample(int sequence) {
        //
        AddJoinedMetroForPavilionManagerCommand addJoinedMetroForPavilionManagerCommand = new AddJoinedMetroForPavilionManagerCommand(
                UUID.randomUUID().toString(),
                "nea-m" + sequence,
                UUID.randomUUID().toString());
        addJoinedMetroForPavilionManagerCommand.setRequester(Requester.sample());
        return addJoinedMetroForPavilionManagerCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
