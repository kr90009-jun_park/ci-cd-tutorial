package io.naraplatform.metro.domain.facade.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.drama.AuthorizedRole;
import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="command.NaraCommandProto",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface NaraClientCommandFacade {
    //
    @AuthorizedRole(isPublic = true)
    RegisterNaraCommand registerNara(RegisterNaraCommand command, CommandIdentity callerCommandIdentity);
}
