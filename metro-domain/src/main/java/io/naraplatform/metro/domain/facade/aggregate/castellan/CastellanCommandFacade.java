package io.naraplatform.metro.domain.facade.aggregate.castellan;

import io.naradrama.prologue.domain.drama.AuthorizedRole;
import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="CastellanCommand",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CastellanCommandFacade {
    //
    @AuthorizedRole(isPublic = true)
    RegisterManagerCastellanCommand registerManagerCastellan(RegisterManagerCastellanCommand registerManagerCastellanCommand);
    @AuthorizedRole(isPublic = true)
    RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellan(RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellanCommand);
}
