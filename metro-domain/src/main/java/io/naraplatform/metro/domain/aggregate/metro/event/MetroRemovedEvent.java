package io.naraplatform.metro.domain.aggregate.metro.event;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetroRemovedEvent extends CqrsDomainEvent {
    //
    private String metroId;

    public MetroRemovedEvent(IdName aggregate, CommandIdentity commandIdentity, String metroId) {
        //
        super(aggregate, commandIdentity);
        this.metroId = metroId;
    }
}
