package io.naraplatform.metro.domain.aggregate.citizen.event;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PavilionManagerCitizenRegisteredEvent extends CqrsDomainEvent {
    //
    private String citizenId;
    private String metroId;
    private String name;
    private String email;
    private String encryptedPassword;
    private String usid;
    private String castellanId;
    private CitizenSettings settings;

    public PavilionManagerCitizenRegisteredEvent(Citizen citizen,
                                                 CommandIdentity commandIdentity,
                                                 String metroId,
                                                 String name,
                                                 String email,
                                                 String encryptedPassword,
                                                 String usid,
                                                 String castellanId,
                                                 CitizenSettings settings) {
        //
        super(citizen.genEntityIdName(), commandIdentity);
        this.citizenId = citizen.getId();
        this.metroId = metroId;
        this.name = name;
        this.email = email;
        this.encryptedPassword = encryptedPassword;
        this.usid = usid;
        this.castellanId = castellanId;
        this.settings = settings;
    }
}
