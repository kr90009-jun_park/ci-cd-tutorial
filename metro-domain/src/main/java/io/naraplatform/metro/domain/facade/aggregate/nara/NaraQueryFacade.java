package io.naraplatform.metro.domain.facade.aggregate.nara;

import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.nara.query.FindNaraQuery;
import io.naraplatform.metro.domain.aggregate.nara.query.FindNarasQuery;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="NaraQuery",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface NaraQueryFacade {
    //
    FindNaraQuery findNara(FindNaraQuery findNaraQuery);
    FindNarasQuery findNaras(FindNarasQuery findNarasQuery);
}
