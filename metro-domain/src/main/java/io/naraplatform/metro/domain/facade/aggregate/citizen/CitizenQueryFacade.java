package io.naraplatform.metro.domain.facade.aggregate.citizen;

import io.naradrama.prologue.domain.drama.ServiceFeature;
import io.naraplatform.metro.domain.aggregate.citizen.query.*;
import io.naraplatform.share.domain.constant.NaraPlatformBasicRoles;

@ServiceFeature(
        name="CitizenQuery",
        editions = {"Standard"},
        authorizedRoles = {
                NaraPlatformBasicRoles.StationManager,
                NaraPlatformBasicRoles.SquareManager,
                NaraPlatformBasicRoles.PavilionManager,
                NaraPlatformBasicRoles.CineroomManager
        }
)
public interface CitizenQueryFacade {
    //
    FindCitizenQuery findCitizen(FindCitizenQuery findCitizenQuery);
    FindCitizenIdentityQuery findCitizenIdentity(FindCitizenIdentityQuery findCitizenIdentityQuery);
    FindCitizenIdentityByMetroIdAndEmailQuery findCitizenIdentityByMetroIdAndEmail(
            FindCitizenIdentityByMetroIdAndEmailQuery findCitizenIdentityByMetroIdAndEmailQuery);

    FindCitizenIdentitiesQuery findCitizenIdentities(FindCitizenIdentitiesQuery findCitizenIdentitiesQuery);
    FindCitizensByMetroIdAndStateQuery findCitizensByMetroIdAndState(FindCitizensByMetroIdAndStateQuery findCitizensByMetroIdAndStateQuery);

    FindCitizenIdentitiesByStateQuery findCitizenIdentitiesByState(
            FindCitizenIdentitiesByStateQuery findCitizenIdentitiesByStateQuery);
    FindCitizenIdentitiesByIdsQuery findCitizenIdentitiesByIds(
            FindCitizenIdentitiesByIdsQuery findCitizenIdentitiesByIdsQuery);
    FindCitizenCountsQuery countCitizensByMetroId(FindCitizenCountsQuery findCitizenCountsQuery);

    FindCitizenIdentitiesByMetroIdAndStateAndKeywordQuery findCitizenIdentitiesByMetroIdAndStateAndKeyword(
            FindCitizenIdentitiesByMetroIdAndStateAndKeywordQuery findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery);
}
