package io.naraplatform.metro.domain.aggregate.castellan.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterManagerCastellanCommand extends CqrsUserCommand {
    //
    private String managerId;
    private String email;
    private LangString name;
    private String encryptedPassword;
    private boolean stationManager;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterManagerCastellanCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterManagerCastellanCommand.class);
    }

    public static RegisterManagerCastellanCommand sample() {
        //
        return sample(5);
    }

    public static RegisterManagerCastellanCommand sample(int sequence) {
        //
        RegisterManagerCastellanCommand registerManagerCastellanCommand = new RegisterManagerCastellanCommand(
                "r2p8@nea-m" + sequence,
//                "nea-m" + sequence,
//                "Nextree Metro",
                sequence + "jobs@nextree.io",
                LangString.sample(),
                "$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei",
                false
        );
        registerManagerCastellanCommand.setRequester(Requester.sample());
        return registerManagerCastellanCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
