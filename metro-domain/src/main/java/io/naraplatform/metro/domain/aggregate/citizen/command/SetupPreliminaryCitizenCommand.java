package io.naraplatform.metro.domain.aggregate.citizen.command;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SetupPreliminaryCitizenCommand extends CqrsUserCommand {
    //
    private String metroId;
    private String name;
    private String email;
    private String usid;

    private String encryptedPassword;
    private boolean activating;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static SetupPreliminaryCitizenCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, SetupPreliminaryCitizenCommand.class);
    }

    public static SetupPreliminaryCitizenCommand sample() {
        //
        return sample(1);
    }

    public static SetupPreliminaryCitizenCommand sample(int sequence) {
        //
        SetupPreliminaryCitizenCommand setupPreliminaryCitizenCommand = new SetupPreliminaryCitizenCommand(
                "nea-m" + sequence,
                "Park Metro",
                sequence + "metro@nextree.io",
                UUID.randomUUID().toString(),
                "$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei",
                true);
        setupPreliminaryCitizenCommand.setRequester(Requester.sample());
        return setupPreliminaryCitizenCommand;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
