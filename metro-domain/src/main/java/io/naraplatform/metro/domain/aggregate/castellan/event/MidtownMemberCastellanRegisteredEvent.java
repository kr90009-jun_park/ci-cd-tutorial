package io.naraplatform.metro.domain.aggregate.castellan.event;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MidtownMemberCastellanRegisteredEvent extends CqrsDomainEvent {
    //
    private String castellanId;
    private String citizenId;
    private Member member;

    public MidtownMemberCastellanRegisteredEvent(Castellan castellan, CommandIdentity commandIdentity, String citizenId, Member member) {
        //
        super(castellan.genEntityIdName(), commandIdentity);
        this.castellanId = castellan.getId();
        this.citizenId = citizenId;
        this.member = member;
    }
}
