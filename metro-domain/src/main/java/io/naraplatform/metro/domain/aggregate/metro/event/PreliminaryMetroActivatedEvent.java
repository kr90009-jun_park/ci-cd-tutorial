package io.naraplatform.metro.domain.aggregate.metro.event;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreliminaryMetroActivatedEvent extends CqrsDomainEvent {
    //
    private String metroId;
    private String secretCode;
    private String officialId;
    private String officialName;
    private String officialEmail;
    private String officialPassword;

    public PreliminaryMetroActivatedEvent(IdName aggregate, CommandIdentity commandIdentity, String metroId, String secretCode, String officialId, String officialName, String officialEmail, String officialPassword) {
        //
        super(aggregate, commandIdentity);
        this.metroId = metroId;
        this.secretCode = secretCode;
        this.officialId = officialId;
        this.officialName = officialName;
        this.officialEmail = officialEmail;
        this.officialPassword = officialPassword;
    }
}
