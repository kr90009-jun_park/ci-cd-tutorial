package io.naraplatform.metro.command.aggregate;

import io.naradrama.prologue.domain.patron.AudienceKey;
import io.naradrama.prologue.domain.workspace.Workspace;
import io.naraplatform.envoy.context.DramaContext;
import io.naraplatform.envoy.context.DramaRequest;
import io.naraplatform.metro.command.MetroClientTestApplication;
import io.naraplatform.metro.command.service.NaraService;
import io.naraplatform.metro.domain.aggregate.nara.command.ModifyNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.ModifyPublicServantCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetroClientTestApplication.class)
public class NaraCommandLogicTest {
    //
    @Autowired
    private NaraService naraService;

    private static RegisterNaraCommand registerNaraCommand;
    private static boolean initialized;
    private static String initialNaraId;

    private void setCurrentDramaRequest() {
        //
        List<Workspace> workspaces = new ArrayList<>();
        workspaces.add(new Workspace("ne1-m1-c2", "Station Manager Team", "1@ne1-m1-c2"));

        AudienceKey audienceKey = AudienceKey.sample();
        DramaContext.setCurrentRequest(new DramaRequest(
                audienceKey.getKeyString(),
                audienceKey.genCineroomKeyString(),
                Arrays.asList("Admin,User"),
                workspaces,
                "manager@nextree.io",
                "manager"
        ));
    }

    @Before
    public void before() {
        //
        log.debug("\n## initialized {}", initialized);
        if (initialized) {
            return;
        }
        setCurrentDramaRequest();

        registerNaraCommand = RegisterNaraCommand.sample();
        Nara nara = naraService.registerNara(registerNaraCommand.getName(), registerNaraCommand.getType(),
                registerNaraCommand.getZoneId(),
                registerNaraCommand.getBaseLanguage(), registerNaraCommand.getSupportLanguages(),
                registerNaraCommand.getPublicServantName(), registerNaraCommand.getPublicServantEmail(),
                registerNaraCommand.genCommandIdentity());
        initialNaraId = nara.getId();

        log.debug("\n## in before() register Metro {}", nara.toPrettyJson());
        initialized = true;

        log.debug("\n## initialized {}", initialized);
    }

    @Test
    public void registerNaraTest() {
        //
        try{ Thread.sleep(100); } catch(Exception e){}
        RegisterNaraCommand command = RegisterNaraCommand.sample();
        command.setName("Nextree Nara");
        Nara nara = naraService.registerNara(command.getName(), command.getType(),
                command.getZoneId(),
                command.getBaseLanguage(), command.getSupportLanguages(),
                command.getPublicServantName(), command.getPublicServantEmail(),
                command.genCommandIdentity());
        log.debug("\n## CommandStringResponse in registerNaraTest() {}", nara.toPrettyJson());
        assertNotNull(nara);
    }

    @Test
    public void modifyNaraTest() {
        //
        ModifyNaraCommand command = ModifyNaraCommand.sample();
        command.setNaraId(initialNaraId);
        Nara nara = naraService.modifyNara(command.getNaraId(), command.getNameValues(),
                command.genCommandIdentity());
        log.debug("\n## CommandStringResponse in modifyNaraTest() {}", nara.toPrettyJson());
        assertNotNull(nara);
    }

    @Test
    public void deactivateNaraTest() {
        //
        try{ Thread.sleep(100); } catch(Exception e){}
        RegisterNaraCommand registerCommand = RegisterNaraCommand.sample();
        registerCommand.setName("Nextree Test Nara");
        Nara nara = naraService.registerNara(registerCommand.getName(), registerCommand.getType(),
                registerCommand.getZoneId(),
                registerCommand.getBaseLanguage(), registerCommand.getSupportLanguages(),
                registerCommand.getPublicServantName(), registerCommand.getPublicServantEmail(),
                registerCommand.genCommandIdentity());
        log.debug("\n## CommandStringResponse in deactivateNaraTest() {}", nara.toPrettyJson());
        String naraId = nara.getId();


        Nara nara1 = naraService.deactivateNara(naraId, registerCommand.genCommandIdentity());
        log.debug("\n## CommandVoidResponse in deactivateNaraTest() {}", nara1.toPrettyJson());
        assertNotNull(nara1);
    }

    @Test
    public void modifyPublicServantTest() {
        //
        ModifyPublicServantCommand command = ModifyPublicServantCommand.sample();
        command.setNaraId(initialNaraId);
        Nara nara = naraService.modifyPublicServant(initialNaraId, command.getNameValues(),
                command.genCommandIdentity());
        log.debug("\n## CommandVoidResponse in deactivateNaraTest() {}", nara.toPrettyJson());
        assertNotNull(nara);
    }
}
