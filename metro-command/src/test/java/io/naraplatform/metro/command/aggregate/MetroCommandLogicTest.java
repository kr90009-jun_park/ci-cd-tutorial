package io.naraplatform.metro.command.aggregate;

import io.naradrama.prologue.domain.patron.AudienceKey;
import io.naradrama.prologue.domain.workspace.Workspace;
import io.naraplatform.envoy.context.DramaContext;
import io.naraplatform.envoy.context.DramaRequest;
import io.naraplatform.metro.command.MetroClientTestApplication;
import io.naraplatform.metro.command.service.MetroService;
import io.naraplatform.metro.domain.aggregate.metro.command.ActivateMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.DeactivateMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.RegisterMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naradrama.prologue.domain.cqrs.Requester;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetroClientTestApplication.class)
public class MetroCommandLogicTest {
    //
    @Autowired
    private MetroService metroService;

    private static RegisterMetroCommand registerMetroCommand;
    private static boolean initialized;
    private static String initialMetroId;

    private void setCurrentDramaRequest() {
        //
        List<Workspace> workspaces = new ArrayList<>();
        workspaces.add(new Workspace("ne1-m1-c2", "Station Manager Team", "1@ne1-m1-c2"));

        AudienceKey audienceKey = AudienceKey.sample();
        DramaContext.setCurrentRequest(new DramaRequest(
                audienceKey.getKeyString(),
                audienceKey.genCineroomKeyString(),
                Arrays.asList("Admin,User"),
                workspaces,
                "manager@nextree.io",
                "manager"
        ));
    }

    @Before
    public void before() {
        //
        log.debug("\n## initialized {}", initialized);
        if (initialized) {
            return;
        }
        setCurrentDramaRequest();

        registerMetroCommand = RegisterMetroCommand.sample();
        Metro metro = metroService.registerMetro(registerMetroCommand.getMetroId(), registerMetroCommand.getName(),
                registerMetroCommand.getOfficialId(), registerMetroCommand.genCommandIdentity());
        initialMetroId = metro.getId();

        log.debug("\n## in before() register Metro {}", metro.toPrettyJson());
        log.debug("\n## initial Metro Id {}", initialMetroId);
        initialized = true;

        log.debug("\n### initialized {}", initialized);
    }

    @Test
    public void registerMetroTest() {
        //
        RegisterMetroCommand command = new RegisterMetroCommand(
                "nea-m7",
                "Nextree Metro7",
                "r2p8@nea-m7");
        command.setRequester(Requester.sample());

        Metro metro = metroService.registerMetro(command.getMetroId(), command.getName(),
                command.getOfficialId(), command.genCommandIdentity());
        log.debug("\n## CommandStringResponse in registerMetroTest() {}", metro.toPrettyJson());
        assertNotNull(metro);
    }

    @Test
    public void activateMetroTest() {
        //
        ActivateMetroCommand command = new ActivateMetroCommand(initialMetroId);
        command.setRequester(Requester.sample());
        Metro metro = metroService.activateMetro(command.getMetroId(), command.genCommandIdentity());
        log.debug("\n## CommandVoidResponse in activateMetroTest() {}", metro.toPrettyJson());
        assertNotNull(metro);
//        assertTrue(response.getResultAsBoolean());
    }

    @Test
    public void metroStateTest() {
        //
        RegisterMetroCommand registerMetroCommand = new RegisterMetroCommand(
                "nea-m77",
                "Nextree Metro77",
                "r2p8@nea-m77");
        registerMetroCommand.setRequester(Requester.sample());

        Metro metro = metroService.registerMetro(registerMetroCommand.getMetroId(), registerMetroCommand.getName(),
                registerMetroCommand.getOfficialId(), registerMetroCommand.genCommandIdentity());
        log.debug("\n## registerResponse in metroStateTest() registerMetro {}", metro.toPrettyJson());
        String metroId = metro.getId();

        Metro metro1 = metroService.activateMetro(metroId, registerMetroCommand.genCommandIdentity());
        log.debug("\n## activatedResponse in metroStateTest() activateMetro {}", metro1.toPrettyJson());
        assertNotNull(metro1);

        DeactivateMetroCommand deactivateMetroCommand = new DeactivateMetroCommand(metroId);
        Metro metro2 = metroService.deactivateMetro(metroId, registerMetroCommand.genCommandIdentity());
        log.debug("\n## deactivatedResponse in metroStateTest() deactivateMetro {}", metro2.toPrettyJson());
        assertNotNull(metro2);

        Metro metro3 = metroService.removeMetro(metroId, registerMetroCommand.genCommandIdentity());
        log.debug("\n## removedResponse in metroStateTest() deactivateMetro {}", metro3.toPrettyJson());
//        assertTrue(removedResponse.getResultAsBoolean());
        assertNotNull(metro3);
    }

}
