package io.naraplatform.metro.command.flow;

import io.naradrama.prologue.domain.patron.AudienceKey;
import io.naradrama.prologue.domain.workspace.Workspace;
import io.naraplatform.envoy.context.DramaContext;
import io.naraplatform.envoy.context.DramaRequest;
import io.naraplatform.metro.command.MetroClientTestApplication;
import io.naraplatform.metro.command.service.CitizenService;
import io.naraplatform.metro.domain.aggregate.castellan.command.ModifyMidtownMemberCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterMidtownMemberCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RemoveMidtownMemberCastellanCommand;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterMidtownMemberCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetroClientTestApplication.class)
public class MidtownMemberCastellanFlowTest {
    //
    @Autowired
    private MidtownMemberCastellanFlow midtownMemberCastellanFlow;
    @Autowired
    private CitizenService citizenService;


    private static RegisterMidtownMemberCitizenCommand registerMidtownMemberCitizenCommand;
    private static RegisterMidtownMemberCastellanCommand registerMidtownMemberCastellanCommand;
    private static boolean initialized;
    private static String managerCastellanId;

    private void setCurrentDramaRequest() {
        //
        List<Workspace> workspaces = new ArrayList<>();
        workspaces.add(new Workspace("ne1-m1-c2", "Station Manager Team", "1@ne1-m1-c2"));

        AudienceKey audienceKey = AudienceKey.sample();
        DramaContext.setCurrentRequest(new DramaRequest(
                audienceKey.getKeyString(),
                audienceKey.genCineroomKeyString(),
                Arrays.asList("Admin,User"),
                workspaces,
                "manager@nextree.io",
                "manager"
        ));
    }

    @Before
    public void before() {
        //
        log.debug("\n## initialized {}", initialized);
        if (initialized) {
            return;
        }
        setCurrentDramaRequest();

//        registerMidtownMemberCastellanCommand = RegisterMidtownMemberCastellanCommand.sample(3);
        registerMidtownMemberCitizenCommand = RegisterMidtownMemberCitizenCommand.sample();
        RegisterMidtownMemberCitizenCommand response = midtownMemberCastellanFlow.registerMidtownMemberCitizen(registerMidtownMemberCitizenCommand);
        managerCastellanId = response.getResponse().getEntityIds().get(0);

        log.debug("\n## in before() register manager castellan {}", response.toPrettyJson());
        initialized = true;

        log.debug("\n## initialized {}", initialized);
    }

    @Test
    public void registerMidtownMemberCastellanTest() {
        //
        RegisterMidtownMemberCitizenCommand command = RegisterMidtownMemberCitizenCommand.sample();
        Member member = Member.sample();
        member.setEmail("register@nextree.io");
        command.setMember(member);
        RegisterMidtownMemberCitizenCommand response = midtownMemberCastellanFlow.registerMidtownMemberCitizen(
                command);
        log.debug("\n## registerMidtownMemberCastellanTest() {}", response);
        assertNotNull(response.getResponse());
    }

    @Test
    public void modifyMidtownMemberCastellanTest() {
        //
        ModifyMidtownMemberCastellanCommand command = ModifyMidtownMemberCastellanCommand.sample(3);
        command.setCastellanId(managerCastellanId);

        ModifyMidtownMemberCastellanCommand response = midtownMemberCastellanFlow.modifyMidtownMemberCastellan(command);
        log.debug("\n## modifyMidtownMemberCastellanTest() {}", response.toPrettyJson());
        assertNotNull(response.getResponse());
    }

    @Test
    public void removeMidtownMemberCastellanTest() {
        //
        RegisterMidtownMemberCitizenCommand registerCommand = RegisterMidtownMemberCitizenCommand.sample();

        Member member = Member.sample();
        member.setEmail("remover@nextree.io");
        registerCommand.setMember(member);
        RegisterMidtownMemberCitizenCommand response = midtownMemberCastellanFlow.registerMidtownMemberCitizen(registerCommand);
        log.debug("\n## removeMidtownMemberCastellanTest() {}", response);

        CitizenIdentity identity
                = citizenService.findCitizenIdentityByMetroIdAndEmail(member.getPavilionId(), member.getEmail());

        RemoveMidtownMemberCastellanCommand command = RemoveMidtownMemberCastellanCommand.sample();

        command.setMember(member);
        RemoveMidtownMemberCastellanCommand removeResponse = midtownMemberCastellanFlow.removeMidtownMemberCastellan(command);
        log.debug("\n## removeMidtownMemberCastellanTest() {}", removeResponse.toPrettyJson());
        assertNotNull(response.getResponse());
    }
}
