package io.naraplatform.metro.command.flow;

import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.patron.AudienceKey;
import io.naradrama.prologue.domain.workspace.Workspace;
import io.naraplatform.envoy.context.DramaContext;
import io.naraplatform.envoy.context.DramaRequest;
import io.naraplatform.metro.command.MetroClientTestApplication;
import io.naraplatform.metro.command.service.CitizenService;
import io.naraplatform.metro.domain.aggregate.citizen.command.*;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

// todo fixme
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetroClientTestApplication.class)
public class CitizenFlowTest {
    //
    @Autowired
    private CitizenFlow citizenFlow;
    private static boolean initialized;
    private static String initialCitizenId;

    @Autowired
    private CitizenService citizenService;

    private void setCurrentDramaRequest() {
        //
        List<Workspace> workspaces = new ArrayList<>();
        workspaces.add(new Workspace("ne1-m1-c2", "Station Manager Team", "1@ne1-m1-c2"));

        AudienceKey audienceKey = AudienceKey.sample();
        DramaContext.setCurrentRequest(new DramaRequest(
                audienceKey.getKeyString(),
                audienceKey.genCineroomKeyString(),
                Arrays.asList("Admin,User"),
                workspaces,
                "manager@nextree.io",
                "manager"
        ));
    }

    @Before
    public void before() {
        //
        log.debug("\n## initialized {}", initialized);
        if (initialized) {
            return;
        }
        initialized = true;
        setCurrentDramaRequest();

        if(StringUtils.isEmpty(initialCitizenId)){
            registerCitizenTest();
        }
    }

    @Test
    public void activatePreliminaryTest() {
        //

    }

    @Test
    public void activatePreliminaryByManagerTest() {
        //
        ActivatePreliminaryCitizenByManagerCommand activatePreliminaryCitizenByManagerCommand = new ActivatePreliminaryCitizenByManagerCommand(initialCitizenId, "1");
        activatePreliminaryCitizenByManagerCommand.setRequester(Requester.sample());

        ActivatePreliminaryCitizenByManagerCommand response = citizenFlow.activatePreliminaryByManager(activatePreliminaryCitizenByManagerCommand);
        assertNotNull(response.getResponse());
        assertNull(response.getFailureMessage());
    }


    @Test
    public void modifyCitizenTest() {
        //

        final String modifiedName = "modifiedName";

        ModifyCitizenCommand modifyCitizenCommand = new ModifyCitizenCommand();
        modifyCitizenCommand.setCitizenId(initialCitizenId);
        modifyCitizenCommand.setNameValues(new NameValueList(new NameValue("displayName", modifiedName)));
        modifyCitizenCommand.setRequester(Requester.sample());
        ModifyCitizenCommand response = citizenFlow.modifyCitizen(modifyCitizenCommand);

        assertNotNull(response.getResponse());
        assertNull(response.getFailureMessage());

        CitizenIdentity citizenIdentity = citizenService.retrieveCitizenIdentityById(initialCitizenId);
        assertEquals(modifiedName, citizenIdentity.getDisplayName());
    }


    @Test
    public void activateCitizenTest() {
        //
        ActivateCitizenCommand activateCitizenCommand = new ActivateCitizenCommand(initialCitizenId);
        activateCitizenCommand.setRequester(Requester.sample());

        ActivateCitizenCommand response = citizenFlow.activateCitizen(activateCitizenCommand);
        assertNotNull(response.getResponse());
        assertNull(response.getFailureMessage());

        CitizenIdentity citizenIdentity = citizenService.retrieveCitizenIdentityById(initialCitizenId);
        assertEquals(CitizenState.Active, citizenIdentity.getState());
    }

    @Test
    public void deactivateCitizenTest() {
        //
        DeactivateCitizenCommand deactivateCitizenCommand = new DeactivateCitizenCommand(initialCitizenId);
        deactivateCitizenCommand.setRequester(Requester.sample());

        DeactivateCitizenCommand response = citizenFlow.deactivateCitizen(deactivateCitizenCommand);
        assertNotNull(response.getResponse());
        assertNull(response.getFailureMessage());

        CitizenIdentity citizenIdentity = citizenService.retrieveCitizenIdentityById(initialCitizenId);
        assertEquals(CitizenState.Dormant, citizenIdentity.getState());
    }

    @Test
    public void removeCitizen() {
        //
        RemoveCitizenCommand removeCitizenCommand = new RemoveCitizenCommand(initialCitizenId);
        removeCitizenCommand.setRequester(Requester.sample());

        RemoveCitizenCommand response = citizenFlow.removeCitizen(removeCitizenCommand);
        assertNotNull(response.getResponse());
        assertNull(response.getFailureMessage());

        CitizenIdentity citizenIdentity = citizenService.retrieveCitizenIdentityById(initialCitizenId);
        assertEquals(CitizenState.Removed, citizenIdentity.getState());
    }


    private void registerCitizenTest() {
        //
        RegisterCitizenCommand command = RegisterCitizenCommand.sample();
        command.setRequester(Requester.sample());
        RegisterCitizenCommand response = citizenFlow.registerCitizen(command);

        log.debug("\n## CommandStringResponse in registerActiveCitizenTest() {}", response.toPrettyJson());
        assertNotNull(response.getResponse());
        assertNull(response.getFailureMessage());
        initialCitizenId = response.getResponse().getEntityIds().get(0);
    }


}
