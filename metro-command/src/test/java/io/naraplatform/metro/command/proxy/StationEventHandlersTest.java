//package io.naraplatform.metro.command.proxy;
//
//import io.naradrama.prologue.domain.patron.AudienceKey;
//import io.naradrama.prologue.domain.workspace.Workspace;
//import io.naraplatform.envoy.context.DramaContext;
//import io.naraplatform.envoy.context.DramaRequest;
//import io.naraplatform.metro.command.MetroClientTestApplication;
//import io.naraplatform.metro.command.service.CastellanService;
//import io.naraplatform.metro.command.service.NaraService;
//import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
//import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
//import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
//import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
//import io.naraplatform.metro.store.domain.aggregate.castellan.CastellanStore;
//import io.naraplatform.metro.store.domain.aggregate.metro.MetroStore;
//import io.naradrama.prologue.domain.lang.LangString;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//@Slf4j
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = MetroClientTestApplication.class)
//public class StationEventHandlersTest {
//    //
//    @Autowired
//    private StationEventHandler stationEventHandler;
//    @Autowired
//    private NaraService naraService;
//    @Autowired
//    private CastellanService castellanService;
//    @Autowired
//    private MetroStore metroStore;
//    @Autowired
//    private CastellanStore castellanStore;
//
//    private static boolean initialized;
//
//    private void setCurrentDramaRequest() {
//        //
//        List<Workspace> workspaces = new ArrayList<>();
//        workspaces.add(new Workspace("ne1-m1-c2", "Station Manager Team", "1@ne1-m1-c2"));
//
//        AudienceKey audienceKey = AudienceKey.sample();
//        DramaContext.setCurrentRequest(new DramaRequest(
//                audienceKey.getKeyString(),
//                audienceKey.genCineroomKeyString(),
//                Arrays.asList("Admin,User"),
//                workspaces,
//                "manager@nextree.io",
//                "manager"
//        ));
//    }
//
//    @Before
//    public void before() {
//        //
//        log.debug("\n## initialized {}", initialized);
//        if (initialized) {
//            return;
//        }
//        setCurrentDramaRequest();
//
//        initialized = true;
//        log.debug("\n## initialized {}", initialized);
//    }
//
////    todo fixme
////    @Test
//    public void preliminaryPavilionActivatedEventHandleTest() {
//        //
////        PreliminaryPavilionActivatedEvent event = new PreliminaryPavilionActivatedEvent();
////        RegisterMetroCommand commmand = RegisterMetroCommand.sample();
////        String pavilionId = "ne1-m5";
////        event.setPavilionId(pavilionId);
////        event.setPavilionName(commmand.getName());
////        event.setManagerId(commmand.getOfficialId());
////        log.debug("\n## preliminaryPavilionActivatedEventHandleTest() {}", event.toPrettyJson());
////
////        stationEventHandler.handle(event);
////        Metro metro = metroStore.retrieveByMetroId(pavilionId);
////        log.debug("\n## preliminaryPavilionActivatedEventHandleTest() : Metro {}", metro.toPrettyJson());
////        Assert.assertEquals(pavilionId, metro.getId());
//    }
//
////    todo fixme
////    @Test
//    public void activePavilionRegisteredEventTest() {
//        //
////        ActivePavilionRegisteredEvent event = new ActivePavilionRegisteredEvent();
////
////        RegisterMetroCommand command = RegisterMetroCommand.sample();
////        String pavilionId = "ne1-m6";
////        event.setId(pavilionId);
////        event.setName(command.getName());
////
////        stationEventHandler.handle(event);
////        Metro metro = metroStore.retrieveByMetroId(pavilionId);
////
////        log.debug("\n## activePavilionRegisteredEventTest() {}", metro.toPrettyJson());
////        Assert.assertEquals(pavilionId, metro.getId());
//    }
//
//    @Test
//    public void squareManagerModifiedEventHandleTest() {
//        //
////        RegisterNaraCommand registerNaraCommand = RegisterNaraCommand.sample();
////        Nara nara = naraService.registerNara(registerNaraCommand.getName(), registerNaraCommand.getType(),
////                registerNaraCommand.getZoneId(),
////                registerNaraCommand.getBaseLanguage(), registerNaraCommand.getSupportLanguages(),
////                registerNaraCommand.getPublicServantName(), registerNaraCommand.getPublicServantEmail(),
////                registerNaraCommand.genCommandIdentity());
////        String naraId = nara.getId();
////        log.debug("RegisterNara() : NaraId {}", naraId);
////
////        RegisterManagerCastellanCommand command = RegisterManagerCastellanCommand.sample();
////        command.setManagerId("r2p8-r@ne1-m5-c5");
////        command.setEmail(registerNaraCommand.getPublicServantEmail());
////        command.setName(LangString.sample());
////        command.setEncryptedPassword("$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei");
////        Castellan castellanResponse = castellanService.registerManagerCastellan(command.getManagerId(),
////                command.getEmail(),
////                command.getName(), command.getEncryptedPassword(),
////                command.isStationManager(), command.genCommandIdentity());
////        String castellanId = castellanResponse.getId();
//
////        todo fixme
////        SquareManagerModifiedEvent event = new SquareManagerModifiedEvent();
////        event.setCastellanId(castellanId);
////        event.setSquareId(naraId);
////        event.setNameValues(ModifySquareManagerCastellanCommand.sample().getNameValues());
////
////        log.debug("\n## squareManagerModifiedEventHandleTest() {}", event.toPrettyJson());
////        stationEventHandler.handle(event);
////
////        Castellan castellan = castellanStore.retrieve(castellanId);
////        log.debug("\n## squareManagerModifiedEventHandleTest() : Castellan {}", castellan.toPrettyJson());
////        Assert.assertEquals(castellanId, castellan.getId());
//    }
//}
