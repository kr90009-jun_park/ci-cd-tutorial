package io.naraplatform.metro.command.aggregate;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.patron.AudienceKey;
import io.naradrama.prologue.domain.workspace.Workspace;
import io.naraplatform.envoy.context.DramaContext;
import io.naraplatform.envoy.context.DramaRequest;
import io.naraplatform.metro.command.MetroClientTestApplication;
import io.naraplatform.metro.command.service.CastellanService;
import io.naraplatform.metro.domain.aggregate.castellan.command.*;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetroClientTestApplication.class)
public class CastellanServiceTest {
    //
    @Autowired
    private CastellanService castellanService;
    private static String testCitizenId = "z1@ne1-m1";

    private static RegisterManagerCastellanCommand registerManagerCastellanCommand;
    private static boolean initialized;
    private static String managerCastellanId;

    private void setCurrentDramaRequest() {
        //

        List<Workspace> workspaces = new ArrayList<>();
        workspaces.add(new Workspace("ne1-m1-c2", "Station Manager Team", "1@ne1-m1-c2"));

        AudienceKey audienceKey = AudienceKey.sample();
        DramaContext.setCurrentRequest(new DramaRequest(
                audienceKey.getKeyString(),
                audienceKey.genCineroomKeyString(),
                Arrays.asList("Admin,User"),
                workspaces,
                "manager@nextree.io",
                "manager"
        ));

    }

    @Before
    public void before() {
        //
        log.debug("\n## initialized {}", initialized);
        if (initialized) {
            return;
        }
        setCurrentDramaRequest();

        registerManagerCastellanCommand = RegisterManagerCastellanCommand.sample();
        Castellan castellan = castellanService.registerManagerCastellan(
                registerManagerCastellanCommand.getManagerId(),
                registerManagerCastellanCommand.getEmail(),
                registerManagerCastellanCommand.getName(),
                registerManagerCastellanCommand.getEncryptedPassword(),
                registerManagerCastellanCommand.isStationManager(),
                registerManagerCastellanCommand.genCommandIdentity());
        managerCastellanId = castellan.getId();

        log.debug("\n## in before() register manager castellan {}", castellan.toPrettyJson());
        initialized = true;

        log.debug("\n## initialized {}", initialized);
    }

    @Test
    public void registerCitizenCastellanTest() {
        //
        RegisterCitizenCastellanCommand command = RegisterCitizenCastellanCommand.sample();
        Castellan castellan = castellanService.registerCastellan(command.getEmail(), command.getName(), command.getEncryptedPassword(), command.genCommandIdentity());
        assertNotNull(castellan);
    }

    @Test
    public void registerMidtownMemberCastellanTest() {
        //
        RegisterMidtownMemberCastellanCommand command = RegisterMidtownMemberCastellanCommand.sample();
        command.setRequester(Requester.sample());
        Castellan castellan = castellanService.registerMidtownMemberCastellan(command.getMember(), "z1@ne1-m1",
                command.genCommandIdentity());
        assertNotNull(castellan);
        log.debug("\n## registerMidtownMemberCastellanTest() {}", castellan);

    }

    @Test
    public void modifyMidtownMemberCastellanTest() {
        //
        ModifyMidtownMemberCastellanCommand command = ModifyMidtownMemberCastellanCommand.sample();
        command.setCastellanId(managerCastellanId);
        command.setRequester(Requester.sample());
        Castellan castellan = castellanService.modifyMidtownMemberCastellan(command.getCastellanId(),
                command.getMember(), command.getPreviousEmail(),
                command.genCommandIdentity());
        assertNotNull(castellan);
    }

    @Test
    public void modifySquareManagerCastellanTest() {
        //
        ModifySquareManagerCastellanCommand command = ModifySquareManagerCastellanCommand.sample();
        command.setCastellanId(managerCastellanId);
        command.setRequester(Requester.sample());
        castellanService.modifySquareManagerCastellan(command.getCastellanId(),
                command.getSquareId(), command.getNameValues(),
                command.genCommandIdentity());
    }

    @Test
    public void removeMidtownMemberCastellanTest() {
        //
        RegisterMidtownMemberCastellanCommand registerCommand = RegisterMidtownMemberCastellanCommand.sample();
        registerCommand.setRequester(Requester.sample());
        Member member = Member.sample();
        member.setEmail("remover@nextree.io");
        registerCommand.setMember(member);
        Castellan castellan = castellanService.registerMidtownMemberCastellan(registerCommand.getMember(),testCitizenId,
                registerCommand.genCommandIdentity());
        log.debug("\n## removeMidtownMemberCastellanTest() {}", castellan);

        RemoveMidtownMemberCastellanCommand command = RemoveMidtownMemberCastellanCommand.sample();
        castellanService.removeMidtownMemberCastellan(castellan.getId(),
                command.getMember(), testCitizenId,
                command.genCommandIdentity());
    }

    @Test
    public void findCastellanTest() {
        //
        Castellan castellan = castellanService.findCastellan(managerCastellanId);
        log.debug("\n## findCastellanTest() {}", castellan.toPrettyJson());

        assertEquals(castellan.getId(), managerCastellanId);
    }

    @Test
    public void existsCastellanEmailTest() {
        //
        boolean exists
                = castellanService.existsCastellanEmail(registerManagerCastellanCommand.getEmail());
        log.debug("\n## existsCastellanEmailTest() {}", exists);
        assertTrue(exists);
        exists = castellanService.existsCastellanEmail("tester@exists.com");
        log.debug("\n## existsCastellanEmailTest() {}", exists);
        assertFalse(exists);
    }
}
