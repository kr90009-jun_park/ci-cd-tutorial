package io.naraplatform.metro.command;

import io.naraplatform.daysman.client.config.EnableDaysman;
import io.naraplatform.envoy.config.NaraDramaApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@NaraDramaApplication
@SpringBootApplication(scanBasePackages = {
        "io.naraplatform.metro",
        "io.naraplatform.checkpoint.client.local",
        "io.naradrama.pigeon.client.local"
})
@EnableDaysman
@EnableMongoRepositories(basePackages = "io.naraplatform.metro.store.mongo")
public class MetroClientTestApplication {
    //
    public static void main(String[] args) {
        //
        SpringApplication.run(MetroClientTestApplication.class, args);
    }
}
