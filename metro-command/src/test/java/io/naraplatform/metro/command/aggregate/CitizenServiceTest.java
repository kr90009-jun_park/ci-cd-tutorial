package io.naraplatform.metro.command.aggregate;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.patron.AudienceKey;
import io.naradrama.prologue.domain.workspace.Workspace;
import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.envoy.context.DramaContext;
import io.naraplatform.envoy.context.DramaRequest;
import io.naraplatform.metro.command.MetroClientTestApplication;
import io.naraplatform.metro.command.flow.CitizenFlow;
import io.naraplatform.metro.command.flow.MidtownMemberCastellanFlow;
import io.naraplatform.metro.command.service.CitizenService;
import io.naraplatform.metro.domain.aggregate.castellan.command.ModifyMidtownMemberCastellanCommand;
import io.naraplatform.metro.domain.aggregate.citizen.command.*;
import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetroClientTestApplication.class)
public class CitizenServiceTest {
    //
    @Autowired
    private CitizenService citizenService;

    @Autowired
    private CitizenFlow citizenFlow;

    @Autowired
    private MidtownMemberCastellanFlow midtownMemberCastellanFlow;

    private static RegisterPreliminaryCitizenCommand registerPreliminaryCitizenCommand;
    private static boolean initialized;
    private static String initialCitizenId;
    private static String initialCastellanId;

    private void setCurrentDramaRequest() {
        //
        List<Workspace> workspaces = new ArrayList<>();
        workspaces.add(new Workspace("ne1-m1-c2", "Station Manager Team", "1@ne1-m1-c2"));

        AudienceKey audienceKey = AudienceKey.sample();
        DramaContext.setCurrentRequest(new DramaRequest(
                audienceKey.getKeyString(),
                audienceKey.genCineroomKeyString(),
                Arrays.asList("Admin,User"),
                workspaces,
                "manager@nextree.io",
                "manager"
        ));
    }

    @Before
    public void before() {
        //
        log.debug("\n## initialized {}", initialized);
        if (initialized) {
            return;
        }
        setCurrentDramaRequest();

        registerPreliminaryCitizenCommand = RegisterPreliminaryCitizenCommand.sample();
        CitizenIdentity citizenIdentity = citizenService.registerPreliminaryCitizen(
                registerPreliminaryCitizenCommand.getMetroId(),
                registerPreliminaryCitizenCommand.getName(),
                registerPreliminaryCitizenCommand.getEmail(),
                registerPreliminaryCitizenCommand.getUsid(),
                UUID.randomUUID().toString(),
                newSecretCode(),
                registerPreliminaryCitizenCommand.genCommandIdentity());
        initialCastellanId = citizenIdentity.getCastellanId();
        initialCitizenId = citizenIdentity.getId();

        log.debug("\n## in before() register preliminary citizen id {} castellanId {}", initialCastellanId, initialCitizenId);
        initialized = true;

        log.debug("\n## initialized {}", initialized);
    }

    @Test
    public void registerCitizenTest() {
        //
        RegisterCitizenCommand command = new RegisterCitizenCommand();
        command.setMetroId("nea-m7");
        command.setName("Mr. Soo");
        command.setEmail("nara@nextree.io");
        command.setEncryptedPassword("$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei");

    }

    @Test
    public void registerActiveCitizenTest() {
        //
        RegisterActiveCitizenCommand command = RegisterActiveCitizenCommand.sample();
        command.setMetroId("nea-m7");
        command.setName("Mr Soo");
        command.setEmail("nara@nextree.io");
        command.setCastellanId(initialCastellanId);
        String citizenId = citizenService.registerActiveCitizen(command.getMetroId(),
                command.getName(),
                command.getEmail(),
                command.getCastellanId(),
                command.genCommandIdentity());
        log.debug("\n## CommandStringResponse in registerActiveCitizenTest() {}", citizenId);

        assertNotNull(citizenId);
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void registerActiveCitizenFailTest() {
        //
        exceptionRule.expect(NaraException.class);
        exceptionRule.expectMessage(MetroExceptionMessage.citizenAlreadyRegistered);

        RegisterActiveCitizenCommand command = RegisterActiveCitizenCommand.sample();
        command.setMetroId(registerPreliminaryCitizenCommand.getMetroId());
        command.setName(registerPreliminaryCitizenCommand.getName());
        command.setEmail(registerPreliminaryCitizenCommand.getEmail());
        command.setCastellanId(initialCastellanId);
        String citizenId = citizenService.registerActiveCitizen(command.getMetroId(),
                command.getName(),
                command.getEmail(),
                command.getCastellanId(),
                command.genCommandIdentity());
        log.debug("\n## CommandStringResponse in registerActiveCitizenFailTest() {}", citizenId);

        assertNotNull(citizenId);
    }

    @Test
    public void registerPreliminaryCitizenTest() {
        //
        CitizenIdentity citizenIdentity = citizenService.registerPreliminaryCitizen(
                "nea-m11",
                "Park Nara 11",
                "nara11@nextree.io",
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                newSecretCode(),
                CommandIdentity.sample());
        log.debug("\n## registerPreliminaryCitizenTest() {}", citizenIdentity);

        assertNotNull(citizenIdentity.getId());
        assertNotNull(citizenIdentity.getCastellanId());
    }

    @Test(expected = NaraException.class)
    public void registerPreliminaryCitizenFailTest() {
        //
        RegisterPreliminaryCitizenCommand command = RegisterPreliminaryCitizenCommand.sample();
        CitizenIdentity citizenIdentity = citizenService.registerPreliminaryCitizen(command.getMetroId(),
                command.getName(),
                command.getEmail(),
                command.getUsid(),
                UUID.randomUUID().toString(),
                newSecretCode(),
                command.genCommandIdentity());
    }

//    todo fixme
//    @Test
//    public void registerPavilionManagerCitizenTest() {
//        //
//        RegisterPavilionManagerCitizenCommand command = RegisterPavilionManagerCitizenCommand.sample();
//        command.setCastellanId(initialCastellanId);
//        String citizenId = citizenService.registerPavilionManagerCitizen(command.getMetroId(),
//                command.getName(),
//                command.getEmail(),
//                command.getEncryptedPassword(),
//                command.getPredefinedId(),
//                command.getCastellanId(),
//                command.getUsid(),
//                command.genCommandIdentity());
//        log.debug("\n## registerPavilionManagerCitizenTest() {}", citizenId);
//        assertNotNull(citizenId);
//    }

    @Test
    public void activatePreliminaryTest() {
        //
        Citizen citizen = citizenService.retrieveCitizenById(initialCitizenId);
        ActivatePreliminaryCitizenCommand command = new ActivatePreliminaryCitizenCommand(
                citizen.getId(), citizen.getEmailVerifedInfo().getSecretCode(), "1"
        );
        command.setRequester(Requester.sample());
        citizenService.verifySecretCode(command.getCitizenId(), command.getSecretCode());
        Citizen citizen1 = citizenService.activatePreliminary(command.getCitizenId(),
                command.getPassword(),
                command.genCommandIdentity());
        log.debug("\n## activatePreliminaryTest() {}", citizen1.toPrettyJson());
        assertNotNull(citizen1);
    }

    @Test
    public void activatePreliminaryFailTest() {
        //
        exceptionRule.expect(NaraException.class);
        exceptionRule.expectMessage(MetroExceptionMessage.secretCodeDoesNotMatch);

        Citizen citizen = citizenService.retrieveCitizenById(initialCitizenId);
        ActivatePreliminaryCitizenCommand command = new ActivatePreliminaryCitizenCommand(
                citizen.getId(), "", "1"
        );

        citizenService.verifySecretCode(command.getCitizenId(), command.getSecretCode());
        Citizen citizen1 = citizenService.activatePreliminary(command.getCitizenId(),
                command.getPassword(),
                command.genCommandIdentity());
        log.debug("\n## activatePreliminaryFailTest() {}", citizen1.toPrettyJson());

        assertNotNull(citizen1);
    }

    @Test
    public void modifyCitizenTest() {
        //
        ModifyCitizenCommand command = ModifyCitizenCommand.sample();
        command.setCitizenId(initialCitizenId);
        Citizen citizen1 = citizenService.modifyCitizen(command.getCitizenId(),
                command.getNameValues(),
                command.genCommandIdentity());
        log.debug("\n## modifyCitizenTest() {}", citizen1.toPrettyJson());

        Citizen citizen = citizenService.retrieveCitizenById(initialCitizenId);
        assertEquals(citizen.getIdentity().getDisplayName(),
                command.getNameValues().getNameValue("displayName").getValue());
    }

    @Test
    public void citizenStateTest() {
        //
        CitizenIdentity citizenIdentity = citizenService.registerPreliminaryCitizen(
                "nea-m17",
                "Park Nara 17",
                "nara17@nextree.io",
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                newSecretCode(),
                CommandIdentity.sample());
        String citizenId = citizenIdentity.getId();

        Citizen citizen = citizenService.retrieveCitizenById(citizenIdentity.getId());
        ActivatePreliminaryCitizenCommand command = new ActivatePreliminaryCitizenCommand(
                citizenId, citizen.getEmailVerifedInfo().getSecretCode(), "1"
        );
        command.setRequester(Requester.sample());


        citizenService.verifySecretCode(command.getCitizenId(), command.getSecretCode());
        Citizen citizen1 = citizenService.activatePreliminary(command.getCitizenId(),
                command.getPassword(),
                command.genCommandIdentity());
        log.debug("\n## citizenStateTest() activatePreliminary {}", citizen1.toPrettyJson());
        assertNotNull(citizen1);

        Citizen citizen2 = citizenService.deactivateCitizen(citizenId, CommandIdentity.sample());
        log.debug("\n## citizenStateTest() deactivateCitizen {}", citizen2.toPrettyJson());
        assertNotNull(citizen2);

        Citizen citizen3 = citizenService.activateCitizen(citizenId, CommandIdentity.sample());
        log.debug("\n## citizenStateTest() activateCitizen {}", citizen3.toPrettyJson());
        assertNotNull(citizen3);

        Citizen citizen4 = citizenService.deactivateCitizen(citizenId, CommandIdentity.sample());
        log.debug("\n## citizenStateTest() deactivateCitizen2 {}", citizen4.toPrettyJson());
        assertNotNull(citizen4);

//      todo jmpark RemoveCitizenCommand station에서부터 시작하도록 변경하는 것이 어떨지?
//        exceptionRule.expect(NaraException.class);
//        exceptionRule.expectMessage("cannot_delete_since_team_member");
//        RemoveCitizenCommand removeCitizenCommand = new RemoveCitizenCommand(citizenId);
//        CommandVoidResponse removedResponse
//                = citizenCommandLogic.removeCitizen(removeCitizenCommand);
//        log.debug("\n## citizenStateTest() removeCitizen {}", removedResponse.toPrettyJson());
    }

    @Test
    public void sendMailForInvitationTest() {
        //
        ResendMailForCitizenInvitationCommand command = new ResendMailForCitizenInvitationCommand(initialCitizenId);
        command.setRequester(Requester.sample());
        command = citizenFlow.sendMailForInvitation(command);
        log.debug("\n## sendMailForInvitationTest() {}", command);
        assertNotNull(command.getResponse());
    }

    @Test
    public void registerMidtownMemberCitizenTest() {
        //
        RegisterMidtownMemberCitizenCommand command = RegisterMidtownMemberCitizenCommand.sample();
        command = midtownMemberCastellanFlow.registerMidtownMemberCitizen(
                command);
        log.debug("\n## registerMidtownMemberCitizenTest() {}", command.toPrettyJson());
        assertNotNull(command.getResponse());
    }

//    todo fixme
//    @Test
//    public void modifyMidtownMemberCastellanTest() {
//        //
//        ModifyMidtownMemberCastellanCommand command = ModifyMidtownMemberCastellanCommand.sample();
//        command.setCastellanId(initialCastellanId);
//
//        command.setPreviousEmail(registerPreliminaryCitizenCommand.getEmail());
//        command = midtownMemberCastellanFlow.modifyMidtownMemberCastellan(command);
//        log.debug("\n## modifyMidtownMemberCitizenTest() {}", command);
//        assertNotNull(command.getResponse());
//    }

    @Test
    public void removeMidtownMemberCitizenTest() {
        //
        CitizenIdentity citizenIdentity = citizenService.registerPreliminaryCitizen(
                "nea-m18",
                "Park Nara 18",
                "nara18@nextree.io",
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                newSecretCode(),
                CommandIdentity.sample());
        String castellanId = citizenIdentity.getCastellanId();
        String citizenId = citizenIdentity.getId();

        RemoveMidtownMemberCitizenCommand command = RemoveMidtownMemberCitizenCommand.sample();
        command.setCastellanId(castellanId);
        command.setCitizenId(citizenId);
        Citizen citizen = citizenService.removeMidtownMemberCitizen(citizenId,
                command.getCastellanId(),
                command.getMember(),
                command.genCommandIdentity());
        log.debug("\n## removeMidtownMemberCitizenTest() {}", citizen.toPrettyJson());
        assertNotNull(citizen);
    }

    private String newSecretCode() {
        //
        return String.format("%05d", RandomUtils.nextInt(1, 99999));
    }
}
