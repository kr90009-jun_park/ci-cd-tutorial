//package io.naraplatform.metro.command.proxy;
//
//import io.naraplatform.metro.command.MetroClientTestApplication;
//import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
//import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
//import io.naraplatform.metro.store.domain.aggregate.citizen.CitizenIdentityStore;
//import io.naraplatform.metro.store.query.aggregate.castellan.CastellanEmailStore;
//import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
//import io.naraplatform.midtown.domain.aggregate.member.event.MemberModifiedEvent;
//import io.naraplatform.midtown.domain.aggregate.member.event.MemberRegisteredEvent;
//import io.naraplatform.midtown.domain.aggregate.member.event.MemberRemovedEvent;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@Slf4j
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = MetroClientTestApplication.class)
//public class MidtownEventHandlersTest {
//    //
//    @Autowired
//    private MidtownEventHandler midtownEventHandler;
//    @Autowired
//    private CastellanEmailStore castellanEmailStore;
//    @Autowired
//    private CitizenIdentityStore citizenIdentityStore;
//
//    private static boolean initialized;
//
//    private void setCurrentDramaRequest() {
//        //
////        AudienceKey audienceKey = AudienceKey.sample();
////        DramaContext.setCurrentRequest(new DramaRequest(
////                audienceKey.getKeyString(),
////                audienceKey.genCineroomKeyString(),
////                Arrays.asList("User")
////        ));
//    }
//
//    @Before
//    public void before() {
//        //
//        log.debug("\n## initialized {}", initialized);
//        if (initialized) {
//            return;
//        }
//        setCurrentDramaRequest();
//
//        initialized = true;
//        log.debug("\n## initialized {}", initialized);
//    }
//
//    @Test
//    public void midtownMemberRegisteredEventHandleTest() {
//        //
//        MemberRegisteredEvent event = new MemberRegisteredEvent();
//        Member member = Member.sample();
//        member.setEmail("handlers@nextree.io");
//        event.setMember(member);
//
//        log.debug("\n## midtownMemberRegisteredEventHandleTest() 1 {}", event.toPrettyJson());
//        midtownEventHandler.handle(event);
//        CastellanEmail castellanEmail = castellanEmailStore.retrieveByEmail(member.getEmail());
//        log.debug("\n## midtownMemberRegisteredEventHandleTest() 1 : CastellanEmail {}", castellanEmail.toPrettyJson());
//        Assert.assertEquals(castellanEmail.getEmail(), member.getEmail());
//
//        member.setPavilionId("nea-m7");
//        log.debug("\n## midtownMemberRegisteredEventHandleTest() 2 {}", event.toPrettyJson());
//        midtownEventHandler.handle(event);
//        CitizenIdentity identity
//                = citizenIdentityStore.retrieveByMetroIdAndEmail(member.getPavilionId(), member.getEmail());
//        log.debug("\n## midtownMemberRegisteredEventHandleTest() 2 : CitizenIdentity {}", identity.toPrettyJson());
//        Assert.assertEquals(identity.getEmail(), member.getEmail());
//
////        Name name = Name.newFamilyFirst("Park", "Nara");
////        member.setName("ko", name);
//        log.debug("\n## midtownMemberRegisteredEventHandleTest() 3 {}", event.toPrettyJson());
//        midtownEventHandler.handle(event);
//        identity = citizenIdentityStore.retrieveByMetroIdAndEmail(member.getPavilionId(), member.getEmail());
//        log.debug("\n## midtownMemberRegisteredEventHandleTest() 3 : CitizenIdentity {}", identity.toPrettyJson());
////        Assert.assertEquals(identity.getDisplayName(), name.showFamilyLast());
//        Assert.assertEquals(identity.getDisplayName(), member.getNames().genString());
//    }
//
//    @Test
//    public void midtownMemberModifiedEventHandleTest() {
//        //
//        MemberModifiedEvent event = new MemberModifiedEvent();
//        Member member = Member.sample();
//        member.setEmail("handlers@nextree.io");
//        event.setMember(member);
//
//        log.debug("\n## midtownMemberModifiedEventHandleTest() 1 {}", event.toPrettyJson());
//        midtownEventHandler.handle(event);
//        CastellanEmail castellanEmail = castellanEmailStore.retrieveByEmail(member.getEmail());
//        log.debug("\n## midtownMemberModifiedEventHandleTest() 1 : CastellanEmail {}", castellanEmail.toPrettyJson());
//        Assert.assertEquals(castellanEmail.getEmail(), member.getEmail());
//
//        log.debug("\n## midtownMemberModifiedEventHandleTest() 2 {}", event.toPrettyJson());
//        midtownEventHandler.handle(event);
//        CitizenIdentity identity
//                = citizenIdentityStore.retrieveByMetroIdAndEmail(member.getPavilionId(), member.getEmail());
//        log.debug("\n## midtownMemberModifiedEventHandleTest() 2 : CitizenIdentity {}", identity.toPrettyJson());
//        Assert.assertEquals(identity.getDisplayName(), member.getNames().genString());
//    }
//
//    @Test
//    public void midtownMemberRemovedEventHandleTest() {
//        //
//        MemberRegisteredEvent registeredEvent = new MemberRegisteredEvent();
//        Member member = Member.sample();
//        member.setEmail("handlers@nextree.io");
//        registeredEvent.setMember(member);
//
//        log.debug("\n## midtownMemberRemovedEventHandleTest() 1 {}", registeredEvent.toPrettyJson());
//        midtownEventHandler.handle(registeredEvent);
//        CastellanEmail castellanEmail = castellanEmailStore.retrieveByEmail(member.getEmail());
//        log.debug("\n## midtownMemberRemovedEventHandleTest() 1 : CastellanEmail {}", castellanEmail.toPrettyJson());
//        Assert.assertEquals(castellanEmail.getEmail(), member.getEmail());
//
//
//        MemberRemovedEvent event = new MemberRemovedEvent();
//        event.setMember(member);
//        log.debug("\n## midtownMemberRemovedEventHandleTest() 2 {}", event.toPrettyJson());
//        midtownEventHandler.handle(event);
//        castellanEmail = castellanEmailStore.retrieveByEmail(member.getEmail());
//        log.debug("\n## midtownMemberRemovedEventHandleTest() 2 : CastellanEmail {}", castellanEmail);
//        Assert.assertNull(castellanEmail);
//    }
//}
