package io.naraplatform.metro.command.flow;

import io.naradrama.prologue.domain.drama.NaraDramaBasicRoles;
import io.naradrama.prologue.domain.patron.AudienceKey;
import io.naradrama.prologue.domain.workspace.Workspace;
import io.naraplatform.envoy.context.DramaContext;
import io.naraplatform.envoy.context.DramaRequest;
import io.naraplatform.metro.command.MetroClientTestApplication;
import io.naraplatform.metro.command.service.CastellanService;
import io.naraplatform.metro.domain.aggregate.castellan.command.ModifySquareManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetroClientTestApplication.class)
public class CastellanFlowTest {
    //
    @Autowired
    private CastellanFlow castellanFlow;
    @Autowired
    private CastellanService castellanService;

    private static RegisterManagerCastellanCommand registerManagerCastellanCommand;
    private static boolean initialized;
    private static String managerCastellanId;

    private void setCurrentDramaRequest() {
        //
        List<Workspace> workspaces = new ArrayList<>();
        workspaces.add(new Workspace("ne1-m1-c2", "Station Manager Team", "1@ne1-m1-c2"));

        AudienceKey audienceKey = AudienceKey.sample();
        DramaContext.setCurrentRequest(new DramaRequest(
                audienceKey.getKeyString(),
                audienceKey.genCineroomKeyString(),
                Arrays.asList(NaraDramaBasicRoles.Admin, NaraDramaBasicRoles.Member),
                workspaces,
                "manager@nextree.io",
                "manager"
        ));
    }

    @Before
    public void before() {
        //
        log.debug("\n## initialized {}", initialized);
        if (initialized) {
            return;
        }
        setCurrentDramaRequest();

        registerManagerCastellanCommand = RegisterManagerCastellanCommand.sample(12);
        RegisterManagerCastellanCommand command = registerManagerCastellanCommand;
        Castellan castellan = castellanService.registerCastellan(
                command.getEmail(),
                command.getName().getString(), command.getEncryptedPassword(),
                command.genCommandIdentity());
        managerCastellanId = castellan.getId();

        log.debug("\n## in before() register manager castellan {}", castellan.toPrettyJson());
        initialized = true;

        log.debug("\n## initialized {}", initialized);
    }

//    todo fixme
//    @Test
//    public void registerPavilionManagerCastellanTest() {
//        //
//        RegisterPavilionManagerCastellanCommand command = RegisterPavilionManagerCastellanCommand.sample();
//
//        RegisterPavilionManagerCastellanCommand response = castellanFlow.registerPavilionManagerCastellan(command);
//        log.debug("\n## response in registerPavilionManagerCastellan() {}", response.toPrettyJson());
//
//        assertNotNull(response.getResponse());
//    }

    @Test
    public void modifySquareManagerCastellanTest() {
        //
        ModifySquareManagerCastellanCommand command = ModifySquareManagerCastellanCommand.sample();
        command.setCastellanId(managerCastellanId);
        ModifySquareManagerCastellanCommand response = castellanFlow.modifySquareManagerCastellan(command);
        log.debug("\n## response in addJoinedMetroForPavilionManager() {}", response.toPrettyJson());

        assertNotNull(response.getResponse());
    }
}
