//package io.naraplatform.metro.command.grpc;
//
//import io.grpc.stub.StreamObserver;
//import io.naraplatform.metro.command.flow.CastellanFlow;
//import io.naraplatform.metro.domain.aggregate.castellan.command.AddJoinedMetroForPavilionManagerCommand;
//import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
//import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
//import io.naraplatform.metro.proto.command.AddJoinedMetroForPavilionManagerCommandMsg;
//import io.naraplatform.metro.proto.command.ManagerCastellanCommandProtoGrpc;
//import io.naraplatform.metro.proto.command.RegisterPavilionManagerCastellanCommandMsg;
//import io.naradrama.prologue.domain.cqrs.FailureMessage;
//import lombok.extern.slf4j.Slf4j;
//import org.lognet.springboot.grpc.GRpcService;
//
//@Slf4j
//@GRpcService
//public class ManagerCastellanFlowGrpc extends ManagerCastellanCommandProtoGrpc.ManagerCastellanCommandProtoImplBase {
//    //
//    private final CastellanFlow castellanFlow;
//
//    public ManagerCastellanFlowGrpc(CastellanFlow castellanFlow) {
//        //
//        this.castellanFlow = castellanFlow;
//    }
//
//    @Override
//    public void registerPavilionManagerCastellan(RegisterPavilionManagerCastellanCommandMsg request, StreamObserver<RegisterPavilionManagerCastellanCommandMsg> responseObserver) {
//        //
//        RegisterPavilionManagerCastellanCommand command = MetroGrpcMessage.toCommand(request);
//        try {
//            command = castellanFlow.registerPavilionManagerCastellan(command);
//        } catch (Throwable e) {
//            log.error("failed_to_register_nara : \n{}", command, e);
//            command.setFailureMessage(new FailureMessage(e.getClass().getSimpleName(), e.getMessage()));
//        }
//
//        responseObserver.onNext(MetroGrpcMessage.toMessage(command));
//        responseObserver.onCompleted();
//    }
//
//    @Override
//    public void addJoinedMetroForPavilionManager(AddJoinedMetroForPavilionManagerCommandMsg request, StreamObserver<AddJoinedMetroForPavilionManagerCommandMsg> responseObserver) {
//        //
//        AddJoinedMetroForPavilionManagerCommand command = MetroGrpcMessage.toCommand(request);
//        try {
//            command = castellanFlow.addJoinedMetroForPavilionManager(command);
//        } catch (Throwable e) {
//            log.error("failed_to_register_nara : \n{}", command, e);
//            command.setFailureMessage(new FailureMessage(e.getClass().getSimpleName(), e.getMessage()));
//        }
//
//        responseObserver.onNext(MetroGrpcMessage.toMessage(command));
//        responseObserver.onCompleted();
//
//    }
//
//    //    @Override
////    public void registerPavilionManagerCastellan(RegisterPavilionManagerCastellanCommandMsg request,
////                                                 StreamObserver<CommandStringResponseMsg> responseObserver) {
////        //
////        CommandStringResponse response = castellanFlow.registerPavilionManagerCastellan(toCommand(request));
////
////        responseObserver.onNext(toMessage(response));
////        responseObserver.onCompleted();
////    }
////
////    @Override
////    public void addJoinedMetroForPavilionManager(AddJoinedMetroForPavilionManagerCommandMsg request,
////                                                 StreamObserver<CommandStringResponseMsg> responseObserver) {
////        //
////        CommandStringResponse response = castellanFlow.addJoinedMetroForPavilionManager(toCommand(request));
////
////        responseObserver.onNext(toMessage(response));
////        responseObserver.onCompleted();
////    }
//}
