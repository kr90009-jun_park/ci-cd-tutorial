package io.naraplatform.metro.command.grpc;

import io.grpc.stub.StreamObserver;
import io.naraplatform.metro.command.flow.NaraFlow;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
import io.naraplatform.metro.proto.command.NaraCommandProtoGrpc;
import io.naraplatform.metro.proto.command.RegisterNaraCommandMsg;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
@GRpcService
public class NaraCommandGrpc extends NaraCommandProtoGrpc.NaraCommandProtoImplBase {
    //
    private final NaraFlow naraFlow;

    public NaraCommandGrpc(NaraFlow naraFlow) {
        //
        this.naraFlow = naraFlow;
    }

    @Override
    public void registerNara(RegisterNaraCommandMsg request, StreamObserver<RegisterNaraCommandMsg> responseObserver) {
        //
        RegisterNaraCommand command = MetroGrpcMessage.toCommand(request);
        try {
            command = naraFlow.registerNara(command);
        } catch (Throwable e) {
            log.error("failed_to_register_nara : \n{}", command, e);
            command.setFailureMessage(new FailureMessage(e.getClass().getSimpleName(), e.getMessage()));
        }

        responseObserver.onNext(MetroGrpcMessage.toMessage(command));
        responseObserver.onCompleted();
    }
}
