package io.naraplatform.metro.command.service;


import io.naraplatform.daysman.spec.EventStreamService;
import io.naraplatform.metro.domain.aggregate.metro.command.ActivateMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.DeactivateMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.RegisterMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.RemoveMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naraplatform.metro.domain.aggregate.metro.entity.MetroState;
import io.naraplatform.metro.domain.aggregate.metro.event.MetroActivatedEvent;
import io.naraplatform.metro.domain.aggregate.metro.event.MetroDeactivatedEvent;
import io.naraplatform.metro.domain.aggregate.metro.event.MetroRegisteredEvent;
import io.naraplatform.metro.domain.aggregate.metro.event.MetroRemovedEvent;
import io.naraplatform.metro.store.domain.aggregate.metro.MetroStore;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static io.naraplatform.metro.command.ServiceEventBuilder.newEvent;

@Slf4j
@Service
@Transactional
public class MetroService {
    //
    private final EventStreamService eventStreamService;
    private final MetroStore metroStore;

    public MetroService(EventStreamService eventStreamService,
                             MetroStore metroStore) {
        this.eventStreamService = eventStreamService;
        this.metroStore = metroStore;
    }

    public Metro findById(String id){
        //
        return metroStore.retrieveByMetroId(id);
    }

    public Metro registerMetro(String metroId, String name, String officialId, CommandIdentity commandIdentity) {
        //
        Metro metro = new Metro(metroId, MetroState.Active);

        metroStore.create(metro);

        MetroRegisteredEvent metroRegisteredEvent
                = new MetroRegisteredEvent(metro.genEntityIdName(),
                commandIdentity, metroId,
                name,
                officialId);
        eventStreamService.produce(newEvent(metroRegisteredEvent));

        return metro;
    }


    public Metro activateMetro(String metroId, CommandIdentity commandIdentity) {
        //
        Metro metro = metroStore.retrieveByMetroId(metroId);
        metro.setState(MetroState.Active);
        metroStore.update(metro);

        MetroActivatedEvent metroActivatedEvent = new MetroActivatedEvent(metro.genEntityIdName(),
                commandIdentity, metroId);
        eventStreamService.produce(newEvent(metroActivatedEvent));

        return metro;
    }


    public Metro deactivateMetro(String metroId, CommandIdentity commandIdentity) {
        //
        Metro metro = metroStore.retrieveByMetroId(metroId);
        metro.setState(MetroState.Dormant);
        metroStore.update(metro);

        MetroDeactivatedEvent metroDeactivatedEvent = new MetroDeactivatedEvent(metro.genEntityIdName(),
                commandIdentity,
                metroId);
        eventStreamService.produce(newEvent(metroDeactivatedEvent));

        return metro;
    }


    public Metro removeMetro(String metroId, CommandIdentity commandIdentity) {
        //
        Metro metro = metroStore.retrieveByMetroId(metroId);
        metro.setState(MetroState.Removed);
        metroStore.update(metro);

        MetroRemovedEvent metroRemovedEvent = new MetroRemovedEvent(
                metro.genEntityIdName(),
                commandIdentity,
                metroId);
        eventStreamService.produce(newEvent(metroRemovedEvent));
        return metro;
    }
}
