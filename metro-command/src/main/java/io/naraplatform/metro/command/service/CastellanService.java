package io.naraplatform.metro.command.service;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.Tier;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.granule.Email;
import io.naradrama.prologue.domain.granule.EmailList;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.domain.lang.LangStrings;
import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.daysman.spec.EventStreamService;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.metro.domain.aggregate.castellan.event.*;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanName;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import io.naraplatform.metro.store.domain.aggregate.castellan.CastellanStore;
import io.naraplatform.metro.store.query.aggregate.castellan.CastellanQueryStore;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static io.naraplatform.metro.command.ServiceEventBuilder.newEvent;

@Service
@Transactional
@Slf4j
public class CastellanService {
    //
    private final EventStreamService eventStreamService;
    private final CastellanStore castellanStore;
    private final CastellanQueryStore castellanQueryStore;

    public CastellanService(EventStreamService eventStreamService,
                            CastellanStore castellanStore,
                            CastellanQueryStore castellanQueryStore) {
        //
        this.eventStreamService = eventStreamService;
        this.castellanStore = castellanStore;
        this.castellanQueryStore = castellanQueryStore;
    }

    public Castellan registerCastellan(
            String email,
            String name,
            String encryptedPassword,
            CommandIdentity commandIdentity) {

        //
        if (castellanQueryStore.castellanEmailExistsByEmail(email)) {
            throw new NaraException(MetroExceptionMessage.emailIsAlreadyRegistered);
        }

        final Castellan castellan = new Castellan(newCastellanId(), Email.newPrimary(email), new LangString("ko", name));
        createCastellan(castellan);

        eventStreamService.produce(newEvent(new CastellanRegisteredEvent(
                castellan,
                commandIdentity,
                email,
                name,
                encryptedPassword)));

        return castellan;
    }

    @Deprecated
    public Castellan registerManagerCastellan(
            String managerId,
            String email,
            LangString name,
            String encryptedPassword,
            boolean stationManager,
            CommandIdentity commandIdentity) {
        //
        if (castellanQueryStore.castellanEmailExistsByEmail(email)) {
            throw new NaraException(MetroExceptionMessage.emailIsAlreadyRegistered);
        }

        Castellan castellan = new Castellan(
                newCastellanId(),
                Email.newPrimary(email),
                name);

        createCastellan(castellan);

        ManagerCastellanRegisteredEvent managerCastellanRegisteredEvent = new ManagerCastellanRegisteredEvent(
                castellan,
                commandIdentity,
                email,
                name,
                encryptedPassword,
                stationManager);
        eventStreamService.produce(newEvent(managerCastellanRegisteredEvent));

        return castellan;
    }

    @Deprecated
    public Castellan registerPavilionManagerCastellan(
            String email,
            LangString name,
            String joinedMetroId,
            String encryptedPassword,
            String citizenId,
            CommandIdentity commandIdentity) {
        //
        if (castellanQueryStore.castellanEmailExistsByEmail(email)) {
            throw new NaraException(MetroExceptionMessage.emailIsAlreadyRegistered);
        }

        String castellanId = newCastellanId();

        Castellan castellan = new Castellan(
                castellanId,
                Email.newPrimary(email),
                name);

        createCastellan(castellan);

        PavilionManagerCastellanRegisteredEvent castellanRegisteredEvent = new PavilionManagerCastellanRegisteredEvent(
                castellan,
                commandIdentity,
                email,
                name,
                joinedMetroId,
                encryptedPassword,
                citizenId);

        eventStreamService.produce(newEvent(castellanRegisteredEvent));

        return castellan;
    }

    @Deprecated
    public String registerCitizenCastellan(
            String castellanId,
            String email,
            String name,
            String joinedMetroId,
            String citizenId,
            String encryptedPassword,
            boolean activating,
            CommandIdentity commandIdentity) {
        //
        Castellan castellan = new Castellan(castellanId,
                Email.newPrimary(email),
                LangString.newString("ko", name));

        createCastellan(castellan);

        CitizenCastellanRegisteredEvent citizenCastellanRegisteredEvent =
                new CitizenCastellanRegisteredEvent(
                        castellan,
                        commandIdentity,
                        email,
                        name,
                        joinedMetroId,
                        citizenId,
                        encryptedPassword,
                        activating);

        eventStreamService.produce(newEvent(citizenCastellanRegisteredEvent));

        return castellanId;
    }

    public Castellan registerMidtownMemberCastellan(Member member, String citizenId,
                                                    CommandIdentity commandIdentity) {
        //
        LangStrings names = member.getNames();
        String castellanId = newCastellanId();

        Castellan castellan = new Castellan(castellanId, Email.newPrimary(member.getEmail()), names);
        castellan.setNames(member.getNames());

        createCastellan(castellan);

        MidtownMemberCastellanRegisteredEvent midtownMemberCastellanRegisteredEvent =
                new MidtownMemberCastellanRegisteredEvent(castellan, commandIdentity, citizenId, member);
        eventStreamService.produce(newEvent(midtownMemberCastellanRegisteredEvent));

        return castellan;
    }

    public Castellan modifyMidtownMemberCastellan(String castellanId,
                                                  Member member,
                                                  String previousEmail,
                                                  CommandIdentity commandIdentity) {
        //
        Castellan castellan = castellanStore.retrieve(castellanId);
        castellan.setEmails(new EmailList(Email.newPrimary(member.getEmail())));
        castellan.setNames(member.getNames());

        castellanQueryStore.deleteCastellanEmailByCastellanId(castellanId);
        castellanQueryStore.deleteCastellanNameByCastellanId(castellanId);

        createCastellan(castellan);

        MidtownMemberCastellanModfiedEvent midtownMemberCastellanModfiedEvent =
                new MidtownMemberCastellanModfiedEvent(
                        castellan, commandIdentity, member, previousEmail);

        eventStreamService.produce(newEvent(midtownMemberCastellanModfiedEvent));
        return castellan;
    }

    public boolean modifySquareManagerCastellan(String castellanId,
                                                String squareId,
                                                NameValueList nameValues,
                                                CommandIdentity commandIdentity) {
        //
        boolean isModified = false;
        Castellan castellan = castellanStore.retrieve(castellanId);
        if (castellan == null) {
            throw new NaraException(MetroExceptionMessage.cannotFindEntityById, Castellan.class.getSimpleName(), castellanId);
        }
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "name":
                    castellan.setNames(LangStrings.newString("ko", value));
                    break;
                case "email":
                    castellan.setEmails(new EmailList(Email.newPrimary(value)));
                    break;
            }
        }
        castellanStore.update(castellan);

        String displayName = castellan.getNames().genString();
        String email = castellan.getEmails().genPrimaryEmail();

        CastellanEmail castellanEmail =
                new CastellanEmail(email, displayName, castellanId);
        castellanQueryStore.deleteCastellanEmailByCastellanId(castellanId);
        castellanQueryStore.create(castellanEmail);

        CastellanName castellanName = new CastellanName(
                displayName,
                new EmailList(new Email(email, Tier.Primary, true)),
                castellanId);
        castellanQueryStore.deleteCastellanNameByCastellanId(castellanId);
        castellanQueryStore.create(castellanName);

        SquareManagerCastellanModifiedEvent squareManagerCastellanModifiedEvent =
                new SquareManagerCastellanModifiedEvent(
                        castellan, commandIdentity, squareId, nameValues);

        eventStreamService.produce(newEvent(squareManagerCastellanModifiedEvent));
        isModified = true;
        return isModified;
    }


    //TODO : Castellan을 삭제하는데 CitizenId가 왜 필요하지 ?
    public void removeMidtownMemberCastellan(String castellanId,
                                             Member member,
                                             String citizenId,
                                             CommandIdentity commandIdentity) {
        //
        castellanStore.delete(castellanId);
        castellanQueryStore.deleteCastellanEmailByCastellanId(castellanId);
        castellanQueryStore.deleteCastellanNameByCastellanId(castellanId);

        MidtownMemberCastellanRemovedEvent midtownMemberCastellanRemovedEvent = new MidtownMemberCastellanRemovedEvent(
                new IdName(castellanId, Castellan.class.getSimpleName()),
                commandIdentity,
                member,
                citizenId);

        eventStreamService.produce(newEvent(midtownMemberCastellanRemovedEvent));
    }

    public Castellan findCastellan(String castellanId) {
        //
        return castellanStore.retrieve(castellanId);
    }

    public boolean existsCastellanEmail(String email) {
        //
        return castellanQueryStore.castellanEmailExistsByEmail(email);
    }

    public CastellanEmail findCastellanEmailByEmail(String email) {
        //
        return castellanQueryStore.retrieveCastellanEmailByEmail(email);
    }


    private String newCastellanId() {
        //
        return UUID.randomUUID().toString();
    }

    private void createCastellan(Castellan castellan) {
        //
        castellanStore.create(castellan);

        CastellanEmail castellanEmail = new CastellanEmail(
                castellan.getEmails().genPrimaryEmail(),
                castellan.genDisplayName(),
                castellan.getId());
        castellanQueryStore.create(castellanEmail);

        CastellanName castellanName = new CastellanName(
                castellan.genDisplayName(),
                new EmailList(new Email(castellan.getEmails().genPrimaryEmail(), Tier.Primary, true)),
                castellan.getId());
        castellanQueryStore.create(castellanName);
    }
}
