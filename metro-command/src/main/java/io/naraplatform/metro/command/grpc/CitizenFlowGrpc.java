package io.naraplatform.metro.command.grpc;

import io.grpc.stub.StreamObserver;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import io.naraplatform.metro.command.flow.CitizenFlow;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterPavilionManagerCitizenCommand;
import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
import io.naraplatform.metro.proto.command.CitizenCommandProtoGrpc;
import io.naraplatform.metro.proto.command.RegisterCitizenCommandMsg;
import io.naraplatform.metro.proto.command.RegisterPavilionManagerCitizenCommandMsg;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

@Slf4j
@GRpcService
public class CitizenFlowGrpc extends CitizenCommandProtoGrpc.CitizenCommandProtoImplBase {
    //

    private final CitizenFlow citizenFlow;

    public CitizenFlowGrpc(CitizenFlow citizenFlow) {
        //
        this.citizenFlow = citizenFlow;
    }

    @Override
    public void registerCitizen(RegisterCitizenCommandMsg request, StreamObserver<RegisterCitizenCommandMsg> responseObserver) {
        //
        RegisterCitizenCommand command = MetroGrpcMessage.toCommand(request);
        try {
            command = citizenFlow.registerCitizen(command);
        } catch (Throwable e) {
            log.error("failed_to_register_citizen : \n{}", command, e);
            command.setFailureMessage(new FailureMessage(e.getClass().getSimpleName(), e.getMessage()));
        }

        responseObserver.onNext(MetroGrpcMessage.toMessage(command));
        responseObserver.onCompleted();
    }

    @Override
    public void registerPavilionManagerCitizen(RegisterPavilionManagerCitizenCommandMsg request, StreamObserver<RegisterPavilionManagerCitizenCommandMsg> responseObserver) {
        //
        RegisterPavilionManagerCitizenCommand command = MetroGrpcMessage.toCommand(request);
        try {
            command = citizenFlow.registerPavilionManagerCitizen(command);
        } catch (Throwable e) {
            log.error("failed_to_register_citizen : \n{}", command, e);
            command.setFailureMessage(new FailureMessage(e.getClass().getSimpleName(), e.getMessage()));
        }

        responseObserver.onNext(MetroGrpcMessage.toMessage(command));
        responseObserver.onCompleted();
    }
}
