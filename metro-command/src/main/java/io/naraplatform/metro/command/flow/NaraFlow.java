package io.naraplatform.metro.command.flow;


import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naraplatform.metro.command.service.NaraService;
import io.naraplatform.metro.domain.aggregate.nara.command.DeactivateNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.ModifyNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.ModifyPublicServantCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class NaraFlow {
    //
    private final NaraService naraService;

    public NaraFlow(NaraService naraService){
        //
        this.naraService = naraService;
    }

    public RegisterNaraCommand registerNara(RegisterNaraCommand registerNaraCommand) {
        //
        final Nara nara = naraService.registerNara(registerNaraCommand.getName(), registerNaraCommand.getType(),
                registerNaraCommand.getZoneId(),
                registerNaraCommand.getBaseLanguage(), registerNaraCommand.getSupportLanguages(),
                registerNaraCommand.getPublicServantName(), registerNaraCommand.getPublicServantEmail(),
                registerNaraCommand.genCommandIdentity());

        registerNaraCommand.setCommandResponse(new CommandResponse(nara.getId()));
        return registerNaraCommand;
    }

    public ModifyNaraCommand modifyNara(ModifyNaraCommand modifyNaraCommand) {
        //
        final Nara nara = naraService.modifyNara(modifyNaraCommand.getNaraId(), modifyNaraCommand.getNameValues(),
                modifyNaraCommand.genCommandIdentity());

        modifyNaraCommand.setCommandResponse(new CommandResponse(nara.getId()));
        return modifyNaraCommand;
    }

    public DeactivateNaraCommand deactivateNara(DeactivateNaraCommand deactivateNaraCommand) {
        //
        final Nara nara = naraService.deactivateNara(deactivateNaraCommand.getNaraId(),
                deactivateNaraCommand.genCommandIdentity());
        deactivateNaraCommand.setCommandResponse(new CommandResponse(nara.getId()));

        return deactivateNaraCommand;
    }

    public ModifyPublicServantCommand modifyPublicServant(ModifyPublicServantCommand modifyPublicServantCommand) {
        //
        final Nara nara = naraService.modifyPublicServant(modifyPublicServantCommand.getNaraId(),
                modifyPublicServantCommand.getNameValues(),
                modifyPublicServantCommand.genCommandIdentity());

        modifyPublicServantCommand.setCommandResponse(new CommandResponse(nara.getId()));
        return modifyPublicServantCommand;
    }
}
