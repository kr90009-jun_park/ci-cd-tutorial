package io.naraplatform.metro.command.service;

import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import io.naraplatform.share.domain.tenant.CitizenKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CitizenIdGenerator {
    //
    private final CitizenSequenceGenerator citizenSequenceGenerator;
    private int maxRetryCount = 5;
    private long sleepMilis = 300;

    public CitizenIdGenerator(CitizenSequenceGenerator citizenSequenceGenerator) {
        //
        this.citizenSequenceGenerator = citizenSequenceGenerator;
    }

    public synchronized String newCitizenId(String metroId) {
        //
        int retryCount = 0;
        int sequence = 0;
        while (retryCount < maxRetryCount) {
            try {
                sequence = citizenSequenceGenerator.nextSequence(metroId);
                break;
            }
            catch (Exception exc) {
                log.warn("exception({}) during citizenSequenceGenerator.nextSequence {}",
                        retryCount++, exc.toString());
                try {
                    Thread.sleep(sleepMilis);
                } catch (InterruptedException e) {
                    log.warn("InterruptedException during sleep");
                }
            }
        }
        if (retryCount == maxRetryCount) {
            log.error("retryCount == maxRetryCount");
            throw new NaraException(MetroExceptionMessage.citizenSequenceGeneratorMaximumRetryCountExceeded, maxRetryCount);
        }

        CitizenKey citizenKey = CitizenKey.newKey(metroId, sequence);

        return citizenKey.getKeyString();
    }
}
