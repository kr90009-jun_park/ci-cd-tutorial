package io.naraplatform.metro.command.service;

import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.CitizenSequenceBook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CitizenSequenceGenerator {
    //
    private final CitizenSequenceBook citizenSequenceBook;

    public CitizenSequenceGenerator(CitizenSequenceBook citizenSequenceBook) {
        //
        this.citizenSequenceBook = citizenSequenceBook;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int nextSequence(String metroId) {
        //
        return citizenSequenceBook.nextSequence(metroId);
    }
}
