package io.naraplatform.metro.command.flow;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.util.security.bcrypt.BCryptPasswordEncoder;
import io.naraplatform.checkpoint.domain.aggregate.loginuser.entity.LoginIdType;
import io.naraplatform.daysman.spec.EventStreamService;
import io.naraplatform.metro.command.proxy.CheckpointProxy;
import io.naraplatform.metro.command.proxy.EmailProxy;
import io.naraplatform.metro.command.service.CastellanService;
import io.naraplatform.metro.command.service.CitizenService;
import io.naraplatform.metro.command.service.MetroService;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naraplatform.metro.domain.aggregate.citizen.command.*;
import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.event.MailForCitizenInvitationResentEvent;
import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

import static io.naraplatform.metro.command.ServiceEventBuilder.newEvent;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class CitizenFlow {
    //
    private final CastellanService castellanService;
    private final CitizenService citizenService;
    private final CheckpointProxy checkpointProxy;
    private final EmailProxy emailProxy;
    private final EventStreamService eventStreamService;
    private final MetroService metroService;


    public RegisterPavilionManagerCitizenCommand registerPavilionManagerCitizen(RegisterPavilionManagerCitizenCommand registerPavilionManagerCitizenCommand) {
        //
        final Metro metro = metroService.findById(registerPavilionManagerCitizenCommand.getMetroId());
        if (metro == null) {
            metroService.registerMetro(registerPavilionManagerCitizenCommand.getMetroId(), "", "", registerPavilionManagerCitizenCommand.genCommandIdentity());
        }

        final String citizenId =registerCitizen(
                registerPavilionManagerCitizenCommand.getMetroId(),
                registerPavilionManagerCitizenCommand.getPavilionManagerName(),
                registerPavilionManagerCitizenCommand.getPavilionManagerEmail(),
                "",
                registerPavilionManagerCitizenCommand.getPavilionManagerEncryptedPassword(),
                true,
                registerPavilionManagerCitizenCommand.genCommandIdentity()
        );

        registerPavilionManagerCitizenCommand.setCommandResponse(new CommandResponse(citizenId));

        return registerPavilionManagerCitizenCommand;
    }



    public RegisterCitizenCommand registerCitizen(RegisterCitizenCommand registerCitizenCommand) {
        //
        final String citizenId = registerCitizen(registerCitizenCommand.getMetroId(), registerCitizenCommand.getName(),
                registerCitizenCommand.getEmail(), registerCitizenCommand.getUsid(),
                registerCitizenCommand.getEncryptedPassword(), registerCitizenCommand.isActivating(),
                registerCitizenCommand.genCommandIdentity());

        registerCitizenCommand.setCommandResponse(new CommandResponse(citizenId));
        return registerCitizenCommand;
    }

    public ActivatePreliminaryCitizenCommand activatePreliminary(
            ActivatePreliminaryCitizenCommand command) {
        //
        citizenService.verifySecretCode(command.getCitizenId(), command.getSecretCode());

        final Citizen citizen = citizenService.activatePreliminary(command.getCitizenId(),
                command.getPassword(),
                command.genCommandIdentity());

        String encryptedPassword = BCryptPasswordEncoder.encode(command.getPassword());
        checkpointProxy.registerLoginUser(citizen.getIdentity().getEmail(),
                citizen.getIdentity().getEmail(), citizen.getIdentity().getDisplayName(),
                encryptedPassword, LoginIdType.Email, new HashMap<>(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(citizen.getId()));
        return command;
    }


    public ActivatePreliminaryCitizenByManagerCommand activatePreliminaryByManager(
            ActivatePreliminaryCitizenByManagerCommand command) {
        //
        final Citizen citizen = citizenService.activatePreliminary(command.getCitizenId(),
                command.getPassword(),
                command.genCommandIdentity());

        String encryptedPassword = BCryptPasswordEncoder.encode(command.getPassword());
        checkpointProxy.registerLoginUser(citizen.getIdentity().getEmail(),
                citizen.getIdentity().getEmail(), citizen.getIdentity().getDisplayName(),
                encryptedPassword, LoginIdType.Email, new HashMap<>(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(citizen.getId()));
        return command;
    }


    public ModifyCitizenCommand modifyCitizen(ModifyCitizenCommand command) {
        //
        final Citizen citizen = citizenService.modifyCitizen(command.getCitizenId(),
                command.getNameValues(),
                command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(citizen.getId()));
        return command;
    }


    public ActivateCitizenCommand activateCitizen(ActivateCitizenCommand command) {
        //
        final Citizen citizen = citizenService.activateCitizen(command.getCitizenId(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(citizen.getId()));
        return command;
    }


    public DeactivateCitizenCommand deactivateCitizen(DeactivateCitizenCommand command) {
        //
        final Citizen citizen = citizenService.deactivateCitizen(command.getCitizenId(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(citizen.getId()));
        return command;
    }


    public RemoveCitizenCommand removeCitizen(RemoveCitizenCommand command) {
        //
        final Citizen citizen = citizenService.removeCitizen(command.getCitizenId(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(citizen.getId()));
        return command;
    }


    public ResendMailForCitizenInvitationCommand sendMailForInvitation(
            ResendMailForCitizenInvitationCommand command) {
        //
        //TODO : SecretCode가 왜 필요할까?
        final String secretCode = newSecretCode();
        final CitizenIdentity citizenIdentity = citizenService.retrieveCitizenIdentityById(command.getCitizenId());

        emailProxy.publishEmailForPreliminaryCitizenVerification(citizenIdentity.getId(),
                citizenIdentity.getDisplayName(), citizenIdentity.getEmail(), secretCode, command.genCommandIdentity());

        MailForCitizenInvitationResentEvent mailForCitizenInvitationResentEvent =
                new MailForCitizenInvitationResentEvent(
                        new IdName(citizenIdentity.getId(), Citizen.class.getSimpleName()), command.genCommandIdentity(), secretCode);
        eventStreamService.produce(newEvent(mailForCitizenInvitationResentEvent));


        command.setCommandResponse(new CommandResponse(citizenIdentity.getId()));
        return command;
    }

    private String newSecretCode() {
        //
        return String.format("%05d", RandomUtils.nextInt(1, 99999));
    }


    private String registerCitizen(String metroId,
                                   String name,
                                   String email,
                                   String usid,
                                   String encryptedPassword,
                                   boolean isActivating,
                                   CommandIdentity commandIdentity) {
        //
        final CastellanEmail castellanEmail =
                castellanService.findCastellanEmailByEmail(email);

        // castellan
        String castellanId = null;
        if (isActivating && castellanEmail == null) {
            Castellan castellan = castellanService.registerCastellan(
                    email,
                    name,
                    encryptedPassword,
                    commandIdentity);

            castellanId = castellan.getId();
            checkpointProxy.registerLoginUser(castellan.getId(),
                    castellan.getEmails().genPrimaryEmail(),
                    castellan.genDisplayName(),
                    encryptedPassword,
                    LoginIdType.Email,
                    new HashMap<>(),
                    commandIdentity);
        } else {
            castellanId = castellanEmail.getCastellanId();
        }

        // citizen
        String citizenId = null;
        if (isActivating) {
            citizenId = registerActiveCitizen(metroId, name, email,
                    castellanId, commandIdentity);
        } else {

            final String secretCode = newSecretCode();

            final CitizenIdentity citizenIdentity = citizenService.registerPreliminaryCitizen(
                    metroId, name, email,
                    usid, castellanId, secretCode, commandIdentity);

            emailProxy.publishEmailForPreliminaryCitizenVerification(citizenIdentity.getId(),
                    citizenIdentity.getDisplayName(),
                    citizenIdentity.getEmail(),
                    secretCode,
                    commandIdentity);

            citizenId = citizenIdentity.getId();
        }

        return citizenId;

    }


    private String registerActiveCitizen(String metroId, String citizenName, String email, String castellanId, CommandIdentity commandIdentity) {
        //
        String citizenId =
                citizenService.registerActiveCitizen(metroId,
                        citizenName,
                        email,
                        castellanId,
                        commandIdentity);

        return citizenId;
    }
}
