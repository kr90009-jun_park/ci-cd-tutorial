package io.naraplatform.metro.command.flow;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.util.security.bcrypt.BCryptPasswordEncoder;
import io.naraplatform.checkpoint.domain.aggregate.loginuser.entity.LoginIdType;
import io.naraplatform.metro.command.proxy.CheckpointProxy;
import io.naraplatform.metro.command.service.CastellanService;
import io.naraplatform.metro.command.service.CitizenIdGenerator;
import io.naraplatform.metro.command.service.CitizenService;
import io.naraplatform.metro.domain.aggregate.castellan.command.ModifyMidtownMemberCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RemoveMidtownMemberCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterMidtownMemberCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

@Service
@Slf4j
@Transactional
public class MidtownMemberCastellanFlow {
    //
    private final CastellanService castellanService;
    private final CitizenService citizenService;
    private final CheckpointProxy checkpointProxy;
    private final CitizenIdGenerator citizenIdGenerator;

    public MidtownMemberCastellanFlow(CastellanService castellanService,
                                      CitizenService citizenService,
                                      CheckpointProxy checkpointProxy,
                                      CitizenIdGenerator citizenIdGenerator) {
        //
        this.castellanService = castellanService;
        this.citizenService = citizenService;
        this.checkpointProxy = checkpointProxy;
        this.citizenIdGenerator = citizenIdGenerator;
    }


    public RegisterMidtownMemberCitizenCommand registerMidtownMemberCitizen(RegisterMidtownMemberCitizenCommand registerMidtownMemberCitizenCommand) {
        //
        Member member = registerMidtownMemberCitizenCommand.getMember();
        String email = member.getEmail();
        CastellanEmail castellanEmail = castellanService.findCastellanEmailByEmail(email);

        if (castellanEmail == null) {
            registerMidtownMemberCastellan(registerMidtownMemberCitizenCommand.getMember(), registerMidtownMemberCitizenCommand.genCommandIdentity());
            castellanEmail = castellanService.findCastellanEmailByEmail(email);
            log.debug("handle RegisterMidtownMemberCitizenCommand... registerMidtownMemberCastellan : result {}",
                    castellanEmail);
        }

        CitizenIdentity citizenIdentity
                = citizenService.findCitizenIdentityByMetroIdAndEmail(member.getPavilionId(), email);
        String castellanId = castellanEmail.getCastellanId();

        if (citizenIdentity == null || citizenIdentity.getState() == CitizenState.Removed) {
            Citizen citizen = citizenService.registerMidtownMemberCitizen(null,
                    castellanId, member, registerMidtownMemberCitizenCommand.genCommandIdentity());

            log.debug("handle RegisterMidtownMemberCitizenCommand... registerMidtownMemberCitizen : result {}, email {}",
                    citizen, email);
        } else {
            Citizen citizen = citizenService.modifyMidtownMemberCitizen(castellanId,
                    member, null, registerMidtownMemberCitizenCommand.genCommandIdentity());

            log.debug("handle RegisterMidtownMemberCitizenCommand... modifyMidtownMemberCitizen : result {}, email {}",
                    citizen, email);
        }

        log.debug("done handle MidtownMemberRegisteredEvent... {} {}", member.getNames().genString(), email);
        registerMidtownMemberCitizenCommand.setCommandResponse(new CommandResponse(castellanId));
        return registerMidtownMemberCitizenCommand;
    }

    public ModifyMidtownMemberCastellanCommand modifyMidtownMemberCastellan(ModifyMidtownMemberCastellanCommand command) {
        //
        Member member = command.getMember();
        String email = member.getEmail();
        CastellanEmail castellanEmail = castellanService.findCastellanEmailByEmail(email);
        String castellanId = null;

        if (castellanEmail == null) {
            Castellan castellan = registerMidtownMemberCastellan(member, command.genCommandIdentity());
            castellanId = castellan.getId();
            log.debug("handle ModifyMidtownMemberCastellanCommand... registerMidtownMemberCastellan : result {}, email {}",
                    command, email);
        } else {
            modifyMidtownMemberCastellan(castellanEmail.getCastellanId(),
                    member, command.getPreviousEmail(), command.genCommandIdentity());
            castellanId = castellanEmail.getCastellanId();

            log.debug("handle ModifyMidtownMemberCastellanCommand... modifyMidtownMemberCastellan : result {}, email {}",
                    command, email);
        }
        command.setCommandResponse(new CommandResponse(castellanId));
        return command;
    }

    public RemoveMidtownMemberCastellanCommand removeMidtownMemberCastellan(RemoveMidtownMemberCastellanCommand removeMidtownMemberCastellanCommand) {
        //
        Member member = removeMidtownMemberCastellanCommand.getMember();
        CastellanEmail castellanEmail = castellanService.findCastellanEmailByEmail(member.getEmail());

        if (castellanEmail == null) {
            log.warn("castellanEmail not found during MidtownMemberRemovedEvent. member {}", member);
            return removeMidtownMemberCastellanCommand;
        }

        String castellanId = castellanEmail.getCastellanId();
        castellanService.removeMidtownMemberCastellan(castellanId,
                removeMidtownMemberCastellanCommand.getMember(), null,
                removeMidtownMemberCastellanCommand.genCommandIdentity());

        CitizenIdentity citizenIdentity
                = citizenService.findCitizenIdentityByMetroIdAndEmail(member.getPavilionId(), castellanEmail.getEmail());
        if (citizenIdentity != null) {
            String citizenId = citizenIdentity.getId();
            citizenService.removeMidtownMemberCitizen(citizenId,
                    castellanId,
                    removeMidtownMemberCastellanCommand.getMember(),

                    removeMidtownMemberCastellanCommand.genCommandIdentity());
        }
        checkpointProxy.removeLoginUser(castellanId, removeMidtownMemberCastellanCommand.genCommandIdentity());

        removeMidtownMemberCastellanCommand.setCommandResponse(new CommandResponse(castellanId));
        return removeMidtownMemberCastellanCommand;
    }

    private Castellan registerMidtownMemberCastellan(
            Member member, CommandIdentity commandIdentity) {
        //
        String citizenId = newCitizenId(member.getPavilionId());

        final Castellan castellan
                = castellanService.registerMidtownMemberCastellan(member, citizenId,
                commandIdentity);

        String encryptedPassword = BCryptPasswordEncoder.encode(member.getPassword());

        checkpointProxy.registerLoginUser(
                castellan.getId(),
                castellan.getEmails().genPrimaryEmail(),
                castellan.genDisplayName(),
                encryptedPassword,
                LoginIdType.Email,
                new HashMap<>(),
                commandIdentity
        );

        citizenService.registerMidtownMemberCitizen(citizenId, castellan.getId(), member,
                commandIdentity);

        return castellan;
    }

    private Castellan modifyMidtownMemberCastellan(
            String castellanId, Member member, String previousEmail, CommandIdentity commandIdentity) {
        //
        checkpointProxy.modifyLoginUser(castellanId, member, commandIdentity);
        final Castellan castellan = castellanService.modifyMidtownMemberCastellan(castellanId,
                member, previousEmail,
                commandIdentity);

        citizenService.modifyMidtownMemberCitizen(castellanId, member, previousEmail,
                commandIdentity);

        return castellan;
    }

    private String newCitizenId(String metroId) {
        //
        return citizenIdGenerator.newCitizenId(metroId);
    }

}
