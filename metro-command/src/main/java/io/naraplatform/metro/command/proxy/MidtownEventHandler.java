package io.naraplatform.metro.command.proxy;

import io.naraplatform.daysman.client.event.DaysmanEventHandler;
import io.naraplatform.envoy.helper.CqrsRequesterManager;
import io.naraplatform.envoy.helper.SystemDramaRequestHelper;
import io.naraplatform.metro.command.flow.MidtownMemberCastellanFlow;
import io.naraplatform.metro.domain.aggregate.castellan.command.ModifyMidtownMemberCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RemoveMidtownMemberCastellanCommand;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterMidtownMemberCitizenCommand;
import io.naraplatform.midtown.domain.aggregate.member.event.MemberModifiedEvent;
import io.naraplatform.midtown.domain.aggregate.member.event.MemberRegisteredEvent;
import io.naraplatform.midtown.domain.aggregate.member.event.MemberRemovedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MidtownEventHandler {
    //
    private final MidtownMemberCastellanFlow midtownMemberCastellanFlow;

    public MidtownEventHandler(MidtownMemberCastellanFlow midtownMemberCastellanFlow) {
        //
        this.midtownMemberCastellanFlow = midtownMemberCastellanFlow;
    }

    @DaysmanEventHandler
    public void handle(MemberRegisteredEvent event) {
        //
        log.debug("handle MidtownMemberRegisteredEvent... {}", event.toPrettyJson());
        SystemDramaRequestHelper.assignSystemDramaRequestToContext();
        RegisterMidtownMemberCitizenCommand registerMidtownMemberCitizenCommand = new RegisterMidtownMemberCitizenCommand(
                event.getMemberId(),
                event.getMember()
        );
        CqrsRequesterManager.setRequester(event, registerMidtownMemberCitizenCommand);

        midtownMemberCastellanFlow.registerMidtownMemberCitizen(registerMidtownMemberCitizenCommand);
    }

    @DaysmanEventHandler
    public void handle(MemberModifiedEvent event) {
        //
        log.info("handle MidtownMemberModifiedEvent... {}", event.toPrettyJson());
        SystemDramaRequestHelper.assignSystemDramaRequestToContext();
        ModifyMidtownMemberCastellanCommand modifyMidtownMemberCastellanCommand = new ModifyMidtownMemberCastellanCommand(
                null,
                event.getMember(),
                event.getMemberId()
        );
        CqrsRequesterManager.setRequester(event, modifyMidtownMemberCastellanCommand);

        midtownMemberCastellanFlow.modifyMidtownMemberCastellan(modifyMidtownMemberCastellanCommand);
    }

    @DaysmanEventHandler
    public void handle(MemberRemovedEvent event) {
        //
        log.info("handle MidtownMemberRemovedEvent... {}", event.toPrettyJson());
        SystemDramaRequestHelper.assignSystemDramaRequestToContext();
        RemoveMidtownMemberCastellanCommand removeMidtownMemberCastellanCommand = new RemoveMidtownMemberCastellanCommand(event.getMember());
        CqrsRequesterManager.setRequester(event, removeMidtownMemberCastellanCommand);

        midtownMemberCastellanFlow.removeMidtownMemberCastellan(removeMidtownMemberCastellanCommand);
    }
}
