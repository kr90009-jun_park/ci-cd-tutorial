package io.naraplatform.metro.command.rest;

import io.naraplatform.metro.command.flow.CitizenFlow;
import io.naraplatform.metro.domain.aggregate.citizen.command.*;
import io.naraplatform.metro.domain.facade.aggregate.citizen.CitizenCommandFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("citizen")
public class CitizenCommandResource implements CitizenCommandFacade {
    //
    private final CitizenFlow citizenFlow;

    public CitizenCommandResource(CitizenFlow citizenFlow) {
        //
        this.citizenFlow = citizenFlow;
    }

    @Override
    @PostMapping("activate-preliminary")
    public ActivatePreliminaryCitizenCommand activatePreliminary(
            @RequestBody ActivatePreliminaryCitizenCommand activatePreliminaryCitizenCommand) {
        //
        return citizenFlow.activatePreliminary(activatePreliminaryCitizenCommand);
    }

    @Override
    @PostMapping("activate-preliminary-manager")
    public ActivatePreliminaryCitizenByManagerCommand activatePreliminaryByManager(
            @RequestBody ActivatePreliminaryCitizenByManagerCommand activatePreliminaryCitizenByManagerCommand) {
        //
        return citizenFlow.activatePreliminaryByManager(activatePreliminaryCitizenByManagerCommand);
    }

    @Override
    @PostMapping("modify")
    public ModifyCitizenCommand modifyCitizen(@RequestBody ModifyCitizenCommand modifyCitizenCommand) {
        //
        return citizenFlow.modifyCitizen(modifyCitizenCommand);
    }

    @Override
    @PostMapping("activate")
    public ActivateCitizenCommand activateCitizen(@RequestBody ActivateCitizenCommand activateCitizenCommand) {
        //
        return citizenFlow.activateCitizen(activateCitizenCommand);
    }

    @Override
    @PostMapping("deactivate")
    public DeactivateCitizenCommand deactivateCitizen(@RequestBody DeactivateCitizenCommand deactivateCitizenCommand) {
        //
        return citizenFlow.deactivateCitizen(deactivateCitizenCommand);
    }

    @Override
    @PostMapping("remove")
    public RemoveCitizenCommand removeCitizen(@RequestBody RemoveCitizenCommand removeCitizenCommand) {
        //
        return citizenFlow.removeCitizen(removeCitizenCommand);
    }

    @Override
    @PostMapping("send-mail-for-invitation")
    public ResendMailForCitizenInvitationCommand sendMailForInvitation(
            @RequestBody ResendMailForCitizenInvitationCommand resendMailForCitizenInvitationCommand) {
        //
        return citizenFlow.sendMailForInvitation(resendMailForCitizenInvitationCommand);
    }

    @Override
    @PostMapping("register-citizen")
    public RegisterCitizenCommand registerCitizen(
            @RequestBody RegisterCitizenCommand registerCitizenCommand) {
        //
        return citizenFlow.registerCitizen(registerCitizenCommand);
    }
}
