package io.naraplatform.metro.command.grpc;

import io.grpc.stub.StreamObserver;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import io.naraplatform.metro.command.flow.CastellanFlow;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
import io.naraplatform.metro.proto.command.CastellanCommandProtoGrpc;
import io.naraplatform.metro.proto.command.RegisterManagerCastellanCommandMsg;
import io.naraplatform.metro.proto.command.RegisterPavilionManagerCastellanCommandMsg;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

@Slf4j
@GRpcService
public class CastellanCommandGrpc extends CastellanCommandProtoGrpc.CastellanCommandProtoImplBase {
    //
    private final CastellanFlow castellanFlow;

    public CastellanCommandGrpc(CastellanFlow castellanFlow) {
        //
        this.castellanFlow = castellanFlow;
    }

    @Override
    public void registerManagerCastellan(RegisterManagerCastellanCommandMsg request, StreamObserver<RegisterManagerCastellanCommandMsg> responseObserver) {
        //
        RegisterManagerCastellanCommand command = MetroGrpcMessage.toCommand(request);
        try {
            command = castellanFlow.registerManagerCastellan(command);
        } catch (Throwable e) {
            log.error("failed_to_register_login_user : \n{}", command, e);
            log.error("failed_to_register_login_user : \n{}", command, e);
            log.error("failed_to_register_login_user : \n{}", command, e);
            log.error("failed_to_register_login_user : \n{}", command, e);
            command.setFailureMessage(new FailureMessage(e.getClass().getSimpleName(), e.getMessage()));
        }

        responseObserver.onNext(MetroGrpcMessage.toMessage(command));
        responseObserver.onCompleted();
    }

    @Override
    public void registerPavilionManagerCastellan(RegisterPavilionManagerCastellanCommandMsg request, StreamObserver<RegisterPavilionManagerCastellanCommandMsg> responseObserver) {
        //
        RegisterPavilionManagerCastellanCommand command = MetroGrpcMessage.toCommand(request);
        try {
            command = castellanFlow.registerPavilionManagerCastellan(command);
        } catch (Throwable e) {
            log.error("failed_to_register_nara : \n{}", command, e);
            command.setFailureMessage(new FailureMessage(e.getClass().getSimpleName(), e.getMessage()));
        }

        responseObserver.onNext(MetroGrpcMessage.toMessage(command));
        responseObserver.onCompleted();
    }
}
