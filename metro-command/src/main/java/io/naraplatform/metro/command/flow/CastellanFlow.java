package io.naraplatform.metro.command.flow;

import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.util.exception.NaraException;
import io.naraplatform.checkpoint.domain.aggregate.loginuser.entity.LoginIdType;
import io.naraplatform.metro.command.proxy.CheckpointProxy;
import io.naraplatform.metro.command.service.CastellanService;
import io.naraplatform.metro.command.service.CitizenService;
import io.naraplatform.metro.command.service.NaraService;
import io.naraplatform.metro.domain.aggregate.castellan.command.ModifySquareManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

@Service
@Transactional
public class CastellanFlow {
    //
    private CastellanService castellanService;
    private CitizenService citizenService;
    private NaraService naraService;
    private CheckpointProxy checkpointProxy;


    public CastellanFlow(CastellanService castellanService,
                         CitizenService citizenService,
                         NaraService naraService,
                         CheckpointProxy checkpointProxy) {
        //
        this.castellanService = castellanService;
        this.citizenService = citizenService;
        this.naraService = naraService;
        this.checkpointProxy = checkpointProxy;
    }


    public RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellan(
            RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellanCommand) {
        //

        throw new RuntimeException("unimplemented registerPavilionManagerCastellan");

//        final String citizenId = citizenService.newCitizenId(registerPavilionManagerCastellanCommand.getJoinedMetro());
//
//        final Castellan castellan
//                = castellanService.registerPavilionManagerCastellan(registerPavilionManagerCastellanCommand.getEmail(),
//                registerPavilionManagerCastellanCommand.getName(), registerPavilionManagerCastellanCommand.getJoinedMetro(),
//                registerPavilionManagerCastellanCommand.getEncryptedPassword(), citizenId,
//                registerPavilionManagerCastellanCommand.genCommandIdentity());
//
//        if (citizenId == null) {
//            throw new NaraException(MetroExceptionMessage.failedToRegisterCastellanForPavilionsManager);
//        }
//
//        String joinedMetroId = registerPavilionManagerCastellanCommand.getJoinedMetro();
//
//        String displayName = castellan.getNames().genString();
//        String email = castellan.getEmails().genPrimary().getEmail();
//
//        String pavilionManagerCitizenId = citizenService.registerPavilionManagerCitizen(
//                joinedMetroId,
//                LangString.newString("ko", displayName), email,
//                null,
//                citizenId,
//                castellan.getId(), "", registerPavilionManagerCastellanCommand.genCommandIdentity());
//
//        if (!citizenId.equals(pavilionManagerCitizenId)) {
//            throw new NaraException(MetroExceptionMessage.failedToRegisterCitizenForPavilionsManager);
//        }
//
//        checkpointProxy.registerLoginUser(
//                castellan.getId(), castellan.getEmails().genPrimaryEmail(), castellan.genDisplayName(),
//                registerPavilionManagerCastellanCommand.getEncryptedPassword(),
//                LoginIdType.Email, new HashMap<>(), registerPavilionManagerCastellanCommand.genCommandIdentity()
//        );
//
//        registerPavilionManagerCastellanCommand.setCommandResponse(new CommandResponse(castellan.getId()));
//        return registerPavilionManagerCastellanCommand;
    }

    public ModifySquareManagerCastellanCommand modifySquareManagerCastellan(ModifySquareManagerCastellanCommand modifySquareManagerCastellanCommand) {
        //
        String castellanId = modifySquareManagerCastellanCommand.getCastellanId();

        if (castellanService.modifySquareManagerCastellan(modifySquareManagerCastellanCommand.getCastellanId(),
                modifySquareManagerCastellanCommand.getSquareId(), modifySquareManagerCastellanCommand.getNameValues(),
                modifySquareManagerCastellanCommand.genCommandIdentity())) {
            boolean publicServantModificationNeeded = false;
            NameValueList userInformationNameValues = new NameValueList();
            NameValueList publicServantNameValues = new NameValueList();
            for (NameValue nameValue : modifySquareManagerCastellanCommand.getNameValues().list()) {
                String value = nameValue.getValue();
                switch (nameValue.getName()) {
                    case "name":
                        userInformationNameValues.add("displayName", value);
                        publicServantNameValues.add("displayName", value);
                        publicServantModificationNeeded = true;
                        break;
                    case "email":
                        userInformationNameValues.add("loginId", value);
                        publicServantNameValues.add("loginId", value);
                        publicServantModificationNeeded = true;
                        break;
                    case "encryptedPassword":
                        userInformationNameValues.add("encryptedPassword", value);
                        break;
                }
            }

            checkpointProxy.modifyLoginUser(castellanId, userInformationNameValues, modifySquareManagerCastellanCommand.genCommandIdentity());

            if (publicServantModificationNeeded) {
                String naraId = modifySquareManagerCastellanCommand.getSquareId();
                naraService.modifyPublicServant(naraId, publicServantNameValues,
                        modifySquareManagerCastellanCommand.genCommandIdentity());
            }
        }
        modifySquareManagerCastellanCommand.setCommandResponse(new CommandResponse(castellanId));
        return modifySquareManagerCastellanCommand;
    }



    public RegisterManagerCastellanCommand registerManagerCastellan(
             RegisterManagerCastellanCommand command) {
        //
        final Castellan castellan = castellanService.registerManagerCastellan(command.getManagerId(), command.getEmail(),
                command.getName(), command.getEncryptedPassword(),
                command.isStationManager(), command.genCommandIdentity());

        checkpointProxy.registerLoginUser(
                castellan.getId(),
                castellan.getEmails().genPrimaryEmail(),
                castellan.genDisplayName(),
                command.getEncryptedPassword(),
                LoginIdType.Email,
                new HashMap<>(),
                command.genCommandIdentity()
        );

        command.setCommandResponse(new CommandResponse(castellan.getId()));
        return command;
    }
}
