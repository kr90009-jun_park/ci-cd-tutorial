package io.naraplatform.metro.command.service;

import io.naraplatform.daysman.spec.EventStreamService;
import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naraplatform.metro.domain.aggregate.nara.entity.NaraIdentity;
import io.naraplatform.metro.domain.aggregate.nara.entity.NaraState;
import io.naraplatform.metro.domain.aggregate.nara.entity.settings.NaraLanguageSettings;
import io.naraplatform.metro.domain.aggregate.nara.entity.settings.NaraSettings;
import io.naraplatform.metro.domain.aggregate.nara.event.NaraDeactivatedEvent;
import io.naraplatform.metro.domain.aggregate.nara.event.NaraModifedEvent;
import io.naraplatform.metro.domain.aggregate.nara.event.NaraRegisteredEvent;
import io.naraplatform.metro.domain.aggregate.nara.event.PublicServantModifiedEvent;
import io.naraplatform.metro.store.domain.aggregate.nara.NaraStore;
import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.NaraSequenceBook;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naraplatform.share.domain.tenant.NaraKey;
import io.naraplatform.share.domain.tenant.NaraType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

import static io.naraplatform.metro.command.ServiceEventBuilder.newEvent;

@Service
@Transactional
@Slf4j
public class NaraService {
    //
    private final EventStreamService eventStreamService;
    private final NaraStore naraStore;
    private final NaraSequenceBook naraSequenceBook;

    public NaraService(EventStreamService eventStreamService,
                       NaraStore naraStore,
                       NaraSequenceBook naraSequenceBook) {
        //
        this.eventStreamService = eventStreamService;
        this.naraStore = naraStore;
        this.naraSequenceBook = naraSequenceBook;
    }

    public Nara registerNara(String name, NaraType type, String zoneId, String baseLanguage,
                             List<String> supportLanguages, String publicServantName,
                             String publicServantEmail, CommandIdentity commandIdentity) {
        //
        int sequence = naraSequenceBook.nextSequence();
        NaraKey naraKey = NaraKey.newKey(type, sequence);

        NaraIdentity identity = new NaraIdentity(naraKey, name);
        identity.setState(NaraState.Active);

        Locale locale = Locale.forLanguageTag(baseLanguage);

        NaraLanguageSettings naraLanguageSettings = new NaraLanguageSettings(
                baseLanguage,
                locale,
                supportLanguages,
                name
        );

        NaraSettings settings = new NaraSettings(
                zoneId,
                naraLanguageSettings,
                publicServantName,
                publicServantEmail);

        Nara nara = new Nara(identity, settings);

        naraStore.create(identity);
        naraStore.create(nara);

        NaraRegisteredEvent naraRegisteredEvent
                = new NaraRegisteredEvent(nara.genEntityIdName(),
                commandIdentity,
                nara.getId(),
                nara.getIdentity().getName(),
                zoneId,
                baseLanguage,
                supportLanguages,
                publicServantName,
                publicServantEmail);
        eventStreamService.produce(newEvent(naraRegisteredEvent));

        return nara;
    }


    public Nara modifyNara(String naraId, NameValueList nameValues, CommandIdentity commandIdentity) {
        //
        Nara nara = naraStore.retrieve(naraId);
        nara.setValues(nameValues);
        naraStore.update(nara);

        NaraIdentity identity = naraStore.retrieveNaraIdentity(naraId);
        identity.setValues(nameValues);
        naraStore.update(identity);

        NaraModifedEvent naraModifedEvent
                = new NaraModifedEvent(nara.genEntityIdName(),
                commandIdentity,
                nara.getId(),
                nameValues);
        eventStreamService.produce(newEvent(naraModifedEvent));

        return nara;
    }

    public Nara deactivateNara(String naraId, CommandIdentity commandIdentity) {
        //
        Nara nara = naraStore.retrieve(naraId);
        nara.getIdentity().setState(NaraState.Dormant);
        naraStore.update(nara);

        NaraIdentity identity = naraStore.retrieveNaraIdentity(naraId);
        identity.setState(NaraState.Dormant);
        naraStore.update(identity);

        NaraDeactivatedEvent naraDeactivatedEvent = new NaraDeactivatedEvent(nara.genEntityIdName(),
                commandIdentity,
                nara.getId());
        eventStreamService.produce(newEvent(naraDeactivatedEvent));
        return nara;
    }

    public Nara modifyPublicServant(String naraId, NameValueList nameValues, CommandIdentity commandIdentity) {
        //
        Nara nara = naraStore.retrieve(naraId);
        nara.getSettings().getServant().setValues(nameValues);
        naraStore.update(nara);

        PublicServantModifiedEvent publicServantModifiedEvent
                = new PublicServantModifiedEvent(nara.genEntityIdName(),
                commandIdentity,
                naraId,
                nameValues);
        eventStreamService.produce(newEvent(publicServantModifiedEvent));

        return nara;
    }
}
