package io.naraplatform.metro.command.proxy;

import io.naraplatform.daysman.client.event.DaysmanEventHandler;
import io.naraplatform.envoy.helper.CqrsRequesterManager;
import io.naraplatform.envoy.helper.SystemDramaRequestHelper;
import io.naraplatform.metro.command.flow.CastellanFlow;
import io.naraplatform.metro.command.flow.MetroFlow;
import io.naraplatform.metro.domain.aggregate.castellan.command.ModifySquareManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.RegisterMetroCommand;
import io.naraplatform.station.domain.aggregate.pavilion.event.ActivePavilionRegisteredEvent;
import io.naraplatform.station.domain.aggregate.pavilion.event.PavilionActivatedEvent;
import io.naraplatform.station.domain.aggregate.pavilion.event.PavilionDeactivatedEvent;
import io.naraplatform.station.domain.aggregate.pavilion.event.PreliminaryPavilionActivatedEvent;
import io.naraplatform.station.domain.aggregate.square.event.SquareManagerModifiedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class StationEventHandler {
    //
    private final MetroFlow metroFlow;
    private final CastellanFlow castellanFlow;

    public StationEventHandler(MetroFlow metroFlow, CastellanFlow castellanFlow) {
        //
        this.metroFlow = metroFlow;
        this.castellanFlow = castellanFlow;
    }

    @DaysmanEventHandler
    public void handle(PavilionDeactivatedEvent event) {
        //
        SystemDramaRequestHelper.assignSystemDramaRequestToContext();
        log.debug("handle PavilionDeactivatedEvent event {}", event.toPrettyJson());

        log.warn("unimplemented PavilionDeactivatedEvent handling");
    }

    @DaysmanEventHandler
    public void handle(PavilionActivatedEvent event) {
        //
        SystemDramaRequestHelper.assignSystemDramaRequestToContext();
        log.debug("handle PavilionActivatedEvent event {}", event.toPrettyJson());

        log.warn("unimplemented PavilionActivatedEvent handling");
    }

    @DaysmanEventHandler
    public void handle(PreliminaryPavilionActivatedEvent event) {
        //
        SystemDramaRequestHelper.assignSystemDramaRequestToContext();
        log.info("handle PreliminaryPavilionActivatedEvent... {}", event.toPrettyJson());
        RegisterMetroCommand command = new RegisterMetroCommand(
                event.getPavilionId(),
                event.getPavilionName(),
                event.getManagerId()
        );
        CqrsRequesterManager.setRequester(event, command);
        command = metroFlow.registerMetro(command);
        log.debug("handle PreliminaryPavilionActivatedEvent... registerMetro... {}", command);
    }

    @DaysmanEventHandler
    public void handle(ActivePavilionRegisteredEvent event) {
        //
        SystemDramaRequestHelper.assignSystemDramaRequestToContext();
        log.info("handle ActivePavilionRegisteredEvent... {}", event.toPrettyJson());
        RegisterMetroCommand command = new RegisterMetroCommand(
                event.getPavilionId(),
                event.getName(),
                event.getManagerCastellenId()
        );
        CqrsRequesterManager.setRequester(event, command);

        command = metroFlow.registerMetro(command);
        log.debug("handle ActivePavilionRegisteredEvent... registerMetro... {}", command);
    }

    @DaysmanEventHandler
    public void handle(SquareManagerModifiedEvent event) {
        //
        SystemDramaRequestHelper.assignSystemDramaRequestToContext();
        log.info("handle SquareManagerModifiedEvent... {}", event.toPrettyJson());
        ModifySquareManagerCastellanCommand command
                = new ModifySquareManagerCastellanCommand(
                event.getCastellanId(),
                event.getSquareId(),
                event.getNameValues()
        );
        CqrsRequesterManager.setRequester(event, command);

        command = castellanFlow.modifySquareManagerCastellan(command);
        log.debug("handle SquareManagerModifiedEvent... modifySquareManagerCastellan... {}", command);
    }
}
