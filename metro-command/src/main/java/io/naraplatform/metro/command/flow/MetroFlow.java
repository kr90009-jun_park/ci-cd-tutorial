package io.naraplatform.metro.command.flow;


import io.naraplatform.metro.command.service.MetroService;
import io.naraplatform.metro.domain.aggregate.metro.command.ActivateMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.DeactivateMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.RegisterMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.command.RemoveMetroCommand;
import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class MetroFlow {
    //
    private final MetroService metroService;

    public MetroFlow(MetroService metroService){
        //
        this.metroService = metroService;
    }

    public RegisterMetroCommand registerMetro(RegisterMetroCommand command) {
        //
        final Metro metro = metroService.registerMetro(command.getMetroId(), command.getName(),
                command.getOfficialId(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(metro.getId()));
        return command;
    }


    public ActivateMetroCommand activateMetro(ActivateMetroCommand command) {
        //
        final Metro metro = metroService.activateMetro(command.getMetroId(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(metro.getId()));
        return command;
    }


    public DeactivateMetroCommand deactivateMetro(DeactivateMetroCommand command) {
        //
        final Metro metro = metroService.deactivateMetro(command.getMetroId(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(metro.getId()));
        return command;
    }


    public RemoveMetroCommand removeMetro(RemoveMetroCommand command) {
        //
        final Metro metro = metroService.removeMetro(command.getMetroId(), command.genCommandIdentity());

        command.setCommandResponse(new CommandResponse(metro.getId()));
        return command;
    }
}
