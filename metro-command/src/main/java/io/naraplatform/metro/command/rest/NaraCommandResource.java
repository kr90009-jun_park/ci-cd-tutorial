package io.naraplatform.metro.command.rest;

import io.naraplatform.metro.command.flow.NaraFlow;
import io.naraplatform.metro.command.service.NaraService;
import io.naraplatform.metro.domain.aggregate.nara.command.DeactivateNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.ModifyNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.metro.domain.facade.aggregate.nara.NaraCommandFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("nara")
public class NaraCommandResource implements NaraCommandFacade {
    //
    private final NaraFlow naraFlow;

    public NaraCommandResource(NaraFlow naraFlow) {
        //
        this.naraFlow = naraFlow;
    }
    @Override
    @PostMapping("register-nara")
    public RegisterNaraCommand registerNara(@RequestBody RegisterNaraCommand registerNaraCommand) {
        //
        return naraFlow.registerNara(registerNaraCommand);
    }

    @Override
    @PostMapping("modify-nara")
    public ModifyNaraCommand modifyNara(@RequestBody ModifyNaraCommand modifyNaraCommand) {
        //
        return naraFlow.modifyNara(modifyNaraCommand);
    }

    @Override
    @PostMapping("deactivate-nara")
    public DeactivateNaraCommand deactivateNara(@RequestBody DeactivateNaraCommand deactivateNaraCommand) {
        //
        return naraFlow.deactivateNara(deactivateNaraCommand);
    }
}
