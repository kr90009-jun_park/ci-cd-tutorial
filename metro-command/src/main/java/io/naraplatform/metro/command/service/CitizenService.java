package io.naraplatform.metro.command.service;


import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.lang.LangString;
import io.naradrama.prologue.domain.lang.LangStrings;
import io.naradrama.prologue.util.exception.NaraException;
import io.naradrama.prologue.util.security.bcrypt.BCryptPasswordEncoder;
import io.naraplatform.daysman.spec.EventStreamService;
import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.metro.domain.aggregate.citizen.entity.EmailVerifedInfo;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenLanguageSettings;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naraplatform.metro.domain.aggregate.citizen.event.*;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.*;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import io.naraplatform.metro.store.domain.aggregate.citizen.CitizenStore;
import io.naraplatform.metro.store.query.aggregate.citizen.CitizenQueryStore;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import io.naraplatform.share.domain.tenant.CitizenKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static io.naraplatform.metro.command.ServiceEventBuilder.newEvent;

@Service
@Transactional
@Slf4j
public class CitizenService {
    //
    private final EventStreamService eventStreamService;
    private final CitizenStore citizenStore;
    private final CitizenQueryStore citizenQueryStore;
    private final CitizenIdGenerator citizenIdGenerator;

    public CitizenService(EventStreamService eventStreamService,
                          CitizenStore citizenStore,
                          CitizenQueryStore citizenQueryStore,
                          CitizenIdGenerator citizenIdGenerator) {
        //
        this.eventStreamService = eventStreamService;
        this.citizenStore = citizenStore;
        this.citizenQueryStore = citizenQueryStore;
        this.citizenIdGenerator = citizenIdGenerator;
    }

    public String registerActiveCitizen(
            String metroId,
            String name,
            String email,
            String castellanId,
            CommandIdentity commandIdentity) {
        //
        CitizenIdentity citizenIdentity = citizenStore.retrieveCitizenIdentityByMetroIdAndEmail(metroId, email);

        if (citizenIdentity != null) {
            throw new NaraException(MetroExceptionMessage.citizenAlreadyRegistered, metroId, email);
        }

        String citizenId = newCitizenId(metroId);
        CitizenIdentity identity = new CitizenIdentity(citizenId, name, email, castellanId);
        identity.setMetroId(metroId);
        identity.setState(CitizenState.Active);
        CitizenSettings settings = genCitizenSettings(citizenId, name);

        Citizen citizen = new Citizen(identity, settings);
        citizenStore.create(citizen);

        ActiveCitizen activeCitizen = new ActiveCitizen(identity);
        activeCitizen.setSettings(settings);

        citizenQueryStore.create(activeCitizen);
        citizenStore.create(identity);

        ActiveCitizenRegisteredEvent activeCitizenRegisteredEvent = new ActiveCitizenRegisteredEvent(
                citizen,
                commandIdentity,
                metroId,
                name,
                email,
                castellanId,
                settings);

        eventStreamService.produce(newEvent(activeCitizenRegisteredEvent));

        return citizenId;
    }

    public CitizenIdentity registerPreliminaryCitizen(
            String metroId,
            String name,
            String email,
            String usid,
            String castellanId,
            String secretCode,
            CommandIdentity commandIdentity) {
        //
        CitizenIdentity citizenIdentity = citizenStore.retrieveCitizenIdentityByMetroIdAndEmail(metroId, email);

        if (citizenIdentity != null) {
            throw new NaraException(MetroExceptionMessage.citizenAlreadyRegistered, metroId, email);
        }

        String citizenId = newCitizenId(metroId);

        if (castellanId == null) {
            castellanId = newCastellanId();
        }

        CitizenIdentity identity = new CitizenIdentity(citizenId, name, email, castellanId);
        identity.setMetroId(metroId);
        identity.setUsid(usid);
        identity.setState(CitizenState.Preliminary);
        CitizenSettings settings = genCitizenSettings(citizenId, name);

        Citizen citizen = new Citizen(identity, settings);
        citizen.setEmailVerifedInfo(new EmailVerifedInfo(email, secretCode));
        PreliminaryCitizen preliminaryCitizen = new PreliminaryCitizen(identity);
        preliminaryCitizen.setSettings(settings);

        citizenStore.create(citizen);
        citizenQueryStore.create(preliminaryCitizen);
        citizenStore.create(identity);

        PreliminaryCitizenRegisteredEvent preliminaryCitizenRegisteredEvent = new PreliminaryCitizenRegisteredEvent(
                citizen,
                commandIdentity,
                castellanId,
                metroId,
                name,
                email,
                secretCode,
                settings,
                usid);

        eventStreamService.produce(newEvent(preliminaryCitizenRegisteredEvent));

        return identity;
    }

    public String registerPavilionManagerCitizen(
            String metroId,
            LangString name,
            String email,
            String encryptedPassword,
            String predefinedId,
            String castellanId,
            String usid,
            CommandIdentity commandIdentity) {
        //
        String citizenId = null;
        if (predefinedId == null) {
            citizenId = newCitizenId(metroId);
        } else {
            citizenId = predefinedId;
        }

        String displayName = name.getString();
        CitizenIdentity identity = new CitizenIdentity(citizenId, displayName, email, castellanId);
        identity.setMetroId(metroId);
        identity.setUsid(usid);
        identity.setState(CitizenState.Active);

        CitizenSettings settings = genCitizenSettings(citizenId, name);
        Citizen citizen = new Citizen(identity, settings);

        citizenStore.create(citizen);

        ActiveCitizen activeCitizen
                = new ActiveCitizen(identity);
        activeCitizen.setSettings(settings);

        citizenQueryStore.create(activeCitizen);
        citizenStore.create(identity);

        PavilionManagerCitizenRegisteredEvent pavilionManagerCitizenRegisteredEvent =
                new PavilionManagerCitizenRegisteredEvent(
                        citizen,
                        commandIdentity,
                        metroId,
                        displayName,
                        email,
                        encryptedPassword,
                        usid,
                        castellanId,
                        settings);

        eventStreamService.produce(newEvent(pavilionManagerCitizenRegisteredEvent));

        return citizenId;
    }

    public void verifySecretCode(String citizenId, String secretCode) {
        //
        Citizen citizen = citizenStore.retrieve(citizenId);
        if (citizen == null) {
            throw new NaraException(MetroExceptionMessage.cannotFindEntityById, Citizen.class.getSimpleName(), citizenId);
        }

        Optional<String> optSecretCode = Optional.ofNullable(citizen.getEmailVerifedInfo().getSecretCode());

        if (!optSecretCode.isPresent() ||
                !optSecretCode.get().equals(secretCode)) {
            throw new NaraException(MetroExceptionMessage.secretCodeDoesNotMatch);
        }
    }

    public Citizen activatePreliminary(String citizenId, String password, CommandIdentity commandIdentity) {
        //
        Citizen citizen = citizenStore.retrieve(citizenId);

        String encryptedPassword = BCryptPasswordEncoder.encode(password);

        citizen.getIdentity().setState(CitizenState.Active);
        citizenStore.update(citizen);
        activatePreliminaryCitizen(citizenId);

        PreliminaryCitizenActivatedEvent preliminaryCitizenActivatedEvent = new PreliminaryCitizenActivatedEvent(
                citizen, commandIdentity, encryptedPassword);

        eventStreamService.produce(newEvent(preliminaryCitizenActivatedEvent));
        return citizen;
    }

    public Citizen modifyCitizen(String citizenId, NameValueList nameValues, CommandIdentity commandIdentity) {
        //
        Citizen citizen = citizenStore.retrieve(citizenId);
        citizen.setValues(nameValues);
        citizenStore.update(citizen);

        AbstractCitizen abstractCitizen = citizenQueryStore.retrieve(citizenId);
        abstractCitizen.setValues(nameValues);
        citizenQueryStore.update(abstractCitizen, abstractCitizen.isIdentityModified());

        CitizenModifiedEvent citizenModifiedEvent = new CitizenModifiedEvent(
                citizen, commandIdentity, nameValues);

        eventStreamService.produce(newEvent(citizenModifiedEvent));
        return citizen;
    }

    public Citizen activateCitizen(String citizenId, CommandIdentity commandIdentity) {
        //
        Citizen citizen = citizenStore.retrieve(citizenId);
        citizen.getIdentity().setState(CitizenState.Active);
        citizenStore.update(citizen);

        DormantCitizen dormantCitizen
                = (DormantCitizen) citizenQueryStore.retrieve(citizenId, CitizenState.Dormant);
        if (dormantCitizen == null) {
            return citizen;
        }
        ActiveCitizen activeCitizen = new ActiveCitizen(dormantCitizen);
        citizenQueryStore.create(activeCitizen);
        modifyCitizenIdentityState(citizenId, CitizenState.Active);
        citizenQueryStore.delete(citizenId, CitizenState.Dormant);

        CitizenActivatedEvent citizenActivatedEvent = new CitizenActivatedEvent(citizen, commandIdentity);
        eventStreamService.produce(newEvent(citizenActivatedEvent));
        return citizen;
    }

    public Citizen deactivateCitizen(String citizenId, CommandIdentity commandIdentity) {
        //
        Citizen citizen = citizenStore.retrieve(citizenId);
        citizen.getIdentity().setState(CitizenState.Dormant);
        citizenStore.update(citizen);

        ActiveCitizen activeCitizen
                = (ActiveCitizen) citizenQueryStore.retrieve(citizenId, CitizenState.Active);

        DormantCitizen dormantCitizen = new DormantCitizen(activeCitizen);
        citizenQueryStore.create(dormantCitizen);
        modifyCitizenIdentityState(citizenId, CitizenState.Dormant);
        citizenQueryStore.delete(citizenId, CitizenState.Active);

        CitizenDeactivatedEvent citizenDeactivatedEvent = new CitizenDeactivatedEvent(citizen, commandIdentity);
        eventStreamService.produce(newEvent(citizenDeactivatedEvent));
        return citizen;
    }

    //    todo remove citizen : station 부터 시작하도록 변경
    public Citizen removeCitizen(String citizenId, CommandIdentity commandIdentity) {
        //
        Citizen citizen = citizenStore.retrieve(citizenId);

        citizen.getIdentity().setState(CitizenState.Removed);
        citizenStore.update(citizen);

        DormantCitizen dormantCitizen =
                (DormantCitizen) citizenQueryStore.retrieve(citizenId, CitizenState.Dormant);

        RemovedCitizen removedCitizen = new RemovedCitizen(dormantCitizen);
        citizenQueryStore.create(removedCitizen);
        modifyCitizenIdentityState(citizenId, CitizenState.Removed);
        citizenQueryStore.delete(citizenId, CitizenState.Dormant);

        CitizenRemovedEvent citizenRemovedEvent = new CitizenRemovedEvent(citizen, commandIdentity);
        eventStreamService.produce(newEvent(citizenRemovedEvent));
        return citizen;
    }


    public CitizenIdentity retrieveCitizenIdentityById(String id) {
        //
        return citizenStore.retrieveCitizenIdentity(id);
    }

    public Citizen retrieveCitizenById(String id) {
        //
        return citizenStore.retrieve(id);
    }

    public Citizen registerMidtownMemberCitizen(String citizenId, String castellanId, Member member, CommandIdentity commandIdentity) {
        //
        String metroId = member.getPavilionId();
        if (citizenId == null) {
            citizenId = newCitizenId(metroId);
        }
        String displayName = member.getNames().genString();

        CitizenIdentity identity = new CitizenIdentity(citizenId,
                displayName,
                member.getEmail(),
                castellanId);
        identity.setMetroId(metroId);
        identity.setUsid(member.getUsid());
        identity.setState(CitizenState.Active);
        CitizenSettings settings = genCitizenSettings(citizenId, displayName);

        Citizen citizen = new Citizen(identity, settings);
        citizenStore.create(citizen);

        ActiveCitizen activeCitizen
                = new ActiveCitizen(identity);
        activeCitizen.setSettings(settings);

        citizenQueryStore.create(activeCitizen);
        citizenStore.create(identity);

        MidtownMemberCitizenRegisteredEvent midtownMemberCitizenRegisteredEvent =
                new MidtownMemberCitizenRegisteredEvent(citizen, commandIdentity, castellanId, member);
        eventStreamService.produce(newEvent(midtownMemberCitizenRegisteredEvent));

        return citizen;
    }

    public Citizen modifyMidtownMemberCitizen(String castellanId, Member member, String previousEmail,
                                              CommandIdentity commandIdentity) {
        //
        String email = member.getEmail();
        if (previousEmail != null) {
            email = previousEmail;
        }
        CitizenIdentity citizenIdentity
                = findCitizenIdentityByMetroIdAndEmail(member.getPavilionId(), email);
        if (citizenIdentity == null) {
            String exceptionMessage = String.format("citizen_identity_not_found_exception_by_metroId(%s)_and_email(%s)", member.getPavilionId(), email);
            log.debug(exceptionMessage);
            throw new NaraException(MetroExceptionMessage.cannotFindCitizenIdentityByMetroIdAndEmail, member.getPavilionId(), email);
        }

        String citizenId = citizenIdentity.getId();
        Citizen citizen = citizenStore.retrieve(citizenId);

        String displayName = member.getNames().genString();
        citizen.getIdentity().setDisplayName(displayName);
        citizen.getIdentity().setEmail(member.getEmail());
        citizen.getSettings().getLanguageSettings().setNames(member.getNames());
        citizenStore.update(citizen);

        AbstractCitizen abstractCitizen = citizenQueryStore.retrieve(citizenId);
        abstractCitizen.getIdentity().setDisplayName(displayName);
        abstractCitizen.getIdentity().setEmail(member.getEmail());
        abstractCitizen.getSettings().getLanguageSettings().setNames(member.getNames());
        citizenQueryStore.update(abstractCitizen, true);

        MidtownMemberCitizenModifiedEvent midtownMemberCitizenModifiedEvent =
                new MidtownMemberCitizenModifiedEvent(citizen, commandIdentity, castellanId, member, previousEmail);

        eventStreamService.produce(newEvent(midtownMemberCitizenModifiedEvent));
        return citizen;
    }

    public Citizen removeMidtownMemberCitizen(String citizenId, String castellanId, Member member,
                                              CommandIdentity commandIdentity) {
        //
        Citizen citizen = citizenStore.retrieve(citizenId);
        citizen.getIdentity().setState(CitizenState.Removed);
        citizenStore.update(citizen);

        citizenQueryStore.deleteAllByCitizenId(citizenId);

        MidtownMemberCitizenRemovedEvent midtownMemberCitizenRemovedEvent =
                new MidtownMemberCitizenRemovedEvent(citizen, commandIdentity, castellanId, member);

        eventStreamService.produce(newEvent(midtownMemberCitizenRemovedEvent));
        return citizen;
    }

    public CitizenIdentity findCitizenIdentityByMetroIdAndEmail(String metroId, String email) {
        //
        return citizenStore.retrieveCitizenIdentityByMetroIdAndEmail(metroId, email);
    }

    private String newCastellanId() {
        //
        return UUID.randomUUID().toString();
    }

    public String newCitizenId(String metroId) {
        //
        return citizenIdGenerator.newCitizenId(metroId);
    }

    private CitizenSettings genCitizenSettings(String citizenId, String name) {
        //
        String baseLanguage = "ko"; //Locale.KOREAN.getLanguage();
        CitizenLanguageSettings languageSettings = new CitizenLanguageSettings();
        languageSettings.setBaseLanguage(baseLanguage);
        languageSettings.setNames(LangStrings.newString(baseLanguage, name));

        return new CitizenSettings(CitizenKey.fromKeyString(citizenId), languageSettings);
    }

    private CitizenSettings genCitizenSettings(String citizenId, LangString name) {
        //
        CitizenLanguageSettings languageSettings = new CitizenLanguageSettings();
        languageSettings.setBaseLanguage(name.getLanguage());
        languageSettings.setNames(LangStrings.newString(name));

        return new CitizenSettings(CitizenKey.fromKeyString(citizenId), languageSettings);
    }

    private void activatePreliminaryCitizen(String citizenId) {
        //
        PreliminaryCitizen preliminaryCitizen =
                (PreliminaryCitizen) citizenQueryStore.retrieve(citizenId, CitizenState.Preliminary);

        if (preliminaryCitizen == null) {
            return;
        }
        ActiveCitizen activeCitizen = new ActiveCitizen(preliminaryCitizen);
        CitizenIdentity identity = activeCitizen.getIdentity();
        identity.setState(CitizenState.Active);

        citizenQueryStore.create(activeCitizen);
        citizenStore.update(identity);
        citizenQueryStore.delete(citizenId, CitizenState.Preliminary);
    }

    private void modifyCitizenIdentityState(String citizenId, CitizenState state) {
        //
        CitizenIdentity citizenIdentity = citizenStore.retrieveCitizenIdentity(citizenId);
        citizenIdentity.setState(state);
        citizenStore.update(citizenIdentity);
    }
}
