package io.naraplatform.metro.command.proxy;


import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.lang.LangStrings;
import io.naradrama.prologue.domain.workspace.Workspace;
import io.naradrama.prologue.util.exception.NaraException;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naraplatform.checkpoint.domain.aggregate.loginuser.command.ModifyLoginUserCommand;
import io.naraplatform.checkpoint.domain.aggregate.loginuser.command.RegisterLoginUserCommand;
import io.naraplatform.checkpoint.domain.aggregate.loginuser.command.RegisterLoginUserWorkspaceCommand;
import io.naraplatform.checkpoint.domain.aggregate.loginuser.command.RemoveLoginUserCommand;
import io.naraplatform.checkpoint.domain.aggregate.loginuser.entity.LoginIdType;
import io.naraplatform.checkpoint.domain.facade.client.LoginUserClient;
import io.naraplatform.metro.domain.constant.MetroExceptionMessage;
import io.naraplatform.midtown.domain.aggregate.member.entity.Member;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class CheckpointProxy {
    //
    private final LoginUserClient loginUserClient;

    public CheckpointProxy(LoginUserClient loginUserClient) {
        //
        this.loginUserClient = loginUserClient;
    }

    public String registerLoginUser(String userId, String loginId, String displayName, String encryptedPassword,
                                    LoginIdType idType, Map<String, String> additionalInformation, CommandIdentity commandIdentity) {
        //
        RegisterLoginUserCommand registerLoginUserCommand = new RegisterLoginUserCommand(userId, loginId, displayName, encryptedPassword, idType, additionalInformation);
        RegisterLoginUserCommand response = loginUserClient.registerLoginUser(registerLoginUserCommand, commandIdentity);

        if (response.getFailureMessage() != null && !StringUtils.isEmpty(response.getFailureMessage().getExceptionName())) {
            String exceptionMessage = String.format("failed to register login user : %s", userId);
            log.debug(exceptionMessage);
            throw new NaraException(MetroExceptionMessage.failedToRegisterLoginUser, userId);
        }

        String loginUserId = null;
        if (response.getResponse() != null && !response.getResponse().getEntityIds().isEmpty()) {
            loginUserId = response.getResponse().getEntityIds().get(0);
        }

        log.debug("castellanId : {}, loginUserId : {}", userId, loginUserId);

        return loginUserId;
    }

    public void modifyLoginUser(String castellanId, NameValueList nameValues, CommandIdentity commandIdentity) {
        //
        ModifyLoginUserCommand modifyLoginUserCommand
                = new ModifyLoginUserCommand(castellanId, nameValues);

        modifyLoginUser(modifyLoginUserCommand, commandIdentity);
    }

    public void modifyLoginUser(String castellanId, Member member, CommandIdentity commandIdentity) {
        //
        LangStrings names = member.getNames();
        NameValueList nameValues = new NameValueList();
        nameValues.add("loginId", member.getEmail());
        nameValues.add("displayName", names.genString());
        nameValues.add("additionalInformation", JsonUtil.toJson(member.getAdditionalInformation()));

        ModifyLoginUserCommand modifyLoginUserCommand = new ModifyLoginUserCommand(
                castellanId, nameValues);

        modifyLoginUser(modifyLoginUserCommand, commandIdentity);
    }

    private void modifyLoginUser(ModifyLoginUserCommand modifyLoginUserCommand, CommandIdentity commandIdentity) {
        //
        ModifyLoginUserCommand response = loginUserClient.modifyLoginUser(modifyLoginUserCommand, commandIdentity);
        if (response.getFailureMessage() != null && !StringUtils.isEmpty(response.getFailureMessage().getExceptionName())) {
            log.warn("loginUserClient.modifyLoginUser {}", response.toPrettyJson());
            throw new NaraException(MetroExceptionMessage.failedToModifyLoginUser);
        }
    }

    public void removeLoginUser(String castellanId, CommandIdentity commandIdentity) {
        //
        RemoveLoginUserCommand removeLoginUserCommand = new RemoveLoginUserCommand(castellanId);

        RemoveLoginUserCommand response = loginUserClient.removeLoginUser(removeLoginUserCommand, commandIdentity);
        if (response.getFailureMessage() != null && !StringUtils.isEmpty(response.getFailureMessage().getExceptionName())) {
            log.warn("loginUserClient.removeLoginUser {}", response.toPrettyJson());
            throw new NaraException(MetroExceptionMessage.failedToRemoveLoginUser);
        }
    }

    public void registerLoginUserWorkspace(String castellanId, Workspace workspace, CommandIdentity commandIdentity) {
        //
        RegisterLoginUserWorkspaceCommand registerLoginUserWorkspaceCommand = new RegisterLoginUserWorkspaceCommand(
                castellanId, workspace);

        // 추후 테스트
        RegisterLoginUserWorkspaceCommand response = loginUserClient.registerLoginUserWorkspace(registerLoginUserWorkspaceCommand, commandIdentity);
        if (response.getFailureMessage() != null && !StringUtils.isEmpty(response.getFailureMessage().getExceptionName())) {
            log.warn("loginUserClient.registerLoginUserWorkspace {}", response.toPrettyJson());
            throw new NaraException(MetroExceptionMessage.failedToRegisterUserWorkspace);
        }
    }

}