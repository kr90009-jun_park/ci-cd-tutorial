package io.naraplatform.metro.command.proxy;


import io.naradrama.pigeon.domain.aggregate.email.command.SendEmailCommand;
import io.naradrama.pigeon.domain.aggregate.email.entity.EmailMessage;
import io.naradrama.pigeon.domain.aggregate.email.entity.EmailTemplate;
import io.naradrama.pigeon.domain.facade.client.EmailClient;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class EmailProxy {
    //
    private final EmailClient emailClient;

    @Value("${nara.station.baseUrl:http://test}")
    private String stationFrontBaseUrl;

    public EmailProxy(EmailClient emailClient) {
        //
        this.emailClient = emailClient;
    }

    public void publishEmailForPreliminaryCitizenVerification(
            String citizenId, String citizenName, String citizenEmail, String secretCode, CommandIdentity commandIdentity) {
        //
        String uri = String.format("/cinerooms/verifyEmail?reason=email-verifying&citizenId=%s&email=%s",
                citizenId,
                citizenEmail);

        String url = String.format("%s%s", stationFrontBaseUrl, uri);

        String contents = String.format("Email 확인 중입니다. 아래 링크를 방문하여 Email 인증을 완료하세요. (Secret Code : %s)",
                secretCode);
        buildAndSendEmailMessage("[Nara Platform] Confirmation for Citizen Account",
                citizenEmail,
                citizenName,
                url,
                contents, commandIdentity);
    }

    private void buildAndSendEmailMessage(
            String subject, String email, String name, String url, String contents, CommandIdentity commandIdentity) {
        //
        EmailMessage emailMessage = buildMailMessageFromTemplateForAuthMail(
                subject, email, name, url, contents);

        SendEmailCommand sendEmailCommand = new SendEmailCommand();
        sendEmailCommand.setEmailMessage(emailMessage);
        sendEmailCommand.setRequester(commandIdentity.getRequester());
        emailClient.sendMail(sendEmailCommand, commandIdentity);
    }

    private EmailMessage buildMailMessageFromTemplateForAuthMail
            (String subject, String toAddress,
             String userName, String url, String contents) {
        //
        EmailMessage emailMessage = new EmailMessage(
                toAddress,
                subject,
                ""
        );
        EmailTemplate template = new EmailTemplate();
        template.setName("AuthMail");

        Map<String, String> stringsMap = new HashMap<>();
        stringsMap.put("userName", userName);
        stringsMap.put("href", url);
        stringsMap.put("contents", contents);

        template.setStrings(stringsMap);

        Map<String, String> imagesMap = new HashMap<>();
        imagesMap.put("emailicon", "email-icon.png");
        template.setImages(imagesMap);

        emailMessage.setTemplate(template);

        return emailMessage;
    }
}
