package io.naraplatform.metro.command;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.RequesterType;
import io.naradrama.prologue.domain.cqrs.broker.StreamEventMessage;
import io.naradrama.prologue.domain.cqrs.event.CqrsEvent;

public interface ServiceEventBuilder {
    //
    String serviceName = "io.naraplatform.metro";

    static StreamEventMessage newEvent(CqrsEvent cqrsEvent) {
        //
        return new StreamEventMessage(cqrsEvent);
    }

    static CommandIdentity genTopicTypeCommandIdentity(CqrsEvent event) {
        //
        return new CommandIdentity(event.getCommandId(),
                new Requester(event.getId(), RequesterType.Topic, ServiceEventBuilder.serviceName));
    }
}
