package io.naraplatform.metro.command.rest;

import io.naraplatform.metro.command.flow.CastellanFlow;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.metro.domain.facade.aggregate.castellan.CastellanCommandFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("castellan")
public class CastellanCommandResource implements CastellanCommandFacade {
    //
    private final CastellanFlow castellanFlow;

    public CastellanCommandResource(CastellanFlow castellanFlow) {
        //
        this.castellanFlow = castellanFlow;
    }

    @Override
    @PostMapping("registerManagerCastellan")
    public RegisterManagerCastellanCommand registerManagerCastellan(
            @RequestBody RegisterManagerCastellanCommand registerManagerCastellanCommand) {
        //
        return castellanFlow.registerManagerCastellan(registerManagerCastellanCommand);
    }

    @Override
    @PostMapping("registerPavilionManagerCastellan")
    public RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellan(
            @RequestBody RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellanCommand) {
        //
        return castellanFlow.registerPavilionManagerCastellan(registerPavilionManagerCastellanCommand);
    }
}
