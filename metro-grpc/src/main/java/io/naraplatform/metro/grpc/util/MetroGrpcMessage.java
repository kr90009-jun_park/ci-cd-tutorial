package io.naraplatform.metro.grpc.util;

import com.google.protobuf.BoolValue;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import com.google.protobuf.StringValue;
import io.naradrama.prologue.grpc.PrologueGrpcMessage;
import io.naradrama.prologue.grpc.cqrs.GrpcCqrsMessageUtils;
import io.naraplatform.metro.domain.aggregate.OptionalSettings;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailQuery;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterPavilionManagerCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenLanguageSettings;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentitiesByIdsQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityByMetroIdAndEmailQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityQuery;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.AbstractCitizen;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.metro.proto.command.*;
import io.naraplatform.metro.proto.message.*;
import io.naraplatform.metro.proto.query.FindCastellanEmailByEmailQueryMsg;
import io.naraplatform.metro.proto.query.FindCitizenIdentitiesByIdsQueryMsg;
import io.naraplatform.metro.proto.query.FindCitizenIdentityByMetroIdAndEmailQueryMsg;
import io.naraplatform.metro.proto.query.FindCitizenIdentityQueryMsg;
import io.naraplatform.protobuf.tenant.NaraTypeMsg;
import io.naraplatform.share.domain.tenant.NaraType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MetroGrpcMessage {
    //
    // for gRPC Message ------------------------------------------------------------------------------------------------
    public static StringValue toStringValue(String value) {
        //
        return StringValue.newBuilder()
                .setValue(value)
                .build();
    }

    public static BoolValue toBoolValue(boolean value) {
        //
        return BoolValue.newBuilder()
                .setValue(value)
                .build();
    }

    public static Empty toEmpty() {
        //
        return Empty.newBuilder().build();
    }

    public static RegisterManagerCastellanCommand toCommand(RegisterManagerCastellanCommandMsg message) {
        //
        RegisterManagerCastellanCommand command = new RegisterManagerCastellanCommand();

        GrpcCqrsMessageUtils.applyCqrsCommandMsgToCqrsCommand(message.getCqrsUserCommand(), command);
        command.setResponse(GrpcCqrsMessageUtils.fromMessage(message.getResponse()));

        command.setManagerId(message.getManagerId());
        command.setEmail(message.getEmail());
        command.setName(PrologueGrpcMessage.fromMessage(message.getName()));
        command.setEncryptedPassword(message.getEncryptedPassword());
        command.setStationManager(message.getStationManager());

        return command;
    }

    public static RegisterManagerCastellanCommandMsg toMessage(RegisterManagerCastellanCommand command){
        //
        RegisterManagerCastellanCommandMsg.Builder builder = RegisterManagerCastellanCommandMsg.newBuilder();
        builder.setCqrsUserCommand(GrpcCqrsMessageUtils.buildCqrsCommandMsg(command));
        if(command.getResponse()!=null){
            builder.setResponse(GrpcCqrsMessageUtils.toMessage(command.getResponse()));
        }

        builder.setManagerId(command.getManagerId());
        builder.setEmail(command.getEmail());
        builder.setName(PrologueGrpcMessage.toMessage(command.getName()));
        builder.setEncryptedPassword(command.getEncryptedPassword());
        builder.setStationManager(command.isStationManager());

        return builder.build();
    }


//    @Deprecated
//    public static AddManagingWorkspaceCommand toCommand(AddManagingWorkspaceCommandMsg message) {
//        //
//
//        if(true){
//            throw new RuntimeException("Removed method");
//        }
//
//        AddManagingWorkspaceCommand command = new AddManagingWorkspaceCommand();
//
//        GrpcCqrsMessageUtils.applyCqrsCommandMsgToCqrsCommand(message.getCqrsUserCommand(), command);
//        command.setResponse(GrpcCqrsMessageUtils.fromMessage(message.getResponse()));
//
//        command.setManagerId(message.getManagerId());
//        command.setCastellanId(message.getCastellanId());
//
//        return command;
//    }
//
//    @Deprecated
//    public static AddManagingWorkspaceCommandMsg toMessage(AddManagingWorkspaceCommand command){
//        //
//        if(true){
//            throw new RuntimeException("Removed method");
//        }
//        AddManagingWorkspaceCommandMsg.Builder builder = AddManagingWorkspaceCommandMsg.newBuilder();
//        builder.setCqrsUserCommand(GrpcCqrsMessageUtils.buildCqrsCommandMsg(command));
//        if(command.getResponse()!=null){
//            builder.setResponse(GrpcCqrsMessageUtils.toMessage(command.getResponse()));
//        }
//
//        builder.setManagerId(command.getManagerId());
//        builder.setCastellanId(command.getCastellanId());
//
//        return builder.build();
//    }

    public static RegisterPavilionManagerCastellanCommand toCommand(RegisterPavilionManagerCastellanCommandMsg message) {
        //
        RegisterPavilionManagerCastellanCommand command = new RegisterPavilionManagerCastellanCommand();

        GrpcCqrsMessageUtils.applyCqrsCommandMsgToCqrsCommand(message.getCqrsUserCommand(), command);
        command.setResponse(GrpcCqrsMessageUtils.fromMessage(message.getResponse()));

        command.setEmail(message.getEmail());
        command.setName(PrologueGrpcMessage.fromMessage(message.getName()));
        command.setEncryptedPassword(message.getEncryptedPassword());

        return command;
    }

    public static RegisterPavilionManagerCastellanCommandMsg toMessage(RegisterPavilionManagerCastellanCommand command){
        //
        RegisterPavilionManagerCastellanCommandMsg.Builder builder = RegisterPavilionManagerCastellanCommandMsg.newBuilder();
        builder.setCqrsUserCommand(GrpcCqrsMessageUtils.buildCqrsCommandMsg(command));
        if(command.getResponse()!=null){
            builder.setResponse(GrpcCqrsMessageUtils.toMessage(command.getResponse()));
        }

        builder.setEmail(command.getEmail());
        builder.setName(PrologueGrpcMessage.toMessage(command.getName()));
        builder.setEncryptedPassword(command.getEncryptedPassword());

        return builder.build();
    }


    public static RegisterNaraCommand toCommand(RegisterNaraCommandMsg message) {
        //
        RegisterNaraCommand command = new RegisterNaraCommand();

        GrpcCqrsMessageUtils.applyCqrsCommandMsgToCqrsCommand(message.getCqrsUserCommand(), command);
        command.setResponse(GrpcCqrsMessageUtils.fromMessage(message.getResponse()));

        command.setName(message.getName());
        command.setType(NaraType.valueOf(message.getType().name()));
        command.setZoneId(message.getZoneId());
        command.setBaseLanguage(message.getBaseLanguage());
        command.setSupportLanguages(message.getSupportLanguagesList());
        command.setPublicServantName(message.getPublicServantName());
        command.setPublicServantEmail(message.getPublicServantEmail());

        return command;
    }

    public static RegisterNaraCommandMsg toMessage(RegisterNaraCommand command){
        //
        RegisterNaraCommandMsg.Builder builder = RegisterNaraCommandMsg.newBuilder();
        builder.setCqrsUserCommand(GrpcCqrsMessageUtils.buildCqrsCommandMsg(command));
        if(command.getResponse()!=null){
            builder.setResponse(GrpcCqrsMessageUtils.toMessage(command.getResponse()));
        }

        builder.setName(command.getName());
        builder.setType(NaraTypeMsg.valueOf(command.getType().name()));
        builder.setZoneId(command.getZoneId());
        builder.setBaseLanguage(command.getBaseLanguage());
        command.getSupportLanguages().forEach(supportLanguage -> builder.addSupportLanguages(supportLanguage));
        builder.setPublicServantName(command.getPublicServantName());
        builder.setPublicServantEmail(command.getPublicServantEmail());

        return builder.build();
    }


    public static FindCastellanEmailByEmailQueryMsg toMessage(FindCastellanEmailByEmailQuery query){
        //
        FindCastellanEmailByEmailQueryMsg.Builder builder = FindCastellanEmailByEmailQueryMsg.newBuilder();
        builder.setCqrsQuery(GrpcCqrsMessageUtils.buildCqrsQueryMsg(query));
        if(query.getQueryResult()!=null){
            builder.setQueryResult(toMessage(query.getQueryResult()));
        }


        builder.setEmail(query.getEmail());

        return builder.build();
    }

    public static FindCastellanEmailByEmailQuery fromMessage(FindCastellanEmailByEmailQueryMsg msg){
        //
        FindCastellanEmailByEmailQuery query = new FindCastellanEmailByEmailQuery();
        GrpcCqrsMessageUtils.applyCqrsQueryMsgToCqrsQuery(msg.getCqrsQuery(), query);

        query.setEmail(msg.getEmail());
        if (msg.hasQueryResult()) {
            query.setQueryResult(fromMessage(msg.getQueryResult()));
        }

        return query;
    }

    //
    public static FindCitizenIdentityQueryMsg toMessage(FindCitizenIdentityQuery query){
        //
        FindCitizenIdentityQueryMsg.Builder builder = FindCitizenIdentityQueryMsg.newBuilder();
        builder.setCqrsQuery(GrpcCqrsMessageUtils.buildCqrsQueryMsg(query));
        if(query.getQueryResult()!=null){
            builder.setQueryResult(toMessage(query.getQueryResult()));
        }

        builder.setCitizenId(query.getCitizenId());

        return builder.build();
    }

    public static FindCitizenIdentityQuery fromMessage(FindCitizenIdentityQueryMsg msg){
        //
        FindCitizenIdentityQuery query = new FindCitizenIdentityQuery();
        GrpcCqrsMessageUtils.applyCqrsQueryMsgToCqrsQuery(msg.getCqrsQuery(), query);

        query.setCitizenId(msg.getCitizenId());
        if(msg.hasQueryResult()){
            query.setQueryResult(fromMessage(msg.getQueryResult()));
        }

        return query;
    }

    public static FindCitizenIdentityByMetroIdAndEmailQueryMsg toMessage(FindCitizenIdentityByMetroIdAndEmailQuery query){
        //
        FindCitizenIdentityByMetroIdAndEmailQueryMsg.Builder builder = FindCitizenIdentityByMetroIdAndEmailQueryMsg.newBuilder();
        builder.setCqrsQuery(GrpcCqrsMessageUtils.buildCqrsQueryMsg(query));
        if(query.getQueryResult()!=null){
            builder.setQueryResult(toMessage(query.getQueryResult()));
        }

        builder.setEmail(query.getEmail());
        builder.setMetroId(query.getMetroId());

        return builder.build();
    }

    public static FindCitizenIdentityByMetroIdAndEmailQuery fromMessage(FindCitizenIdentityByMetroIdAndEmailQueryMsg msg){
        //
        FindCitizenIdentityByMetroIdAndEmailQuery query = new FindCitizenIdentityByMetroIdAndEmailQuery();
        GrpcCqrsMessageUtils.applyCqrsQueryMsgToCqrsQuery(msg.getCqrsQuery(), query);

        query.setEmail(msg.getEmail());
        query.setMetroId(msg.getMetroId());
        if(msg.hasQueryResult()){
            query.setQueryResult(fromMessage(msg.getQueryResult()));
        }

        return query;
    }


    public static FindCitizenIdentitiesByIdsQueryMsg toMessage(FindCitizenIdentitiesByIdsQuery query){
        //
        FindCitizenIdentitiesByIdsQueryMsg.Builder builder = FindCitizenIdentitiesByIdsQueryMsg.newBuilder();
        builder.setCqrsQuery(GrpcCqrsMessageUtils.buildCqrsQueryMsg(query));

        if(query.getQueryResult()!=null){
            query.getQueryResult().forEach(citizenIdentity -> builder.addQueryResult(toMessage(citizenIdentity)));
        }

        query.getCitizenIds().forEach(citizenId -> builder.addIds(citizenId));

        return builder.build();
    }

    public static FindCitizenIdentitiesByIdsQuery fromMessage(FindCitizenIdentitiesByIdsQueryMsg msg){
        //
        FindCitizenIdentitiesByIdsQuery query = new FindCitizenIdentitiesByIdsQuery();
        GrpcCqrsMessageUtils.applyCqrsQueryMsgToCqrsQuery(msg.getCqrsQuery(), query);

        List<CitizenIdentity> citizenIdentities = new ArrayList<>();
        msg.getQueryResultList().forEach(citizenIdentityMsg -> citizenIdentities.add(fromMessage(citizenIdentityMsg)));

        query.setQueryResult(citizenIdentities);
        query.setCitizenIds(msg.getIdsList());

        return query;
    }

    public static RegisterCitizenCommand toCommand(RegisterCitizenCommandMsg message) {
        //
        RegisterCitizenCommand command = new RegisterCitizenCommand();
        GrpcCqrsMessageUtils.applyCqrsCommandMsgToCqrsCommand(message.getCqrsUserCommand(), command);
        command.setResponse(GrpcCqrsMessageUtils.fromMessage(message.getResponse()));

        command.setMetroId(message.getMetroId());
        command.setName(message.getName());
        command.setEmail(message.getEmail());
        command.setUsid(message.getUsid());
        command.setEncryptedPassword(message.getEncryptedPassword());
        command.setActivating(message.getActivating());

        return command;
    }

    public static RegisterCitizenCommandMsg toMessage(RegisterCitizenCommand command){
        //
        RegisterCitizenCommandMsg.Builder builder = RegisterCitizenCommandMsg.newBuilder();
        builder.setCqrsUserCommand(GrpcCqrsMessageUtils.buildCqrsCommandMsg(command));
        if(command.getResponse()!=null){
            builder.setResponse(GrpcCqrsMessageUtils.toMessage(command.getResponse()));
        }

        if(command.getMetroId()!=null){
            builder.setMetroId(command.getMetroId());
        }
        if(command.getEmail()!=null){
            builder.setEmail(command.getEmail());
        }
        if(command.getName()!=null){
            builder.setName(command.getName());
        }
        if(command.getUsid()!=null){
            builder.setUsid(command.getUsid());
        }
        if(command.getEncryptedPassword()!=null){
            builder.setEncryptedPassword(command.getEncryptedPassword());
        }

        builder.setActivating(command.isActivating());

        return builder.build();
    }


    public static RegisterPavilionManagerCitizenCommand toCommand(RegisterPavilionManagerCitizenCommandMsg message) {
        //
        RegisterPavilionManagerCitizenCommand command = new RegisterPavilionManagerCitizenCommand();
        GrpcCqrsMessageUtils.applyCqrsCommandMsgToCqrsCommand(message.getCqrsUserCommand(), command);
        command.setResponse(GrpcCqrsMessageUtils.fromMessage(message.getResponse()));

        command.setMetroId(message.getMetroId());
        command.setPavilionManagerEmail(message.getPavilionManagerEmail());
        command.setPavilionManagerName(message.getPavilionManagerName());
        command.setPavilionManagerEncryptedPassword(message.getPavilionManagerEncryptedPassword());

        return command;
    }

    public static RegisterPavilionManagerCitizenCommandMsg toMessage(RegisterPavilionManagerCitizenCommand command){
        //
        RegisterPavilionManagerCitizenCommandMsg.Builder builder = RegisterPavilionManagerCitizenCommandMsg.newBuilder();
        builder.setCqrsUserCommand(GrpcCqrsMessageUtils.buildCqrsCommandMsg(command));
        if(command.getResponse()!=null){
            builder.setResponse(GrpcCqrsMessageUtils.toMessage(command.getResponse()));
        }

        if(command.getMetroId()!=null){
            builder.setMetroId(command.getMetroId());
        }
        if(command.getPavilionManagerEmail()!=null){
            builder.setPavilionManagerEmail(command.getPavilionManagerEmail());
        }
        if(command.getPavilionManagerName()!=null){
            builder.setPavilionManagerName(command.getPavilionManagerName());
        }
        if(command.getPavilionManagerEncryptedPassword()!=null){
            builder.setPavilionManagerEncryptedPassword(command.getPavilionManagerEncryptedPassword());
        }

        return builder.build();
    }




    public static CastellanEmailMsg toMessage(CastellanEmail castellanEmail) {
        //
        if (castellanEmail == null) {
            return CastellanEmailMsg.newBuilder().build();
        }
        return CastellanEmailMsg.newBuilder()
                .setEmail(castellanEmail.getEmail())
                .setDisplayName(castellanEmail.getDisplayName())
                .setCastellanId(castellanEmail.getCastellanId()).build();
    }

    public static CitizenStateMsg toMessage(CitizenState state) {
        //
        return CitizenStateMsg.valueOf(state.name());
    }

    public static CitizenIdentityMsg toMessage(CitizenIdentity identity) {
        //
        if (identity == null) {
            return CitizenIdentityMsg.newBuilder().build();
        }

        return CitizenIdentityMsg.newBuilder()
                .setId(identity.getId())
                .setUsid(Optional.ofNullable(identity.getUsid()).orElse(""))
                .setDisplayName(identity.getDisplayName())
                .setEmail(identity.getEmail())
                .setState(toMessage(identity.getState()))
                .setMetroId(identity.getMetroId()).build();
    }

    public static CitizenLanguageSettingsMsg toMessage(CitizenLanguageSettings languageSettings) {
        //
        return CitizenLanguageSettingsMsg.newBuilder()
                .setBaseLanguage(languageSettings.getBaseLanguage())
                .addAllSupportLanguages(
                        Optional.ofNullable(languageSettings.getSupportLanguages()).orElse(new ArrayList<String>()))
                .setNames(PrologueGrpcMessage.toMessage(languageSettings.getNames())).build();
    }

    public static OptionalSettingsMsg toMessage(OptionalSettings optionalSettings) {
        //
        return OptionalSettingsMsg.newBuilder()
                .setProfileId(Optional.ofNullable(optionalSettings.getProfileId()).orElse(""))
                .setTinyAlbumId(Optional.ofNullable(optionalSettings.getTinyAlbumId()).orElse("")).build();
    }

    public static CitizenSettingsMsg toMessage(CitizenSettings settings) {
        //
        return CitizenSettingsMsg.newBuilder()
                .setLanguageSettings(toMessage(settings.getLanguageSettings()))
                .setOptionalSettings(toMessage(settings.getOptionalSettings()))
                .setTime(settings.getTime()).build();
    }

    public static AbstractCitizenMsg toMessage(AbstractCitizen abstractCitizen) {
        //
        return AbstractCitizenMsg.newBuilder()
                .setProfilePhoto(ByteString.copyFrom(
                        Optional.ofNullable(abstractCitizen.getProfilePhoto()).orElse(new String("").getBytes())))
                .setIdentity(toMessage(abstractCitizen.getIdentity()))
                .setSettings(toMessage(abstractCitizen.getSettings()))
                .setIdentityModified(abstractCitizen.isIdentityModified()).build();
    }


    public static CitizenState fromMessage(CitizenStateMsg stateMsg) {
        //
        return CitizenState.valueOf(stateMsg.name());
    }

    public static CastellanEmail fromMessage(CastellanEmailMsg castellanEmailMsg) {
        //
//        if(StringUtils.isEmpty(castellanEmailMsg.toString())){
//            return null;
//        }
        CastellanEmail castellanEmail = new CastellanEmail();
        castellanEmail.setEmail(castellanEmailMsg.getEmail());
        castellanEmail.setDisplayName(castellanEmailMsg.getDisplayName());
        castellanEmail.setCastellanId(castellanEmailMsg.getCastellanId());

        return castellanEmail;
    }

    public static CitizenIdentity fromMessage(CitizenIdentityMsg identityMsg) {
        //
        CitizenIdentity identity = new CitizenIdentity();
        identity.setId(identityMsg.getId());
        identity.setUsid(identityMsg.getUsid());
        identity.setDisplayName(identityMsg.getDisplayName());
        identity.setEmail(identityMsg.getEmail());
        identity.setState(fromMessage(identityMsg.getState()));
        identity.setMetroId(identityMsg.getMetroId());

        return identity;
    }

    public static CitizenLanguageSettings fromMessage(CitizenLanguageSettingsMsg languageSettingsMsg) {
        //
        CitizenLanguageSettings languageSettings = new CitizenLanguageSettings();
        languageSettings.setBaseLanguage(languageSettingsMsg.getBaseLanguage());
        languageSettings.setSupportLanguages(languageSettingsMsg.getSupportLanguagesList());
        languageSettings.setNames(PrologueGrpcMessage.fromMessage(languageSettingsMsg.getNames()));

        return languageSettings;
    }

    public static OptionalSettings fromMessage(OptionalSettingsMsg optionalSettingsMsg) {
        //
        OptionalSettings optionalSettings = new OptionalSettings();
        optionalSettings.setProfileId(optionalSettingsMsg.getProfileId());
        optionalSettings.setTinyAlbumId(optionalSettingsMsg.getTinyAlbumId());

        return optionalSettings;
    }

    public static CitizenSettings fromMessage(CitizenSettingsMsg settingsMsg) {
        //
        CitizenSettings settings = new CitizenSettings();
        settings.setLanguageSettings(fromMessage(settingsMsg.getLanguageSettings()));
        settings.setOptionalSettings(fromMessage(settingsMsg.getOptionalSettings()));
        settings.setTime(settingsMsg.getTime());

        return settings;
    }
}
