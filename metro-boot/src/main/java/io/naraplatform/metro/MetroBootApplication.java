package io.naraplatform.metro;

import io.naraplatform.daysman.client.config.EnableDaysman;
import io.naraplatform.envoy.config.NaraDramaApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@NaraDramaApplication
@SpringBootApplication
@EnableDaysman
public class MetroBootApplication {
    //
    public static void main(String[] args) {
        //
        SpringApplication.run(MetroBootApplication.class, args);
    }
}
