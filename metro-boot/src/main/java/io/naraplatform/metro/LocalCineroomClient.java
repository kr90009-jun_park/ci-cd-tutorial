package io.naraplatform.metro;

import io.naraplatform.stage.domain.aggregate.cineroom.query.FindCineroomDramaRoleMapsQuery;
import io.naraplatform.stage.domain.aggregate.cineroom.query.FindCineroomDramaRoleMapsResponse;
import io.naraplatform.stage.domain.aggregate.cineroom.query.FindDramaAssignmentsQuery;
import io.naraplatform.stage.domain.aggregate.cineroom.query.FindDramaAssignmentsResponse;
import io.naraplatform.stage.domain.aggregate.cineroom.querymodel.CineroomDramaRoleMap;
import io.naraplatform.stage.domain.aggregate.cineroom.querymodel.CineroomDramaRoleMapping;
import io.naraplatform.stage.domain.aggregate.cineroom.querymodel.DramaAssignment;
import io.naraplatform.stage.domain.facade.client.CineroomClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Slf4j
@Component
@Primary
//@Profile("local")
@Profile("!default")
public class LocalCineroomClient implements CineroomClient {
    //
    public static final String dramaId = "0002";
    public static final String dramaName = "metro";
    public static final String dramaEdition = "Standard";

    private static FindCineroomDramaRoleMapsResponse findCineroomDramaRoleMapsResponse;
    private static FindDramaAssignmentsResponse findDramaAssignmentsResponse;

    private static List<CineroomDramaRoleMap> getFindCineroomDramaRoleMapsResponse() {
        //
//        if (findCineroomDramaRoleMapsResponse != null) {
//            return findCineroomDramaRoleMapsResponse;
//        }

        List<CineroomDramaRoleMapping> roleMapppings = new ArrayList<>();
        roleMapppings.add(new CineroomDramaRoleMapping("StationManager", new HashSet<>(Arrays.asList("Admin", "StationManager"))));
        roleMapppings.add(new CineroomDramaRoleMapping("SquareManager", new HashSet<>(Arrays.asList("Admin", "SquareManager"))));
        roleMapppings.add(new CineroomDramaRoleMapping("PavilionManager", new HashSet<>(Arrays.asList("Admin", "PavilionManager"))));

        CineroomDramaRoleMap cineroomDramaRoleMap = new CineroomDramaRoleMap();
        cineroomDramaRoleMap.setRoleMappings(roleMapppings);

//        findCineroomDramaRoleMapsResponse = new FindCineroomDramaRoleMapsResponse(
//                Arrays.asList(cineroomDramaRoleMap)
//        );

        return Arrays.asList(cineroomDramaRoleMap);
    }

    private static List<DramaAssignment> getFindDramaAssignmentsResponse() {
        //
        DramaAssignment dramaAssignment = new DramaAssignment(
                "cineroomId",
                "cineroomUsid",
                dramaId,
                dramaName,
                dramaEdition,
                "kollectionVersionId");
        return Arrays.asList(dramaAssignment);
    }

    @Override
    public FindDramaAssignmentsQuery findDramaAssignments(FindDramaAssignmentsQuery findDramaAssignmentsQuery) {
        //
        String cineroomId = findDramaAssignmentsQuery.getCineroomId();
//        if (!cineroomId.startsWith("ne1-m1-")) {
//            return null;
//        }

        findDramaAssignmentsQuery.setQueryResult(getFindDramaAssignmentsResponse());
        return findDramaAssignmentsQuery;
    }

    @Override
    public FindCineroomDramaRoleMapsQuery findCineroomDramaRoleMaps(FindCineroomDramaRoleMapsQuery findCineroomDramaRoleMapsQuery) {
        //
        String cineroomId = findCineroomDramaRoleMapsQuery.getCineroomId();
        if (!cineroomId.startsWith("ne1-m1-")) {
            return findCineroomDramaRoleMapsQuery;
        }

        findCineroomDramaRoleMapsQuery.setQueryResult(getFindCineroomDramaRoleMapsResponse());

        return findCineroomDramaRoleMapsQuery;
    }

    public static void main(String[] args) {
        //

    }
}
