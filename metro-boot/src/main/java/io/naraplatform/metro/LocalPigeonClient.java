package io.naraplatform.metro;

import io.naradrama.pigeon.domain.aggregate.email.command.SendEmailCommand;
import io.naradrama.pigeon.domain.facade.client.EmailClient;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Primary
@Profile("local")
public class LocalPigeonClient implements EmailClient {
    //
    @Override
    public SendEmailCommand sendMail(SendEmailCommand sendEmailCommand, CommandIdentity commandIdentity) {
        //
        log.debug("{}", sendEmailCommand.toPrettyJson());
        return sendEmailCommand;
    }
}
