FROM java:8
#FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY metro-boot/build/libs/metro-boot-0.9.0-SNAPSHOT.jar app.jar
RUN bash -c 'touch /app.jar'
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-Xms256m", "-Xmx256m", "-Dspring.profiles.active=k8s", "-jar","/app.jar"]
ENTRYPOINT ["java",  "-Duser.timezone='Asia/Seoul'", "-Dspring.profiles.active=k8s", "-Xmx512m", "-jar","/app.jar"]
