package io.naraplatform.metro.store.domain.aggregate.citizen;

import io.naradrama.prologue.domain.Offset;
import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;

import java.util.List;

public interface CitizenStore {
    //
    void create(Citizen citizen);
    Citizen retrieve(String citizenId);
    void update(Citizen citizen);
    void delete(String citizenId);



    void create(CitizenIdentity citizenIdentity);
    CitizenIdentity retrieveCitizenIdentity(String citizenId);
    CitizenIdentity retrieveCitizenIdentityByMetroIdAndEmail(String metroId, String email);
    List<CitizenIdentity> retrieveCitizenIdentityByMetroId(String metroId);
    List<CitizenIdentity> retrieveCitizenIdentityByMetroIdAndState(String metroId, CitizenState state);
    List<CitizenIdentity> retrieveCitizenIdentityByIdIn(List<String> citizenIds);
    List<CitizenIdentity> retrieveCitizenIdentityByMetroId(String metroId, Offset offset);
    List<CitizenIdentity> retrieveCitizenIdentityByfMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
            String metroId, String displayLike, String memtoId2, String emailLike, Offset offset);
    List<CitizenIdentity> retrieveCitizenIdentityByMetroIdAndState(String metroId, CitizenState state, Offset offset);
    List<CitizenIdentity> retrieveCitizenIdentityByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
            String metroId, CitizenState state, String displayLike, String memtoId2,
            CitizenState state2, String emailLike, Offset offset);

    int countCitizenIdentityByMetroId(String metroId);
    int countCitizenIdentityByMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
            String metroId, String displayLike, String metroId2, String emailLike);
    int countCitizenIdentityByMetroIdAndState(String metroId, CitizenState state);
    int countCitizenIdentityByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
            String metroId, CitizenState state, String displayLike, String metroId2, CitizenState state2, String emailLike);

    void update(CitizenIdentity citizenIdentity);
}
