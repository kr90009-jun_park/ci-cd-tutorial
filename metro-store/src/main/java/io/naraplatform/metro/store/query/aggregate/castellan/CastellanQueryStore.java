package io.naraplatform.metro.store.query.aggregate.castellan;

import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanName;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanPhone;

import java.util.List;

public interface CastellanQueryStore {
    //

    void create(CastellanEmail castellanEmail);
    CastellanEmail retrieveCastellanEmailByEmail(String email);
    boolean castellanEmailExistsByEmail(String email);
    void update(CastellanEmail castellanEmail);
    void deleteCastellanEmailByCastellanId(String castellanId);

    void create(CastellanName castellanName);
    List<CastellanName> retrieveCastellanNameByDisplayName(String displayName);
    void update(CastellanName castellanName);
    void deleteCastellanNameByCastellanId(String castellanId);

    void create(CastellanPhone castellanPhone);
    void update(CastellanPhone castellanPhone);
}
