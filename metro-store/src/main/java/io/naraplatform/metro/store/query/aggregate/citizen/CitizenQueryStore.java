package io.naraplatform.metro.store.query.aggregate.citizen;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.AbstractCitizen;

import java.util.List;

public interface CitizenQueryStore {
    //
    void create(AbstractCitizen citizen);
    AbstractCitizen retrieve(String citizenId);
    AbstractCitizen retrieve(String citizenId, CitizenState state);

    void update(AbstractCitizen citizen, boolean identityModified);
    void delete(String citizenId, CitizenState state);
    void deleteAllByCitizenId(String citizenId);

    List<AbstractCitizen> retrieveByMetroIdAndState(String metroId, CitizenState state);
}
