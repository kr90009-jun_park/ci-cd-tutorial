package io.naraplatform.metro.store.domain.aggregate.metro;

import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;

import java.util.List;

public interface MetroStore {
    //
    void create(Metro metro);
    Metro retrieveByMetroId(String metroId);
    List<Metro> retrieveAll();
    void update(Metro metro);
    void deleteByMetroId(String metroId);
}
