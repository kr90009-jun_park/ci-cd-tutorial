package io.naraplatform.metro.store.domain.aggregate.castellan;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;

import java.util.List;

public interface CastellanStore {
    //
    void create(Castellan castellan);
    Castellan retrieve(String id);
    List<Castellan> retrieveAll();
    void update(Castellan castellan);
    void delete(String id);
}
