package io.naraplatform.metro.store.domain.aggregate.nara;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naraplatform.metro.domain.aggregate.nara.entity.NaraIdentity;

import java.util.List;

public interface NaraStore {
    //
    void create(Nara nara);
    Nara retrieve(String naraId);
    List<Nara> retrieveAll();
    void update(Nara nara);
    void delete(String naraId);

    void create(NaraIdentity naraIdentity);
    NaraIdentity retrieveNaraIdentity(String naraId);
    void update(NaraIdentity naraIdentity);
}
