package io.naraplatform.metro.store.mongo.domain.aggregate.castellan.doc;

import io.naradrama.prologue.domain.IdNameList;
import io.naradrama.prologue.domain.granule.EmailList;
import io.naradrama.prologue.domain.granule.PhoneList;
import io.naradrama.prologue.domain.lang.LangStrings;
import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.metro.domain.aggregate.castellan.entity.JoinedMetroList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("castellan")
public class CastellanDoc extends DomainEntityDoc {
    //
    private EmailList emails;
    private LangStrings names;
    private PhoneList phones;
    private JoinedMetroList joinedMetros;       // participant metros
    private IdNameList usids;                   // Employee Number, Member Id, etc. ex. "19092:NEXTREE"
    private long time;

    public CastellanDoc(Castellan castellan) {
        //
        super(castellan);
        BeanUtils.copyProperties(castellan, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public Castellan toDomain() {
        //
        Castellan castellan = new Castellan(getId());
        BeanUtils.copyProperties(this, castellan);

        return castellan;
    }

    public static List<Castellan> toDomains(List<CastellanDoc> castellanDocs) {
        //
        return castellanDocs
                .stream()
                .map(CastellanDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static CastellanDoc sample() {
        //
        return new CastellanDoc(Castellan.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(Castellan.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
