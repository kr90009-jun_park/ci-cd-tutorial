package io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc;

import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanPhone;
import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("castellanPhone")
public class CastellanPhoneDoc extends DomainEntityDoc {
    //
    private String phone;           // index and query key, just phone number like 01091170989
    private String displayName;
    private String castellanId;

    public CastellanPhoneDoc(CastellanPhone castellanPhone) {
        //
        super(castellanPhone);
        BeanUtils.copyProperties(castellanPhone, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public CastellanPhone toDomain() {
        //
        CastellanPhone castellanPhone = new CastellanPhone(getId());
        BeanUtils.copyProperties(this, castellanPhone);

        return castellanPhone;
    }

    public static List<CastellanPhone> toDomains(List<CastellanPhoneDoc> castellanPhoneDocs) {
        //
        return castellanPhoneDocs
                .stream()
                .map(CastellanPhoneDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static CastellanPhoneDoc sample() {
        //
        return new CastellanPhoneDoc(CastellanPhone.samples().get(0));
    }

    public static void main(String[] args) {
        //
        System.out.println(CastellanPhone.samples().get(0).toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
