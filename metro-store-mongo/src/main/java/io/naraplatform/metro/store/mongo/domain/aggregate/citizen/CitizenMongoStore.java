package io.naraplatform.metro.store.mongo.domain.aggregate.citizen;

import io.naradrama.prologue.domain.Offset;
import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.metro.store.domain.aggregate.citizen.CitizenStore;
import io.naraplatform.metro.store.mongo.domain.aggregate.citizen.doc.CitizenDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.citizen.doc.CitizenIdentityDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.citizen.repository.CitizenIdentityRepository;
import io.naraplatform.metro.store.mongo.domain.aggregate.citizen.repository.CitizenRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CitizenMongoStore implements CitizenStore {
    //
    private final CitizenRepository citizenRepository;
    private final CitizenIdentityRepository citizenIdentityRepository;

    public CitizenMongoStore(CitizenRepository citizenRepository,
                             CitizenIdentityRepository citizenIdentityRepository) {
        //
        this.citizenRepository = citizenRepository;
        this.citizenIdentityRepository = citizenIdentityRepository;
    }

    @Override
    public void create(Citizen citizen) {
        //
        CitizenDoc citizenDoc = new CitizenDoc(citizen);
        citizenRepository.save(citizenDoc);
    }

    @Override
    public Citizen retrieve(String citizenId) {
        Optional<CitizenDoc> citizenDocOptional = citizenRepository.findById(citizenId);
        return citizenDocOptional.map(CitizenDoc::toDomain).orElse(null);
    }

    @Override
    public void update(Citizen citizen) {
        //
        CitizenDoc citizenDoc = new CitizenDoc(citizen);
        citizenRepository.save(citizenDoc);
    }

    @Override
    public void delete(String citizenId) {
        //
        Optional<CitizenDoc> citizenDocOptional = citizenRepository.findById(citizenId);
        if (citizenDocOptional.isPresent()) {
            Citizen citizen = citizenDocOptional.map(CitizenDoc::toDomain).orElse(null);
            citizenRepository.deleteById(citizen.getId());
        }
    }


    @Override
    public void create(CitizenIdentity identity) {
        //
        CitizenIdentityDoc identityDoc = new CitizenIdentityDoc(identity);
        citizenIdentityRepository.save(identityDoc);
    }

    @Override
    public CitizenIdentity retrieveCitizenIdentity(String citizenId) {
        //
        Optional<CitizenIdentityDoc> identityDocOptional = citizenIdentityRepository.findById(citizenId);
        return identityDocOptional.map(CitizenIdentityDoc::toDomain).orElse(null);
    }

    @Override
    public CitizenIdentity retrieveCitizenIdentityByMetroIdAndEmail(String metroId, String email) {
        //
        Optional<CitizenIdentityDoc> identityDocOptional
                = citizenIdentityRepository.findByMetroIdAndEmail(metroId, email);
        return identityDocOptional.map(CitizenIdentityDoc::toDomain).orElse(null);
    }

    @Override
    public List<CitizenIdentity> retrieveCitizenIdentityByMetroId(String metroId) {
        //
        List<CitizenIdentityDoc> identityDocs = citizenIdentityRepository.findByMetroId(metroId);
        return CitizenIdentityDoc.toDomains(identityDocs);
    }

    @Override
    public List<CitizenIdentity> retrieveCitizenIdentityByMetroIdAndState(String metroId, CitizenState state) {
        //
        List<CitizenIdentityDoc> identityDocs = citizenIdentityRepository.findByMetroIdAndState(metroId, state);
        return CitizenIdentityDoc.toDomains(identityDocs);
    }

    @Override
    public List<CitizenIdentity> retrieveCitizenIdentityByIdIn(List<String> citizenIds) {
        //
        List<CitizenIdentityDoc> identityDocs = citizenIdentityRepository.findByIdIn(citizenIds);
        return CitizenIdentityDoc.toDomains(identityDocs);
    }

    @Override
    public List<CitizenIdentity> retrieveCitizenIdentityByMetroId(String metroId, Offset offset) {
        //
        List<CitizenIdentityDoc> identityDocs = citizenIdentityRepository.findByMetroId(
                metroId, PageRequest.of(offset.offset(), offset.limit()));
        return CitizenIdentityDoc.toDomains(identityDocs);
    }

    @Override
    public List<CitizenIdentity> retrieveCitizenIdentityByfMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
            String metroId, String displayLike, String metroId2, String emailLike, Offset offset) {
        //
        List<CitizenIdentityDoc> identityDocs
                = citizenIdentityRepository.findByMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
                metroId, displayLike, metroId2, emailLike, PageRequest.of(offset.offset(), offset.limit()));
        return CitizenIdentityDoc.toDomains(identityDocs);
    }

    @Override
    public List<CitizenIdentity> retrieveCitizenIdentityByMetroIdAndState(String metroId, CitizenState state, Offset offset) {
        //
        List<CitizenIdentityDoc> identityDocs
                = citizenIdentityRepository.findByMetroIdAndState(
                metroId, state, PageRequest.of(offset.offset(), offset.limit()));
        return CitizenIdentityDoc.toDomains(identityDocs);
    }

    @Override
    public List<CitizenIdentity> retrieveCitizenIdentityByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
            String metroId, CitizenState state, String displayLike,
            String metroId2, CitizenState state2, String emailLike, Offset offset) {
        //
        List<CitizenIdentityDoc> identityDocs
                = citizenIdentityRepository.findByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
                metroId, state, displayLike, metroId2, state2, emailLike,
                PageRequest.of(offset.offset(), offset.limit()));
        return CitizenIdentityDoc.toDomains(identityDocs);
    }

    @Override
    public int countCitizenIdentityByMetroId(String metroId) {
        //
        return citizenIdentityRepository.countByMetroId(metroId);
    }

    @Override
    public int countCitizenIdentityByMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
            String metroId, String displayLike, String metroId2, String emailLike) {
        return citizenIdentityRepository.countByMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
                metroId, displayLike, metroId2, emailLike);
    }

    @Override
    public int countCitizenIdentityByMetroIdAndState(String metroId, CitizenState state) {
        return citizenIdentityRepository.countByMetroIdAndState(metroId, state);
    }

    @Override
    public int countCitizenIdentityByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
            String metroId, CitizenState state, String displayLike,
            String metroId2, CitizenState state2, String emailLike) {
        return citizenIdentityRepository.countByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
                metroId, state, displayLike, metroId2, state2, emailLike);
    }

    @Override
    public void update(CitizenIdentity identity) {
        //
        CitizenIdentityDoc identityDoc = new CitizenIdentityDoc(identity);
        citizenIdentityRepository.save(identityDoc);
    }
}
