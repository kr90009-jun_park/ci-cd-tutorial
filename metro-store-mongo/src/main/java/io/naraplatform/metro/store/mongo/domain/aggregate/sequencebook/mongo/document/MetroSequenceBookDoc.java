package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document;

import io.naradrama.prologue.util.json.JsonSerializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "metro_sequence_book")
@TypeAlias("MetroSequenceBookDoc")
public class MetroSequenceBookDoc implements JsonSerializable {
    //
    @Id
    private String naraId;

    private int sequence;

    public MetroSequenceBookDoc(String naraId) {
        //
        this.naraId = naraId;
    }

    public int nextSequence() {
        //
        return ++sequence;
    }
}
