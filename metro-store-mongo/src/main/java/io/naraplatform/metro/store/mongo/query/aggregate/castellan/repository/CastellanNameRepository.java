package io.naraplatform.metro.store.mongo.query.aggregate.castellan.repository;

import io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc.CastellanNameDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CastellanNameRepository extends MongoRepository<CastellanNameDoc, String> {
    //
    List<CastellanNameDoc> findByDisplayName(String displayName);
    void deleteByCastellanId(String castellanId);
}
