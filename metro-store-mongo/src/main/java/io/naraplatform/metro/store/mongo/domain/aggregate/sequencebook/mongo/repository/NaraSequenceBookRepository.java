package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.repository;

import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document.NaraSequenceBookDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NaraSequenceBookRepository extends MongoRepository<NaraSequenceBookDoc, String> {
    //
}
