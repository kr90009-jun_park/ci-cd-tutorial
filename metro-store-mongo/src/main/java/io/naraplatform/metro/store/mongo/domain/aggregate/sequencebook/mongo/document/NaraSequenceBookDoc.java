package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document;

import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.NaraSequenceBook;
import io.naradrama.prologue.util.json.JsonSerializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "nara_sequence_book")
@TypeAlias("NaraSequenceBookDoc")
public class NaraSequenceBookDoc implements JsonSerializable {
    //
    @Id
    private String id;

    private int sequence;

    public static NaraSequenceBookDoc newOne() {
        //
        NaraSequenceBookDoc naraSequenceBookDoc = new NaraSequenceBookDoc();
        naraSequenceBookDoc.setId(NaraSequenceBook.SEQUENCE_BOOK_ID);

        return naraSequenceBookDoc;
    }

    public int nextSequence() {
        //
        return ++sequence;
    }
}
