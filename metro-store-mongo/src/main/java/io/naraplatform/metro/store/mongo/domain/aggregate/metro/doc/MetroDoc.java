package io.naraplatform.metro.store.mongo.domain.aggregate.metro.doc;

import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naraplatform.metro.domain.aggregate.metro.entity.MetroState;
import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("metro")
public class MetroDoc extends DomainEntityDoc {
    //
    private MetroState state;
//    private MetroOfficials officials;

    public MetroDoc(Metro metro) {
        //
        super(metro);
        BeanUtils.copyProperties(metro, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public Metro toDomain() {
        //
        Metro metro = new Metro(getId());
        BeanUtils.copyProperties(this, metro);

        return metro;
    }

    public static List<Metro> toDomains(List<MetroDoc> metroDocs) {
        //
        return metroDocs
                .stream()
                .map(MetroDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static MetroDoc sample() {
        //
        return new MetroDoc(Metro.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(Metro.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
