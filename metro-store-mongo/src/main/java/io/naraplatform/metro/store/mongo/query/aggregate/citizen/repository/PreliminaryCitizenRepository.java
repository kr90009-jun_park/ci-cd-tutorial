package io.naraplatform.metro.store.mongo.query.aggregate.citizen.repository;

import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.ActiveCitizenDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.PreliminaryCitizenDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PreliminaryCitizenRepository extends MongoRepository<PreliminaryCitizenDoc, String> {
    //
    List<ActiveCitizenDoc> findByIdentityMetroId(String metroId);
}
