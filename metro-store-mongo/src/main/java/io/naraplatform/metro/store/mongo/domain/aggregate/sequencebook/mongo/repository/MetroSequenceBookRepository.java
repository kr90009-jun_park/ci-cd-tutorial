package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.repository;

import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document.MetroSequenceBookDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MetroSequenceBookRepository extends MongoRepository<MetroSequenceBookDoc, String> {
    //
}
