package io.naraplatform.metro.store.mongo.domain.aggregate.castellan;

import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.metro.store.domain.aggregate.castellan.CastellanStore;
import io.naraplatform.metro.store.mongo.domain.aggregate.castellan.doc.CastellanDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.castellan.repository.CastellanRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CastellanMongoStore implements CastellanStore {
    //
    private final CastellanRepository castellanRepository;

    public CastellanMongoStore(CastellanRepository castellanRepository) {
        //
        this.castellanRepository = castellanRepository;
    }

    @Override
    public void create(Castellan castellan) {
        //
        CastellanDoc castellanDoc = new CastellanDoc(castellan);
        castellanRepository.save(castellanDoc);
    }

    @Override
    public Castellan retrieve(String id) {
        //
        Optional<CastellanDoc> castellanDocOptional = castellanRepository.findById(id);
        return castellanDocOptional.map(CastellanDoc::toDomain).orElse(null);
    }

    @Override
    public List<Castellan> retrieveAll() {
        //
        List<CastellanDoc> castellanDocs = castellanRepository.findAll();
        return CastellanDoc.toDomains(castellanDocs);
    }

    @Override
    public void update(Castellan castellan) {
        //
        CastellanDoc castellanDoc = new CastellanDoc(castellan);
        castellanRepository.save(castellanDoc);
    }

    @Override
    public void delete(String id) {
        //
        castellanRepository.deleteById(id);
    }
}
