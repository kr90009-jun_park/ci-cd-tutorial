package io.naraplatform.metro.store.mongo.query.aggregate.citizen;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.*;
import io.naraplatform.metro.store.mongo.domain.aggregate.citizen.doc.CitizenIdentityDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.citizen.repository.CitizenIdentityRepository;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.ActiveCitizenDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.DormantCitizenDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.PreliminaryCitizenDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.RemovedCitizenDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.repository.ActiveCitizenRepository;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.repository.DormantCitizenRepository;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.repository.PreliminaryCitizenRepository;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.repository.RemovedCitizenRepository;
import io.naraplatform.metro.store.query.aggregate.citizen.CitizenQueryStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
//@EnableMongoRepositories(basePackages = "io.naraplatform.metro.store.mongo.domain")
public class CitizenMongoQueryStore implements CitizenQueryStore {
    //
    private final CitizenIdentityRepository citizenIdentityRepository;
    private final PreliminaryCitizenRepository preliminaryCitizenRepository;
    private final ActiveCitizenRepository activeCitizenRepository;
    private final DormantCitizenRepository dormantCitizenRepository;
    private final RemovedCitizenRepository removedCitizenRepository;

    public CitizenMongoQueryStore(CitizenIdentityRepository citizenIdentityRepository,
                                  PreliminaryCitizenRepository preliminaryCitizenRepository,
                                  ActiveCitizenRepository activeCitizenRepository,
                                  DormantCitizenRepository dormantCitizenRepository,
                                  RemovedCitizenRepository removedCitizenRepository) {
        //
        this.citizenIdentityRepository = citizenIdentityRepository;
        this.preliminaryCitizenRepository = preliminaryCitizenRepository;
        this.activeCitizenRepository = activeCitizenRepository;
        this.dormantCitizenRepository = dormantCitizenRepository;
        this.removedCitizenRepository = removedCitizenRepository;
    }

    @Override
    public void create(AbstractCitizen citizen) {
        //
        save(citizen, false);
    }

    @Override
    public AbstractCitizen retrieve(String citizenId) {
        //
        AbstractCitizen citizen = null;
        try {
            Optional<CitizenIdentityDoc> optionalIdentityDoc = citizenIdentityRepository.findById(citizenId);
            CitizenIdentity identity = optionalIdentityDoc.map(CitizenIdentityDoc::toDomain).orElse(null);

            citizen = retrieve(citizenId, identity.getState());
            citizen.setIdentity(identity);
        }
        catch (Exception exc) {
            log.error("exception in findCitizen. citizenId {}", citizenId);
            throw exc;
        }
        return citizen;
    }

    @Override
    public AbstractCitizen retrieve(String citizenId, CitizenState state) {
        //
        AbstractCitizen citizen = null;
        switch (state) {
            case Active:
                Optional<ActiveCitizenDoc> optionalActiveCitizenDoc = activeCitizenRepository.findById(citizenId);
                citizen = optionalActiveCitizenDoc.map(ActiveCitizenDoc::toDomain).orElse(null);
                break;
            case Preliminary:
                Optional<PreliminaryCitizenDoc> optionalPreliminaryCitizenDoc = preliminaryCitizenRepository.findById(citizenId);
                citizen = optionalPreliminaryCitizenDoc.map(PreliminaryCitizenDoc::toDomain).orElse(null);
                break;
            case Dormant:
                Optional<DormantCitizenDoc> optionalDormantCitizenDoc = dormantCitizenRepository.findById(citizenId);
                citizen = optionalDormantCitizenDoc.map(DormantCitizenDoc::toDomain).orElse(null);
                break;
            case Removed:
                Optional<RemovedCitizenDoc> optionalRemovedCitizenDoc = removedCitizenRepository.findById(citizenId);
                citizen = optionalRemovedCitizenDoc.map(RemovedCitizenDoc::toDomain).orElse(null);
                break;
        }

        return citizen;
    }

    @Override
    public void update(AbstractCitizen citizen, boolean identityModified) {
        //
        save(citizen, identityModified);
    }


    @Override
    public void delete(String citizenId, CitizenState state) {
        //
        switch (state) {
            case Active:
                activeCitizenRepository.deleteById(citizenId);
                break;
            case Preliminary:
                preliminaryCitizenRepository.deleteById(citizenId);
                break;
            case Dormant:
                dormantCitizenRepository.deleteById(citizenId);
                break;
            case Removed:
                removedCitizenRepository.deleteById(citizenId);
                break;
        }
    }


    @Override
    public void deleteAllByCitizenId(String citizenId) {
        //
        activeCitizenRepository.deleteById(citizenId);
        dormantCitizenRepository.deleteById(citizenId);
        preliminaryCitizenRepository.deleteById(citizenId);
        removedCitizenRepository.deleteById(citizenId);

        citizenIdentityRepository.deleteById(citizenId);
    }

    @Override
    public List<AbstractCitizen> retrieveByMetroIdAndState(String metroId, CitizenState state) {
        //
        List<AbstractCitizen> citizens = new ArrayList<>();

        switch (state) {
            case Active:
                citizens.addAll(ActiveCitizenDoc.toDomains(activeCitizenRepository.findByIdentityMetroId(metroId)));
                break;
            case Preliminary:
                citizens.addAll(ActiveCitizenDoc.toDomains(preliminaryCitizenRepository.findByIdentityMetroId(metroId)));
                break;
            case Dormant:
                citizens.addAll(ActiveCitizenDoc.toDomains(dormantCitizenRepository.findByIdentityMetroId(metroId)));
                break;
            case Removed:
                citizens.addAll(ActiveCitizenDoc.toDomains(removedCitizenRepository.findByIdentityMetroId(metroId)));
                break;
        }

        return citizens;
    }

    public void save(AbstractCitizen citizen, boolean identityModified) {
        //
        switch (citizen.getIdentity().getState()) {
            case Active:
                save((ActiveCitizen) citizen, identityModified);
                break;
            case Preliminary:
                save((PreliminaryCitizen) citizen, identityModified);
                break;
            case Dormant:
                save((DormantCitizen) citizen, identityModified);
                break;
            case Removed:
                save((RemovedCitizen) citizen, identityModified);
                break;
        }
    }

    private void save(PreliminaryCitizen preliminaryCitizen, boolean identityModified) {
        //
        PreliminaryCitizenDoc citizenDoc = new PreliminaryCitizenDoc(preliminaryCitizen);
        preliminaryCitizenRepository.save(citizenDoc);
        saveIdentityIfModified(preliminaryCitizen, identityModified);
    }

    private void save(ActiveCitizen activeCitizen, boolean identityModified) {
        //
        ActiveCitizenDoc citizenDoc = new ActiveCitizenDoc(activeCitizen);
        activeCitizenRepository.save(citizenDoc);
        saveIdentityIfModified(activeCitizen, identityModified);
    }

    private void save(DormantCitizen dormantCitizen, boolean identityModified) {
        //
        DormantCitizenDoc citizenDoc = new DormantCitizenDoc(dormantCitizen);
        dormantCitizenRepository.save(citizenDoc);
        saveIdentityIfModified(dormantCitizen, identityModified);
    }

    private void save(RemovedCitizen removedCitizen, boolean identityModified) {
        //
        RemovedCitizenDoc citizenDoc = new RemovedCitizenDoc(removedCitizen);
        removedCitizenRepository.save(citizenDoc);
        saveIdentityIfModified(removedCitizen, identityModified);
    }

    private void saveIdentityIfModified(AbstractCitizen abstractCitizen, boolean identityModified) {
        //
        if (identityModified) {
            CitizenIdentityDoc identityDoc = new CitizenIdentityDoc(abstractCitizen.getIdentity());
            citizenIdentityRepository.save(identityDoc);
        }
    }
}
