package io.naraplatform.metro.store.mongo.domain.aggregate.metro;

import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naraplatform.metro.store.domain.aggregate.metro.MetroStore;
import io.naraplatform.metro.store.mongo.domain.aggregate.metro.doc.MetroDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.metro.repository.MetroRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MetroMongoStore implements MetroStore {
    //
    private final MetroRepository metroRepository;

    public MetroMongoStore(MetroRepository metroRepository) {
        //
        this.metroRepository = metroRepository;
    }

    @Override
    public void create(Metro metro) {
        //
        MetroDoc metroDoc = new MetroDoc(metro);
        metroRepository.save(metroDoc);
    }

    @Override
    public Metro retrieveByMetroId(String metroId) {
        Optional<MetroDoc> metroDocOptional = metroRepository.findById(metroId);
        return metroDocOptional.map(MetroDoc::toDomain).orElse(null);
    }

    @Override
    public List<Metro> retrieveAll() {
        //
        List<MetroDoc> metroDocs = metroRepository.findAll();
        return MetroDoc.toDomains(metroDocs);
    }

    @Override
    public void update(Metro metro) {
        //
        MetroDoc metroDoc = new MetroDoc(metro);
        metroRepository.save(metroDoc);
    }

    @Override
    public void deleteByMetroId(String metroId) {
        //
        metroRepository.deleteById(metroId);
    }
}
