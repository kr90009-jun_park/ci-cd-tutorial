package io.naraplatform.metro.store.mongo.domain.aggregate.nara.repository;

import io.naraplatform.metro.store.mongo.domain.aggregate.nara.doc.NaraIdentityDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NaraIdentityRepository extends MongoRepository<NaraIdentityDoc, String> {
    //
}
