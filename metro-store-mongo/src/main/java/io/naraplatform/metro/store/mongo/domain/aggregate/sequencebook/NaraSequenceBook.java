package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook;

public interface NaraSequenceBook {
    //
    String SEQUENCE_BOOK_ID = "NaraSequenceBook";
    int nextSequence();
}
