package io.naraplatform.metro.store.mongo.query.aggregate.citizen.repository;

import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.ActiveCitizenDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ActiveCitizenRepository extends MongoRepository<ActiveCitizenDoc, String> {
    //
    List<ActiveCitizenDoc> findByIdentityMetroId(String metroId);

}
