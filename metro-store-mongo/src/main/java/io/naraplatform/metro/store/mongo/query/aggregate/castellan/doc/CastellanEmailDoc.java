package io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc;

import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("castellanEmail")
public class CastellanEmailDoc extends DomainEntityDoc {
    //
    private String email;           // index and query key
    private String displayName;
    private String castellanId;     // key for Castellan

    public CastellanEmailDoc(CastellanEmail castellanEmail) {
        //
        super(castellanEmail);
        BeanUtils.copyProperties(castellanEmail, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public CastellanEmail toDomain() {
        //
        CastellanEmail castellanEmail = new CastellanEmail(getId());
        BeanUtils.copyProperties(this, castellanEmail);

        return castellanEmail;
    }

    public static List<CastellanEmail> toDomains(List<CastellanEmailDoc> castellanEmailDocs) {
        //
        return castellanEmailDocs
                .stream()
                .map(CastellanEmailDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static CastellanEmailDoc sample() {
        //
        return new CastellanEmailDoc(CastellanEmail.samples().get(0));
    }

    public static void main(String[] args) {
        //
        System.out.println(CastellanEmail.samples().get(0).toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
