package io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc;

import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.RemovedCitizen;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("removedCitizen")
public class RemovedCitizenDoc extends DomainEntityDoc {
    //
    private CitizenIdentity identity;
    private CitizenSettings settings;
    private boolean identityModified;
    private byte[] profilePhoto;

    public RemovedCitizenDoc(RemovedCitizen removedCitizen) {
        //
        super(removedCitizen);
        BeanUtils.copyProperties(removedCitizen, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public RemovedCitizen toDomain() {
        //
        RemovedCitizen removedCitizen
                = new RemovedCitizen(getId());
        BeanUtils.copyProperties(this, removedCitizen);

        return removedCitizen;
    }

    public static List<RemovedCitizen> toDomains(List<RemovedCitizenDoc> removedCitizenDocs) {
        //
        return removedCitizenDocs
                .stream()
                .map(RemovedCitizenDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static RemovedCitizenDoc sample() {
        //
        return new RemovedCitizenDoc(RemovedCitizen.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(RemovedCitizen.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
