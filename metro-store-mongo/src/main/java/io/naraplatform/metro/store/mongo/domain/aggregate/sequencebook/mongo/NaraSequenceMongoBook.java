package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo;

import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.NaraSequenceBook;
import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document.NaraSequenceBookDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.repository.NaraSequenceBookRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class NaraSequenceMongoBook implements NaraSequenceBook {
    //
    private final NaraSequenceBookRepository naraSequenceBookRepository;

    public NaraSequenceMongoBook(NaraSequenceBookRepository naraSequenceBookRepository) {
        //
        this.naraSequenceBookRepository = naraSequenceBookRepository;
    }

    @Override
    public int nextSequence() {
        //
        Optional<NaraSequenceBookDoc> optionalNaraSequenceBookDoc = naraSequenceBookRepository.findById(NaraSequenceBook.SEQUENCE_BOOK_ID);
        NaraSequenceBookDoc sequenceBookDoc = null;
        if (optionalNaraSequenceBookDoc.isPresent()) {
            sequenceBookDoc = optionalNaraSequenceBookDoc.get();
        }
        else {
            sequenceBookDoc = NaraSequenceBookDoc.newOne();
        }

        int sequence = sequenceBookDoc.nextSequence();
        naraSequenceBookRepository.save(sequenceBookDoc);

        return sequence;
    }
}
