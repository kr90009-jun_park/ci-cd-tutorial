package io.naraplatform.metro.store.mongo.domain.aggregate.citizen.repository;

import io.naraplatform.metro.store.mongo.domain.aggregate.citizen.doc.CitizenDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CitizenRepository extends MongoRepository<CitizenDoc, String> {
    //
}
