package io.naraplatform.metro.store.mongo.domain.aggregate.citizen.doc;

import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("citizenIdentity")
public class CitizenIdentityDoc extends DomainEntityDoc {
    //
    private String usid;                // nullable
    private String displayName;
    private String email;
    private CitizenState state;         // mutable
    private String metroId;
    private String castellanId;

    public CitizenIdentityDoc(CitizenIdentity citizenIdentity) {
        //
        super(citizenIdentity);
        BeanUtils.copyProperties(citizenIdentity, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public CitizenIdentity toDomain() {
        //
        CitizenIdentity citizenIdentity = new CitizenIdentity(getId());
        BeanUtils.copyProperties(this, citizenIdentity);

        return citizenIdentity;
    }

    public static List<CitizenIdentity> toDomains(List<CitizenIdentityDoc> citizenIdentityDocs) {
        //
        return citizenIdentityDocs
                .stream()
                .map(CitizenIdentityDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static CitizenIdentityDoc sample() {
        //
        return new CitizenIdentityDoc(CitizenIdentity.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(CitizenIdentity.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
