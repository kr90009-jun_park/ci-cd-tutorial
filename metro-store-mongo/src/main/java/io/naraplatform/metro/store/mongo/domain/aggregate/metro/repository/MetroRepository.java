package io.naraplatform.metro.store.mongo.domain.aggregate.metro.repository;

import io.naraplatform.metro.store.mongo.domain.aggregate.metro.doc.MetroDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MetroRepository extends MongoRepository<MetroDoc, String> {
    //
}
