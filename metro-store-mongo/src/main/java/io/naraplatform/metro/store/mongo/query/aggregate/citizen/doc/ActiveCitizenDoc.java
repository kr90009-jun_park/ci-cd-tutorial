package io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc;

import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.ActiveCitizen;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("activeCitizen")
public class ActiveCitizenDoc extends DomainEntityDoc {
    //
    private CitizenIdentity identity;
    private CitizenSettings settings;
    private boolean identityModified;
    private byte[] profilePhoto;

    public ActiveCitizenDoc(ActiveCitizen activeCitizen) {
        //
        super(activeCitizen);
        BeanUtils.copyProperties(activeCitizen, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public ActiveCitizen toDomain() {
        //
        ActiveCitizen activeCitizen
                = new ActiveCitizen(getId());
        BeanUtils.copyProperties(this, activeCitizen);

        return activeCitizen;
    }

    public static List<ActiveCitizen> toDomains(List<ActiveCitizenDoc> activeCitizenDocs) {
        //
        return activeCitizenDocs
                .stream()
                .map(ActiveCitizenDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static ActiveCitizenDoc sample() {
        //
        return new ActiveCitizenDoc(ActiveCitizen.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(ActiveCitizen.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
