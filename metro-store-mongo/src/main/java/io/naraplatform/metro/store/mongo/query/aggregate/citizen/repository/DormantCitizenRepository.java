package io.naraplatform.metro.store.mongo.query.aggregate.citizen.repository;

import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.ActiveCitizenDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc.DormantCitizenDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface DormantCitizenRepository extends MongoRepository<DormantCitizenDoc, String> {
    //
    List<ActiveCitizenDoc> findByIdentityMetroId(String metroId);
}
