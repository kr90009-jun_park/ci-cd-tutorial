package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.repository;

import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document.CitizenSequenceBookDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CitizenSequenceBookRepository extends MongoRepository<CitizenSequenceBookDoc, String> {
    //
}
