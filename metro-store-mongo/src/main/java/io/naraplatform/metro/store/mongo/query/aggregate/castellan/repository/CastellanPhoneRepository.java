package io.naraplatform.metro.store.mongo.query.aggregate.castellan.repository;

import io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc.CastellanPhoneDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CastellanPhoneRepository extends MongoRepository<CastellanPhoneDoc, String> {
    //
    Optional<CastellanPhoneDoc> findByPhone(String phone);
    void deleteByCastellanId(String castellanId);
}
