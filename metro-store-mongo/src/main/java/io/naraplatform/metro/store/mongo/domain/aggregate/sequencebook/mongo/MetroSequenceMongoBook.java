package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo;

import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.MetroSequenceBook;
import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document.MetroSequenceBookDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.repository.MetroSequenceBookRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MetroSequenceMongoBook implements MetroSequenceBook {
    //
    private final MetroSequenceBookRepository sequenceBookRepository;

    public MetroSequenceMongoBook(MetroSequenceBookRepository sequenceBookRepository) {
        //
        this.sequenceBookRepository = sequenceBookRepository;
    }

    @Override
    public int nextSequence(String naraId) {
        //
        Optional<MetroSequenceBookDoc> optionalSequenceBookDoc = sequenceBookRepository.findById(naraId);
        MetroSequenceBookDoc sequenceBookDoc = null;
        if (optionalSequenceBookDoc.isPresent()) {
            sequenceBookDoc = optionalSequenceBookDoc.get();
        }
        else {
            sequenceBookDoc = new MetroSequenceBookDoc(naraId);
        }

        int sequence = sequenceBookDoc.nextSequence();
        sequenceBookRepository.save(sequenceBookDoc);

        return sequence;
    }
}
