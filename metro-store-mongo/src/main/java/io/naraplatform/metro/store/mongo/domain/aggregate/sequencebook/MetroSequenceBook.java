package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook;

public interface MetroSequenceBook {
    //
    int nextSequence(String naraId);
}
