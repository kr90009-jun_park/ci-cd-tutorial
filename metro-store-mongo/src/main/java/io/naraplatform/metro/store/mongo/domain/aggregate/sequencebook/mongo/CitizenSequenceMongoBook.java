package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo;

import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.CitizenSequenceBook;
import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document.CitizenSequenceBookDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.repository.CitizenSequenceBookRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CitizenSequenceMongoBook implements CitizenSequenceBook {
    //
    private final CitizenSequenceBookRepository sequenceBookRepository;

    public CitizenSequenceMongoBook(CitizenSequenceBookRepository sequenceBookRepository) {
        //
        this.sequenceBookRepository = sequenceBookRepository;
    }

    @Override
    public int nextSequence(String metroId) {
        //
        Optional<CitizenSequenceBookDoc> optionalSequenceBookDoc = sequenceBookRepository.findById(metroId);
        CitizenSequenceBookDoc sequenceBookDoc = null;
        if (optionalSequenceBookDoc.isPresent()) {
            sequenceBookDoc = optionalSequenceBookDoc.get();
        }
        else {
            sequenceBookDoc = new CitizenSequenceBookDoc(metroId);
        }

        int sequence = sequenceBookDoc.nextSequence();
        sequenceBookRepository.save(sequenceBookDoc);

        return sequence;
    }
}
