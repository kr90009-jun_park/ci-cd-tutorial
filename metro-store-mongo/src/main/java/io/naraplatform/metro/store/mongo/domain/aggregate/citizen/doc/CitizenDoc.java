package io.naraplatform.metro.store.mongo.domain.aggregate.citizen.doc;

import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naraplatform.metro.domain.aggregate.citizen.entity.Citizen;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.EmailVerifedInfo;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("citizen")
public class CitizenDoc extends DomainEntityDoc {
    //
    private CitizenIdentity identity;
    private CitizenSettings settings;
    private byte[] profilePhoto;
    private EmailVerifedInfo emailVerifedInfo;

    public CitizenDoc(Citizen citizen) {
        //
        super(citizen);
        BeanUtils.copyProperties(citizen, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public Citizen toDomain() {
        //
        Citizen citizen = new Citizen(getId());
        BeanUtils.copyProperties(this, citizen);

        return citizen;
    }

    public static List<Citizen> toDomains(List<CitizenDoc> citizenDocs) {
        //
        return citizenDocs
                .stream()
                .map(CitizenDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static CitizenDoc sample() {
        //
        return new CitizenDoc(Citizen.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(Citizen.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
