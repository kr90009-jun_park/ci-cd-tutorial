package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook.mongo.document;

import io.naradrama.prologue.util.json.JsonSerializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "citizen_sequence_book")
@TypeAlias("CitizenSequenceBookDoc")
public class CitizenSequenceBookDoc implements JsonSerializable {
    //
    @Id
    private String metroId;

    private int sequence;

    public CitizenSequenceBookDoc(String metroId) {
        //
        this.metroId = metroId;
    }

    public int nextSequence() {
        //
        return ++sequence;
    }
}
