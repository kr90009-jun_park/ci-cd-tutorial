package io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc;

import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanName;
import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naradrama.prologue.domain.granule.EmailList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("castellanName")
public class CastellanNameDoc extends DomainEntityDoc {
    //
    private String displayName;
    private EmailList emails;
    private String castellanId;

    public CastellanNameDoc(CastellanName castellanName) {
        //
        super(castellanName);
        BeanUtils.copyProperties(castellanName, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public CastellanName toDomain() {
        //
        CastellanName castellanName = new CastellanName(getId());
        BeanUtils.copyProperties(this, castellanName);

        return castellanName;
    }

    public static List<CastellanName> toDomains(List<CastellanNameDoc> castellanNameDocs) {
        //
        return castellanNameDocs
                .stream()
                .map(CastellanNameDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static CastellanNameDoc sample() {
        //
        return new CastellanNameDoc(CastellanName.samples().get(0));
    }

    public static void main(String[] args) {
        //
        System.out.println(CastellanName.samples().get(0).toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
