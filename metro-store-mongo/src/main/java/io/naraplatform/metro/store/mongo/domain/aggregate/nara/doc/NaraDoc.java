package io.naraplatform.metro.store.mongo.domain.aggregate.nara.doc;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naraplatform.metro.domain.aggregate.nara.entity.NaraIdentity;
import io.naraplatform.metro.domain.aggregate.nara.entity.settings.NaraSettings;
import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("nara")
public class NaraDoc extends DomainEntityDoc {
    //
    private NaraIdentity identity;
    private NaraSettings settings;

    public NaraDoc(Nara nara) {
        //
        super(nara);
        BeanUtils.copyProperties(nara, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public Nara toDomain() {
        //
        Nara nara = new Nara(getId());
        BeanUtils.copyProperties(this, nara);

        return nara;
    }

    public static List<Nara> toDomains(List<NaraDoc> naraDocs) {
        //
        return naraDocs
                .stream()
                .map(NaraDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static NaraDoc sample() {
        //
        return new NaraDoc(Nara.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(Nara.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
