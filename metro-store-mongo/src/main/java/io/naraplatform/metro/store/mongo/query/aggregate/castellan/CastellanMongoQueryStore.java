package io.naraplatform.metro.store.mongo.query.aggregate.castellan;

import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanName;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanPhone;
import io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc.CastellanEmailDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc.CastellanNameDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc.CastellanPhoneDoc;
import io.naraplatform.metro.store.mongo.query.aggregate.castellan.repository.CastellanEmailRepository;
import io.naraplatform.metro.store.mongo.query.aggregate.castellan.repository.CastellanNameRepository;
import io.naraplatform.metro.store.mongo.query.aggregate.castellan.repository.CastellanPhoneRepository;
import io.naraplatform.metro.store.query.aggregate.castellan.CastellanQueryStore;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class CastellanMongoQueryStore implements CastellanQueryStore {
    //
    private final CastellanEmailRepository castellanEmailRepository;
    private final CastellanNameRepository castellanNameRepository;
    private final CastellanPhoneRepository castellanPhoneRepository;

    public CastellanMongoQueryStore(CastellanEmailRepository castellanEmailRepository,
                                    CastellanNameRepository castellanNameRepository,
                                    CastellanPhoneRepository castellanPhoneRepository) {
        //
        this.castellanEmailRepository = castellanEmailRepository;
        this.castellanNameRepository = castellanNameRepository;
        this.castellanPhoneRepository = castellanPhoneRepository;
    }


    @Override
    public void create(CastellanEmail castellanEmail) {
        //
        CastellanEmailDoc castellanEmailDoc = new CastellanEmailDoc(castellanEmail);
        castellanEmailRepository.save(castellanEmailDoc);
    }

    @Override
    public CastellanEmail retrieveCastellanEmailByEmail(String email) {
        //
        Optional<CastellanEmailDoc> castellanEmailDocOptional = castellanEmailRepository.findByEmail(email);
        return castellanEmailDocOptional.map(CastellanEmailDoc::toDomain).orElse(null);
    }

    @Override
    public boolean castellanEmailExistsByEmail(String email) {
        //
        return castellanEmailRepository.existsByEmail(email);
    }

    @Override
    public void update(CastellanEmail castellanEmail) {
        //
        CastellanEmailDoc castellanEmailDoc = new CastellanEmailDoc(castellanEmail);
        castellanEmailRepository.save(castellanEmailDoc);
    }

    @Override
    public void deleteCastellanEmailByCastellanId(String castellanId) {
        //
        castellanEmailRepository.deleteByCastellanId(castellanId);
    }

    @Override
    public void create(CastellanName castellanName) {
        //
        CastellanNameDoc castellanNameDoc = new CastellanNameDoc(castellanName);
        castellanNameRepository.save(castellanNameDoc);
    }

    @Override
    public List<CastellanName> retrieveCastellanNameByDisplayName(String displayName) {
        //
        List<CastellanNameDoc> castellanNameDocs = castellanNameRepository.findByDisplayName(displayName);
        return CastellanNameDoc.toDomains(castellanNameDocs);
    }

    @Override
    public void update(CastellanName castellanName) {
        //
        CastellanNameDoc castellanNameDoc = new CastellanNameDoc(castellanName);
        castellanNameRepository.save(castellanNameDoc);
    }

    @Override
    public void deleteCastellanNameByCastellanId(String castellanId) {
        //
        castellanNameRepository.deleteByCastellanId(castellanId);
    }

    @Override
    public void create(CastellanPhone castellanPhone) {
        //
        CastellanPhoneDoc castellanPhoneDoc = new CastellanPhoneDoc(castellanPhone);
        castellanPhoneRepository.save(castellanPhoneDoc);
    }

    @Override
    public void update(CastellanPhone castellanPhone) {
        //
        CastellanPhoneDoc castellanPhoneDoc = new CastellanPhoneDoc(castellanPhone);
        castellanPhoneRepository.save(castellanPhoneDoc);
    }

}
