package io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc;

import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.PreliminaryCitizen;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("preliminaryCitizen")
public class PreliminaryCitizenDoc extends DomainEntityDoc {
    //
    private CitizenIdentity identity;
    private CitizenSettings settings;
    private boolean identityModified;
    private byte[] profilePhoto;

    public PreliminaryCitizenDoc(PreliminaryCitizen preliminaryCitizen) {
        //
        super(preliminaryCitizen);
        BeanUtils.copyProperties(preliminaryCitizen, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public PreliminaryCitizen toDomain() {
        //
        PreliminaryCitizen preliminaryCitizen
                = new PreliminaryCitizen(getId());
        BeanUtils.copyProperties(this, preliminaryCitizen);

        return preliminaryCitizen;
    }

    public static List<PreliminaryCitizen> toDomains(List<PreliminaryCitizenDoc> preliminaryCitizenDocs) {
        //
        return preliminaryCitizenDocs
                .stream()
                .map(PreliminaryCitizenDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static PreliminaryCitizenDoc sample() {
        //
        return new PreliminaryCitizenDoc(PreliminaryCitizen.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(PreliminaryCitizen.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
