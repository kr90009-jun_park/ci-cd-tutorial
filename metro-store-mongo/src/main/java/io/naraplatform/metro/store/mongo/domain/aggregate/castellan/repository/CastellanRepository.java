package io.naraplatform.metro.store.mongo.domain.aggregate.castellan.repository;

import io.naraplatform.metro.store.mongo.domain.aggregate.castellan.doc.CastellanDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CastellanRepository extends MongoRepository<CastellanDoc, String> {
    //
}
