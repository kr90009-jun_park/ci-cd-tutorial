package io.naraplatform.metro.store.mongo.domain.aggregate.citizen.repository;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.metro.store.mongo.domain.aggregate.citizen.doc.CitizenIdentityDoc;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CitizenIdentityRepository extends MongoRepository<CitizenIdentityDoc, String> {
    //
    Optional<CitizenIdentityDoc> findByMetroIdAndEmail(String metroId, String email);
    List<CitizenIdentityDoc> findByMetroId(String metroId);
    List<CitizenIdentityDoc> findByMetroIdAndState(String metroId, CitizenState state);
    List<CitizenIdentityDoc> findByIdIn(List<String> citizenIds);
    List<CitizenIdentityDoc> findByMetroId(String metroId, Pageable pageable);

    List<CitizenIdentityDoc> findByMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
            String metroId, String displayLike, String metroId2, String emailLike, Pageable pageable);
    List<CitizenIdentityDoc> findByMetroIdAndState(String metroId, CitizenState state, Pageable pageable);
    List<CitizenIdentityDoc> findByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
            String metroId, CitizenState state, String displayLike, String metroId2, CitizenState state2, String emailLike, Pageable pageable);

    int countByState(CitizenState state);
    int countByMetroId(String metroId);
    int countByMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
            String metroId, String displayLike, String metroId2, String emailLike);
    int countByMetroIdAndState(String metroId, CitizenState state);
    int countByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
            String metroId, CitizenState state, String displayLike, String metroId2, CitizenState state2, String emailLike);
}
