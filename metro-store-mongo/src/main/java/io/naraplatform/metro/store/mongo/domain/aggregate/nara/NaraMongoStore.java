package io.naraplatform.metro.store.mongo.domain.aggregate.nara;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naraplatform.metro.domain.aggregate.nara.entity.NaraIdentity;
import io.naraplatform.metro.store.domain.aggregate.nara.NaraStore;
import io.naraplatform.metro.store.mongo.domain.aggregate.nara.doc.NaraDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.nara.doc.NaraIdentityDoc;
import io.naraplatform.metro.store.mongo.domain.aggregate.nara.repository.NaraIdentityRepository;
import io.naraplatform.metro.store.mongo.domain.aggregate.nara.repository.NaraRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class NaraMongoStore implements NaraStore {
    //
    private final NaraRepository naraRepository;
    private final NaraIdentityRepository naraIdentityRepository;

    public NaraMongoStore(NaraRepository naraRepository,
                          NaraIdentityRepository naraIdentityRepository) {
        //
        this.naraRepository = naraRepository;
        this.naraIdentityRepository = naraIdentityRepository;
    }

    @Override
    public void create(Nara nara) {
        //
        NaraDoc naraDoc = new NaraDoc(nara);
        naraRepository.save(naraDoc);
    }

    @Override
    public Nara retrieve(String naraId) {
        //
        Optional<NaraDoc> naraDocOptional = naraRepository.findById(naraId);
        return naraDocOptional.map(NaraDoc::toDomain).orElse(null);
    }

    @Override
    public List<Nara> retrieveAll() {
        //
        List<NaraDoc> naraDocs = naraRepository.findAll();
        return NaraDoc.toDomains(naraDocs);
    }

    @Override
    public void update(Nara nara) {
        //
        NaraDoc naraDoc = new NaraDoc(nara);
        naraRepository.save(naraDoc);
    }

    @Override
    public void delete(String naraId) {
        //
        Optional<NaraDoc> naraDocOptional = naraRepository.findById(naraId);
        if( naraDocOptional.isPresent() ) {
            Nara nara = naraDocOptional.map(NaraDoc::toDomain).orElse(null);
            naraRepository.deleteById(nara.getId());
        }
    }


    @Override
    public void create(NaraIdentity identity) {
        //
        NaraIdentityDoc identityDoc = new NaraIdentityDoc(identity);
        naraIdentityRepository.save(identityDoc);
    }

    @Override
    public NaraIdentity retrieveNaraIdentity(String naraId) {
        //
        Optional<NaraIdentityDoc> naraIdentityDocOptional = naraIdentityRepository.findById(naraId);
        return naraIdentityDocOptional.map(NaraIdentityDoc::toDomain).orElse(null);
    }

    @Override
    public void update(NaraIdentity identity) {
        //
        NaraIdentityDoc identityDoc = new NaraIdentityDoc(identity);
        naraIdentityRepository.save(identityDoc);
    }
}
