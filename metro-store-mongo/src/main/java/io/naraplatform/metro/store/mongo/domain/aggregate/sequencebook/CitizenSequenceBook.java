package io.naraplatform.metro.store.mongo.domain.aggregate.sequencebook;

public interface CitizenSequenceBook {
    //
    int nextSequence(String metroId);
}
