package io.naraplatform.metro.store.mongo.domain.aggregate.nara.doc;

import io.naraplatform.metro.domain.aggregate.nara.entity.NaraIdentity;
import io.naraplatform.metro.domain.aggregate.nara.entity.NaraState;
import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naraplatform.share.domain.tenant.NaraType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("naraIdentity")
public class NaraIdentityDoc extends DomainEntityDoc {
    //
    private String name;
    private NaraType type;
    private NaraState state;

    public NaraIdentityDoc(NaraIdentity naraIdentity) {
        //
        super(naraIdentity);
        BeanUtils.copyProperties(naraIdentity, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public NaraIdentity toDomain() {
        //
        NaraIdentity naraIdentity = new NaraIdentity(getId());
        BeanUtils.copyProperties(this, naraIdentity);

        return naraIdentity;
    }

    public static List<NaraIdentity> toDomains(List<NaraIdentityDoc> naraIdentityDocs) {
        //
        return naraIdentityDocs
                .stream()
                .map(NaraIdentityDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static NaraIdentityDoc sample() {
        //
        return new NaraIdentityDoc(NaraIdentity.sampleForHome());
    }

    public static void main(String[] args) {
        //
        System.out.println(NaraIdentity.sampleForHome().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
