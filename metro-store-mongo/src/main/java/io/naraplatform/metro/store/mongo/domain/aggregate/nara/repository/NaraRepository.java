package io.naraplatform.metro.store.mongo.domain.aggregate.nara.repository;

import io.naraplatform.metro.store.mongo.domain.aggregate.nara.doc.NaraDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NaraRepository extends MongoRepository<NaraDoc, String> {
    //
}
