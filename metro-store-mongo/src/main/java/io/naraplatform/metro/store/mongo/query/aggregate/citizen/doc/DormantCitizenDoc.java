package io.naraplatform.metro.store.mongo.query.aggregate.citizen.doc;

import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.settings.CitizenSettings;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.DormantCitizen;
import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("dormantCitizen")
public class DormantCitizenDoc extends DomainEntityDoc {
    //
    private CitizenIdentity identity;
    private CitizenSettings settings;
    private boolean identityModified;
    private byte[] profilePhoto;

    public DormantCitizenDoc(DormantCitizen dormantCitizen) {
        //
        super(dormantCitizen);
        BeanUtils.copyProperties(dormantCitizen, this);
    }

    public String toString() {
        //
        return toJson();
    }

    public DormantCitizen toDomain() {
        //
        DormantCitizen dormantCitizen
                = new DormantCitizen(getId());
        BeanUtils.copyProperties(this, dormantCitizen);

        return dormantCitizen;
    }

    public static List<DormantCitizen> toDomains(List<DormantCitizenDoc> dormantCitizenDocs) {
        //
        return dormantCitizenDocs
                .stream()
                .map(DormantCitizenDoc::toDomain)
                .collect(Collectors.toList());
    }

    public static DormantCitizenDoc sample() {
        //
        return new DormantCitizenDoc(DormantCitizen.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(DormantCitizen.sample().toPrettyJson());
        System.out.println(sample().toPrettyJson());
    }
}
