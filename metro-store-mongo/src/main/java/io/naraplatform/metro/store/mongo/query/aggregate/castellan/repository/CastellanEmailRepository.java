package io.naraplatform.metro.store.mongo.query.aggregate.castellan.repository;

import io.naraplatform.metro.store.mongo.query.aggregate.castellan.doc.CastellanEmailDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CastellanEmailRepository extends MongoRepository<CastellanEmailDoc, String> {
    //
    Optional<CastellanEmailDoc> findByEmail(String email);
    void deleteByCastellanId(String castellanId);
    boolean existsByEmail(String email);
}
