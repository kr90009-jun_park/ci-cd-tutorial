package io.naraplatform.metro.query.rest;


import io.naraplatform.metro.domain.aggregate.citizen.query.*;
import io.naraplatform.metro.domain.facade.aggregate.citizen.CitizenQueryFacade;
import io.naraplatform.metro.query.logic.CitizenQueryLogic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("citizen")
public class CitizenQueryResource implements CitizenQueryFacade {
    //
    private final CitizenQueryLogic citizenQueryLogic;

    public CitizenQueryResource(CitizenQueryLogic citizenQueryLogic) {
        //
        this.citizenQueryLogic = citizenQueryLogic;
    }

    @Override
    @PostMapping("find")
    public FindCitizenQuery findCitizen(@RequestBody FindCitizenQuery findCitizenQuery) {
        //
        return citizenQueryLogic.execute(findCitizenQuery);
    }

    @Override
    @PostMapping("find-identity")
    public FindCitizenIdentityQuery findCitizenIdentity(
            @RequestBody FindCitizenIdentityQuery findCitizenIdentityQuery) {
        //
        return citizenQueryLogic.execute(findCitizenIdentityQuery);
    }

    @Override
    @PostMapping("find-identity-by-metro-id-and-email")
    public FindCitizenIdentityByMetroIdAndEmailQuery findCitizenIdentityByMetroIdAndEmail(
            @RequestBody FindCitizenIdentityByMetroIdAndEmailQuery findCitizenIdentityByMetroIdAndEmailQuery) {
        //
        return citizenQueryLogic.execute(findCitizenIdentityByMetroIdAndEmailQuery);
    }

    @Override
    @PostMapping("find-identities")
    public FindCitizenIdentitiesQuery findCitizenIdentities(
            @RequestBody FindCitizenIdentitiesQuery findCitizenIdentitiesQuery) {
        //
        return citizenQueryLogic.execute(findCitizenIdentitiesQuery);
    }

    @Override
    @PostMapping("find-citizens-by-metro-id-and-state")
    public FindCitizensByMetroIdAndStateQuery findCitizensByMetroIdAndState(@RequestBody FindCitizensByMetroIdAndStateQuery findCitizensByMetroIdAndStateQuery) {
        //
        return citizenQueryLogic.execute(findCitizensByMetroIdAndStateQuery);
    }


    @Override
    @PostMapping("find-identities-by-state")
    public FindCitizenIdentitiesByStateQuery findCitizenIdentitiesByState(
            @RequestBody FindCitizenIdentitiesByStateQuery findCitizenIdentitiesByStateQuery) {
        //
        return citizenQueryLogic.execute(findCitizenIdentitiesByStateQuery);
    }

    @Override
    @PostMapping("find-citizen-identities-by-ids")
    public FindCitizenIdentitiesByIdsQuery findCitizenIdentitiesByIds(
            @RequestBody FindCitizenIdentitiesByIdsQuery findCitizenIdentitiesByIdsQuery) {
        //
        return citizenQueryLogic.execute(findCitizenIdentitiesByIdsQuery);
    }

    @Override
    @PostMapping("count-by-metro-id")
    public FindCitizenCountsQuery countCitizensByMetroId(
            @RequestBody FindCitizenCountsQuery findCitizenCountsQuery) {
        //
        return citizenQueryLogic.execute(findCitizenCountsQuery);
    }

    @Override
    @PostMapping("find-identities-by-metro-id-and-state-and-keyword")
    public FindCitizenIdentitiesByMetroIdAndStateAndKeywordQuery findCitizenIdentitiesByMetroIdAndStateAndKeyword(
            @RequestBody FindCitizenIdentitiesByMetroIdAndStateAndKeywordQuery findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery) {
        //
        return citizenQueryLogic.execute(findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery);
    }
}
