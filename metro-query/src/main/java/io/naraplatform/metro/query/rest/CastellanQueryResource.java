package io.naraplatform.metro.query.rest;

import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailAndNameQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellansByDisplayNameQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellansQuery;
import io.naraplatform.metro.domain.facade.aggregate.castellan.CastellanQueryFacade;
import io.naraplatform.metro.query.logic.CastellanQueryLogic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("castellan")
public class CastellanQueryResource implements CastellanQueryFacade {
    //
    private final CastellanQueryLogic castellanQueryLogic;

    public CastellanQueryResource(CastellanQueryLogic castellanQueryLogic) {
        //
        this.castellanQueryLogic = castellanQueryLogic;
    }

    @Override
    @PostMapping("find-by-email")
    public FindCastellanEmailByEmailQuery findCastellanEmailByEmail(
            @RequestBody FindCastellanEmailByEmailQuery query) {
        //
        return castellanQueryLogic.execute(query);
    }

    @Override
    @PostMapping("find-by-email-and-name")
    public FindCastellanEmailByEmailAndNameQuery findCastellanByEmailAndName(
            @RequestBody FindCastellanEmailByEmailAndNameQuery query) {
        //
        return castellanQueryLogic.execute(query);
    }

    @Override
    @PostMapping("find-castellans")
    public FindCastellansQuery findCastellans(@RequestBody FindCastellansQuery query) {
        //
        return castellanQueryLogic.execute(query);
    }

    @Override
    @PostMapping("find-castellans-by-display-name")
    public FindCastellansByDisplayNameQuery findCastellansByDisplayName(
            @RequestBody FindCastellansByDisplayNameQuery query) {
        //
        return castellanQueryLogic.execute(query);
    }
}
