package io.naraplatform.metro.query.logic;


import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naraplatform.metro.domain.aggregate.metro.query.FindMetroQuery;
import io.naraplatform.metro.domain.aggregate.metro.query.FindMetrosQuery;
import io.naraplatform.metro.store.domain.aggregate.metro.MetroStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class MetroQueryLogic {
    //
    private final MetroStore metroStore;

    public MetroQueryLogic(MetroStore metroStore) {
        //
        this.metroStore = metroStore;
    }

    public FindMetroQuery execute(FindMetroQuery findMetroQuery) {
        //
        Metro metro = metroStore.retrieveByMetroId(findMetroQuery.getMetroId());
        findMetroQuery.setQueryResult(metro);
        return findMetroQuery;
    }

    public FindMetrosQuery execute(FindMetrosQuery findMetrosQuery) {
        //
        List<Metro> metros = metroStore.retrieveAll();
        findMetrosQuery.setQueryResult(metros);
        return findMetrosQuery;
    }
}
