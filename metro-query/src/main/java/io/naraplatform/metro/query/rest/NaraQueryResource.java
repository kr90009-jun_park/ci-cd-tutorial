package io.naraplatform.metro.query.rest;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naraplatform.metro.domain.aggregate.nara.query.FindNaraQuery;
import io.naraplatform.metro.domain.aggregate.nara.query.FindNaraResponse;
import io.naraplatform.metro.domain.aggregate.nara.query.FindNarasQuery;
import io.naraplatform.metro.domain.aggregate.nara.query.FindNarasResponse;
import io.naraplatform.metro.domain.facade.aggregate.nara.NaraQueryFacade;
import io.naraplatform.metro.query.logic.NaraQueryLogic;
import io.naraplatform.metro.store.domain.aggregate.nara.NaraStore;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("naras")
public class NaraQueryResource implements NaraQueryFacade {
    //
    private final NaraQueryLogic naraQueryLogic;

    public NaraQueryResource(NaraQueryLogic naraQueryLogic) {
        //
        this.naraQueryLogic = naraQueryLogic;
    }

    @Override
    @PostMapping("find-nara")
    public FindNaraQuery findNara(@RequestBody FindNaraQuery findNaraQuery) {
        //
        return naraQueryLogic.execute(findNaraQuery);
    }

    @Override
    @PostMapping("find-naras")
    public FindNarasQuery findNaras(@RequestBody FindNarasQuery findNarasQuery) {
        //
        return naraQueryLogic.execute(findNarasQuery);
    }
}
