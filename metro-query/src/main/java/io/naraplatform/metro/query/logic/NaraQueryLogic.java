package io.naraplatform.metro.query.logic;

import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naraplatform.metro.domain.aggregate.nara.query.FindNaraQuery;
import io.naraplatform.metro.domain.aggregate.nara.query.FindNarasQuery;
import io.naraplatform.metro.store.domain.aggregate.nara.NaraStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;


@Slf4j
@Service
public class NaraQueryLogic {
    //
    private final NaraStore naraStore;

    public NaraQueryLogic(NaraStore naraStore) {
        //
        this.naraStore = naraStore;
    }

    public FindNaraQuery execute(FindNaraQuery findNaraQuery) {
        //
        Nara nara = naraStore.retrieve(findNaraQuery.getNaraId());
        findNaraQuery.setQueryResult(nara);
        return findNaraQuery;
    }

    public FindNarasQuery execute(FindNarasQuery findNarasQuery) {
        //
        List<Nara> naras = naraStore.retrieveAll();
        findNarasQuery.setQueryResult(naras);
        return findNarasQuery;
    }
}
