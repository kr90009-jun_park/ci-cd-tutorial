package io.naraplatform.metro.query.grpc;

import io.grpc.stub.StreamObserver;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailQuery;
import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
import io.naraplatform.metro.proto.query.CastellanQueryProtoGrpc;
import io.naraplatform.metro.proto.query.FindCastellanEmailByEmailQueryMsg;
import io.naraplatform.metro.query.logic.CastellanQueryLogic;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;


@Slf4j
@GRpcService
public class CastellanQueryGrpc extends CastellanQueryProtoGrpc.CastellanQueryProtoImplBase {
    //

    @Autowired
    private CastellanQueryLogic castellanQueryLogic;

    @Override
    public void findCastellanEmailByEmail(FindCastellanEmailByEmailQueryMsg request, StreamObserver<FindCastellanEmailByEmailQueryMsg> responseObserver) {
         //
        FindCastellanEmailByEmailQuery query = MetroGrpcMessage.fromMessage(request);
        try {
            query = castellanQueryLogic.execute(query);
        } catch (Throwable e){
            log.error("failed_to_find_castellan_email_by_email_query : \n{}", query, e);
            query.setFailureMessage(new FailureMessage(e));
        }

        responseObserver.onNext(MetroGrpcMessage.toMessage(query));
        responseObserver.onCompleted();
    }
}
