package io.naraplatform.metro.query.rest;

import io.naraplatform.metro.domain.aggregate.metro.entity.Metro;
import io.naraplatform.metro.domain.aggregate.metro.query.FindMetroQuery;
import io.naraplatform.metro.domain.aggregate.metro.query.FindMetroResponse;
import io.naraplatform.metro.domain.aggregate.metro.query.FindMetrosQuery;
import io.naraplatform.metro.domain.aggregate.metro.query.FindMetrosResponse;
import io.naraplatform.metro.domain.facade.aggregate.metro.MetroQueryFacade;
import io.naraplatform.metro.query.logic.MetroQueryLogic;
import io.naraplatform.metro.store.domain.aggregate.metro.MetroStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("metro")
public class MetroQueryResource implements MetroQueryFacade {
    //
    private final MetroQueryLogic metroQueryLogic;

    public MetroQueryResource(MetroQueryLogic metroQueryLogic) {
        //
        this.metroQueryLogic = metroQueryLogic;
    }

    @Override
    @PostMapping("find")
    public FindMetroQuery findMetro(@RequestBody FindMetroQuery findMetroQuery) {
        //
        return metroQueryLogic.execute(findMetroQuery);
    }

    @Override
    @PostMapping("find-metros")
    public FindMetrosQuery findMetros(@RequestBody FindMetrosQuery findMetrosQuery) {
        //
        return metroQueryLogic.execute(findMetrosQuery);
    }
}
