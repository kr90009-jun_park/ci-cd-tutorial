package io.naraplatform.metro.query.grpc;

import io.grpc.stub.StreamObserver;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentitiesByIdsQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityByMetroIdAndEmailQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityQuery;
import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
import io.naraplatform.metro.proto.query.CitizenQueryProtoGrpc;
import io.naraplatform.metro.proto.query.FindCitizenIdentitiesByIdsQueryMsg;
import io.naraplatform.metro.proto.query.FindCitizenIdentityByMetroIdAndEmailQueryMsg;
import io.naraplatform.metro.proto.query.FindCitizenIdentityQueryMsg;
import io.naraplatform.metro.query.logic.CitizenQueryLogic;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
@GRpcService
public class CitizenQueryGrpc extends CitizenQueryProtoGrpc.CitizenQueryProtoImplBase {
    //
    private final CitizenQueryLogic citizenQueryLogic;

    public CitizenQueryGrpc(CitizenQueryLogic citizenQueryLogic) {
        //
        this.citizenQueryLogic = citizenQueryLogic;
    }

    @Override
    public void findCitizenIdentity(FindCitizenIdentityQueryMsg request, StreamObserver<FindCitizenIdentityQueryMsg> responseObserver) {
        //
        FindCitizenIdentityQuery query = MetroGrpcMessage.fromMessage(request);
        query = citizenQueryLogic.execute(query);

        responseObserver.onNext(MetroGrpcMessage.toMessage(query));
        responseObserver.onCompleted();
    }

    @Override
    public void findCitizenIdentitiesByIds(FindCitizenIdentitiesByIdsQueryMsg request, StreamObserver<FindCitizenIdentitiesByIdsQueryMsg> responseObserver) {
        //
        FindCitizenIdentitiesByIdsQuery query = MetroGrpcMessage.fromMessage(request);
        query = citizenQueryLogic.execute(query);

        responseObserver.onNext(MetroGrpcMessage.toMessage(query));
        responseObserver.onCompleted();
    }

    @Override
    public void findCitizenIdentityByMetroIdAndEmail(FindCitizenIdentityByMetroIdAndEmailQueryMsg request, StreamObserver<FindCitizenIdentityByMetroIdAndEmailQueryMsg> responseObserver) {
        //
        FindCitizenIdentityByMetroIdAndEmailQuery query = MetroGrpcMessage.fromMessage(request);
        query = citizenQueryLogic.execute(query);

        responseObserver.onNext(MetroGrpcMessage.toMessage(query));
        responseObserver.onCompleted();
    }
}
