package io.naraplatform.metro.query.logic;

import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.OffsetElementList;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenState;
import io.naraplatform.metro.domain.aggregate.citizen.query.*;
import io.naraplatform.metro.domain.aggregate.citizen.querymodel.AbstractCitizen;
import io.naraplatform.metro.store.domain.aggregate.citizen.CitizenStore;
import io.naraplatform.metro.store.query.aggregate.citizen.CitizenQueryStore;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;


@Slf4j
@Service
public class CitizenQueryLogic {
    //
    private final CitizenStore citizenStore;
    private final CitizenQueryStore citizenQueryStore;

    public CitizenQueryLogic(CitizenStore citizenStore,
                             CitizenQueryStore citizenQueryStore) {
        //
        this.citizenStore = citizenStore;
        this.citizenQueryStore = citizenQueryStore;
    }

    public FindCitizenQuery execute(FindCitizenQuery findCitizenQuery) {
        //
        AbstractCitizen abstractCitizen = citizenQueryStore.retrieve(findCitizenQuery.getCitizenId());
        findCitizenQuery.setQueryResult(abstractCitizen);
        return findCitizenQuery;
    }

    public FindCitizenIdentityQuery execute(
            FindCitizenIdentityQuery findCitizenIdentityQuery) {
        //
        CitizenIdentity identity = citizenStore.retrieveCitizenIdentity(findCitizenIdentityQuery.getCitizenId());
        findCitizenIdentityQuery.setQueryResult(identity);
        return findCitizenIdentityQuery;
    }

    public FindCitizenIdentityByMetroIdAndEmailQuery execute(
            FindCitizenIdentityByMetroIdAndEmailQuery findCitizenIdentityByMetroIdAndEmailQuery) {
        //
        CitizenIdentity identity = citizenStore.retrieveCitizenIdentityByMetroIdAndEmail(
                findCitizenIdentityByMetroIdAndEmailQuery.getMetroId(),
                findCitizenIdentityByMetroIdAndEmailQuery.getEmail());
        findCitizenIdentityByMetroIdAndEmailQuery.setQueryResult(identity);
        return findCitizenIdentityByMetroIdAndEmailQuery;
    }

    public FindCitizenIdentitiesQuery execute(
            FindCitizenIdentitiesQuery findCitizenIdentitiesQuery) {
        //
        List<CitizenIdentity> identities
                = citizenStore.retrieveCitizenIdentityByMetroId(findCitizenIdentitiesQuery.getMetroId());
        findCitizenIdentitiesQuery.setQueryResult(identities);
        return findCitizenIdentitiesQuery;
    }

    public FindCitizensByMetroIdAndStateQuery execute(
            FindCitizensByMetroIdAndStateQuery findCitizensByMetroIdAndStateQuery) {
        //
        List<AbstractCitizen> citizens = citizenQueryStore.retrieveByMetroIdAndState(findCitizensByMetroIdAndStateQuery.getMetroId(), findCitizensByMetroIdAndStateQuery.getState());
        findCitizensByMetroIdAndStateQuery.setQueryResult(citizens);

        return findCitizensByMetroIdAndStateQuery;
    }

    public FindCitizenIdentitiesByStateQuery execute(
            FindCitizenIdentitiesByStateQuery findCitizenIdentitiesByStateQuery) {
        //
        List<CitizenIdentity> identities = citizenStore.retrieveCitizenIdentityByMetroIdAndState(
                findCitizenIdentitiesByStateQuery.getMetroId(),
                findCitizenIdentitiesByStateQuery.getState());
        findCitizenIdentitiesByStateQuery.setQueryResult(identities);
        return findCitizenIdentitiesByStateQuery;
    }

    public FindCitizenIdentitiesByIdsQuery execute(
            FindCitizenIdentitiesByIdsQuery findCitizenIdentitiesByIdsQuery) {
        //
        List<CitizenIdentity> identities
                = citizenStore.retrieveCitizenIdentityByIdIn(findCitizenIdentitiesByIdsQuery.getCitizenIds());
        findCitizenIdentitiesByIdsQuery.setQueryResult(identities);
        return findCitizenIdentitiesByIdsQuery;
    }

    public FindCitizenCountsQuery execute(
            FindCitizenCountsQuery findCitizenCountsQuery) {
        String metroId = findCitizenCountsQuery.getMetroId();
        int activeCount = citizenStore.countCitizenIdentityByMetroIdAndState(metroId, CitizenState.Active);
        int preliminaryCount = citizenStore.countCitizenIdentityByMetroIdAndState(metroId, CitizenState.Preliminary);
        int dormantCount = citizenStore.countCitizenIdentityByMetroIdAndState(metroId, CitizenState.Dormant);
        int removedCount = citizenStore.countCitizenIdentityByMetroIdAndState(metroId, CitizenState.Removed);

        findCitizenCountsQuery.setQueryResult(new CitizenCountResposne(activeCount, preliminaryCount, dormantCount, removedCount));
        return findCitizenCountsQuery;
    }

    public FindCitizenIdentitiesByMetroIdAndStateAndKeywordQuery execute(
            FindCitizenIdentitiesByMetroIdAndStateAndKeywordQuery findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery) {
        //
        String metroId = findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery.getMetroId();
        CitizenState state = findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery.getState();
        String keyword = findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery.getKeyword();
        int offset = findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery.getOffset().offset();
        int limit = findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery.getOffset().getLimit();
        int count = 0;

        List<CitizenIdentity> citizenIdentities = null;
        if (state == null) {
            if (StringUtils.isEmpty(keyword)) {
                citizenIdentities = citizenStore.retrieveCitizenIdentityByMetroId(metroId, new Offset(offset, limit));
                count = citizenStore.countCitizenIdentityByMetroId(metroId);
            } else {
                citizenIdentities = citizenStore.retrieveCitizenIdentityByfMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
                        metroId, keyword, metroId, keyword, new Offset(offset, limit));
                count = citizenStore.countCitizenIdentityByMetroIdAndDisplayNameLikeOrMetroIdAndEmailLike(
                        metroId, keyword, metroId, keyword);
            }
        } else {
            if (StringUtils.isEmpty(keyword)) {
                citizenIdentities
                        = citizenStore.retrieveCitizenIdentityByMetroIdAndState(metroId, state, new Offset(offset, limit));
                count = citizenStore.countCitizenIdentityByMetroIdAndState(metroId, state);
            } else {
                citizenIdentities
                        = citizenStore.retrieveCitizenIdentityByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
                        metroId, state, keyword, metroId, state, keyword, new Offset(offset, limit));
                count = citizenStore.countCitizenIdentityByMetroIdAndStateAndDisplayNameLikeOrMetroIdAndStateAndEmailLike(
                        metroId, state, keyword, metroId, state, keyword);
            }
        }

        findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery.setQueryResult(new OffsetElementList(citizenIdentities, count));
        return findCitizenIdentitiesByMetroIdAndStateAndKeywordQuery;
    }

}
