package io.naraplatform.metro.query.logic;


import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailAndNameQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellansByDisplayNameQuery;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellansQuery;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanName;
import io.naraplatform.metro.store.domain.aggregate.castellan.CastellanStore;
import io.naraplatform.metro.store.query.aggregate.castellan.CastellanQueryStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class CastellanQueryLogic {
    //
    private final CastellanStore castellanStore;
    private final CastellanQueryStore castellanQueryStore;

    public CastellanQueryLogic(CastellanStore castellanStore,
                               CastellanQueryStore castellanQueryStore) {
        //
        this.castellanStore = castellanStore;
        this.castellanQueryStore = castellanQueryStore;
    }

    public FindCastellanEmailByEmailQuery execute(
            FindCastellanEmailByEmailQuery findCastellanEmailByEmailQuery) {
        //
        CastellanEmail castellanEmail = castellanQueryStore.retrieveCastellanEmailByEmail(findCastellanEmailByEmailQuery.getEmail());

        findCastellanEmailByEmailQuery.setQueryResult(castellanEmail);
        return findCastellanEmailByEmailQuery;
    }

    public FindCastellanEmailByEmailAndNameQuery execute(
            FindCastellanEmailByEmailAndNameQuery findCastellanEmailByEmailAndNameQuery) {
        //
        CastellanEmail castellanEmail = castellanQueryStore.retrieveCastellanEmailByEmail(findCastellanEmailByEmailAndNameQuery.getEmail());
        if( castellanEmail == null ) {
            return findCastellanEmailByEmailAndNameQuery;
        }

        Castellan castellan = castellanStore.retrieve(castellanEmail.getCastellanId());
        if (castellan.getNames().list().contains(findCastellanEmailByEmailAndNameQuery.getName())) {
            findCastellanEmailByEmailAndNameQuery.setQueryResult(castellanEmail);
            return findCastellanEmailByEmailAndNameQuery;
        }
        else {
            return findCastellanEmailByEmailAndNameQuery;
        }
    }

    public FindCastellansQuery execute(FindCastellansQuery findCastellansQuery) {
        //
        List<Castellan> castellans = castellanStore.retrieveAll();
        findCastellansQuery.setQueryResult(castellans);
        return findCastellansQuery;
    }

    public FindCastellansByDisplayNameQuery execute(
            FindCastellansByDisplayNameQuery findCastellansByDisplayNameQuery) {
        //
        List<CastellanName> castellanNames
                = castellanQueryStore.retrieveCastellanNameByDisplayName(findCastellansByDisplayNameQuery.getDisplayName());
        findCastellansByDisplayNameQuery.setQueryResult(castellanNames);
        return findCastellansByDisplayNameQuery;
    }

}
