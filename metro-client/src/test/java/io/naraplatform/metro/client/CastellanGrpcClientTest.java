package io.naraplatform.metro.client;

// todo fixme
//@Slf4j
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = MetroClientTestApplication.class)
public class CastellanGrpcClientTest {
    //
//    @Autowired
//    private CastellanGrpcClient castellanGrpcClient;
//    @Autowired
//    private JwtUtil jwtUtil;
//    @Autowired
//    private TokenCreator tokenCreator;
//
//    private static RegisterManagerCastellanCommand registerManagerCastellanCommand;
//    private static boolean initialized;
//    private static String managerCastellanId;
//
//    private void setCurrentDramaRequest() {
//        //
//        String audienceId = TokenCreator.getWorkspaceList().getCineroomWorkspaces().get(0).getTenantId();
//        String token = tokenCreator.createToken();
//        DramaRequest dramaRequest = jwtUtil.createDramaRequest(audienceId, token);
//        log.debug("dramaRequest {}", dramaRequest.toPrettyJson());
//        StageContext.setCurrentRequest(dramaRequest);
//    }
//
//    @Before
//    public void before() {
//        //
//        log.debug("\n## initialized {}", initialized);
//        if (initialized) {
//            return;
//        }
//        setCurrentDramaRequest();
//
//        registerManagerCastellanCommand = RegisterManagerCastellanCommand.sample();
//        CommandStringResponse commandStringResponse = castellanGrpcClient.registerManagerCastellan(
//                registerManagerCastellanCommand);
//        managerCastellanId = commandStringResponse.getResult();
//
//        log.debug("\n## in before() register manager castellan {}", commandStringResponse.toPrettyJson());
//        initialized = true;
//        log.debug("\n## initialized {}", initialized);
//    }
//
//    @Test
//    public void registerManagerCastellanTest() {
//        //
//        RegisterManagerCastellanCommand command = RegisterManagerCastellanCommand.sample();
//        command.setManagerId("r2p9@nea-m5");
//        command.setEmail("kspark@nextree.io");
//        command.setName(LangString.sample());
//        command.setEncryptedPassword("$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei");
//        CommandStringResponse commandStringResponse = castellanGrpcClient.registerManagerCastellan(command);
//        CommandStringResponse commandStringResponse = castellanGrpcClient.registerManagerCastellan(command, commandIdentity);
//        CommandStringResponse commandStringResponse = castellanGrpcClient.registerManagerCastellan(command, command.genCommandIdentity());
//        log.debug("\n## commandStringResponse in registerManagerCastellanTest() {}", commandStringResponse.toPrettyJson());
//
//        assertFalse(commandStringResponse.getResult().isEmpty());
//        assertTrue(commandStringResponse.getException().isEmpty());
//    }
//
//    @Rule
//    public ExpectedException exceptionRule = ExpectedException.none();
//
//    @Test
//    public void registerManagerCastellanFailTest() {
//        //
//        // todo 이후 GRPC 에서 예외발생시 처리할 공통 예외 AOP 등이 정의되면 예상 예외 타입 변경될 것임.
//        exceptionRule.expect(StatusRuntimeException.class);
//        exceptionRule.expectMessage("UNKNOWN");
//
//        RegisterManagerCastellanCommand command = RegisterManagerCastellanCommand.sample();
//        command.setManagerId("r2p3@nea-m5");
//        CommandStringResponse commandStringResponse = castellanGrpcClient.registerManagerCastellan(command);
//        log.debug("\n## commandStringResponse in registerManagerCastellanFailTest() {}", commandStringResponse.toPrettyJson());
//
////        assertFalse(commandStringResponse.getException().isEmpty());
//    }
//
//    @Test
//    public void addManagingWorkspaceTest() {
//        //
//        AddManagingWorkspaceCommand command = new AddManagingWorkspaceCommand(
//                managerCastellanId,
//                "r2p8@nea-m5",
//                "nea-m6",
//                "Nextree IO",
//                WorkspaceType.Pavilion
//        );
//
//        CommandVoidResponse commandVoidResponse = castellanGrpcClient.addManagingWorkspace(command);
//        log.debug("\n## commandVoidResponse in addManagingWorkspace() {}", commandVoidResponse.toPrettyJson());
//
//        assertTrue(commandVoidResponse != null);
//    }
//
//    @Test
//    public void registerPavilionManagerCastellanTest() {
//        //
//        RegisterPavilionManagerCastellanCommand command =
//                RegisterPavilionManagerCastellanCommand.sample();
//
//        command.setEmail("kspark2@nextree.io");
//        command.setName(LangString.sample());
//        command.setEncryptedPassword("$2a$10$3BeZxGkl2r6mdwIOThQJGuCixfE8psg7x82j12b1maKdg1mpWRGei");
//
//        CommandStringResponse commandStringResponse = castellanGrpcClient.registerPavilionManagerCastellan(command);
//        log.debug("\n## commandStringResponse in registerPavilionManagerCastellan() {}", commandStringResponse.toPrettyJson());
//
//        assertFalse(commandStringResponse.getResult().isEmpty());
//    }
//
//    @Test
//    public void addJoinedMetroForPavilionManagerTest() {
//        //
//        AddJoinedMetroForPavilionManagerCommand command
//                = AddJoinedMetroForPavilionManagerCommand.sample(7);
//        command.setCastellanId(managerCastellanId);
//
//        CommandStringResponse commandStringResponse = castellanGrpcClient.addJoinedMetroForPavilionManager(command);
//        log.debug("\n## commandStringResponse in addJoinedMetroForPavilionManager() {}", commandStringResponse.toPrettyJson());
//
//        assertFalse(commandStringResponse.getResult().isEmpty());
//    }
//
//    @Test
//    public void findCastellanByEmailTest() {
//        //
//        String email = registerManagerCastellanCommand.getEmail();
//        CastellanEmailResponse response = castellanGrpcClient.findCastellanByEmail(
//                new FindCastellanEmailByEmailQuery(email));
//        log.debug("\n## FindCastellanByEmailResponse() {}", response.toPrettyJson());
//
//        assertEquals(response.getCastellanEmail().getEmail(), email);
//    }
}
