package io.naraplatform.metro.client;

//  todo fixme
//@Slf4j
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = MetroClientTestApplication.class)
public class CitizenGrpcClientTest {
    //
//    @Autowired
//    private CitizenGrpcClient citizenGrpcClient;
//    @Autowired
//    private CastellanCommandLogic castellanCommandLogic;
//    @Autowired
//    private JwtUtil jwtUtil;
//    @Autowired
//    private TokenCreator tokenCreator;
//
//    private static RegisterCitizenCommand registerCitizenCommand;
//    private static boolean initialized;
//    private static String metroId;
//    private static String managerCastellanId;
//    private static String citizenId;
//    private static String email;
//    private static List<String> citizenIds;
//
//    private void setCurrentDramaRequest() {
//        //
//        String audienceId = TokenCreator.getWorkspaceList().getCineroomWorkspaces().get(0).getTenantId();
//        String token = tokenCreator.createToken();
//        DramaRequest dramaRequest = jwtUtil.createDramaRequest(audienceId, token);
//        log.debug("dramaRequest {}", dramaRequest.toPrettyJson());
//        StageContext.setCurrentRequest(dramaRequest);
//    }
//
//    @Before
//    public void before() {
//        //
//        log.debug("\n## initialized {}", initialized);
//        if (initialized) {
//            return;
//        }
//        setCurrentDramaRequest();
//        citizenIds = new ArrayList<>();
//
//        CommandStringResponse castellanResponse = castellanCommandLogic.registerManagerCastellan(
//                RegisterManagerCastellanCommand.sample(6));
//        managerCastellanId = castellanResponse.getResult();
//
//        registerCitizenCommand = RegisterCitizenCommand.sample();
//
////        registerCitizenCommand.setCastellanId(managerCastellanId);
//
//        CommandStringResponse commandStringResponse =
//                citizenGrpcClient.registerCitizen(registerCitizenCommand);
//        citizenId = commandStringResponse.getResult();
//
//        CitizenIdentityResponse response = citizenGrpcClient.findCitizenIdentity(
//                new FindCitizenIdentityQuery(citizenId));
//        email = response.getIdentity().getEmail();
//        metroId = response.getIdentity().getMetroId();
//        citizenIds.add(citizenId);
//
//        log.debug("\n## citizenIds {} ##", citizenIds);
//
//        log.debug("\n## in before() register manager castellan {}", commandStringResponse.toPrettyJson());
//        initialized = true;
//        log.debug("\n## initialized {}", initialized);
//    }
//
//    @Test
//    public void registerCitizenTest() {
//        //
//        RegisterCitizenCommand command = RegisterCitizenCommand.sample(2, false);
//        final CommandStringResponse res = citizenGrpcClient.registerCitizen(command);
//
//        log.debug("\n## CommandStringResponse in registerCitizenTest() {}", res.toPrettyJson());
//        assertNotNull(res);
//        assertFalse(res.getResult().isEmpty());
//
//        citizenIds.add(res.getResult());
//    }
//
//
//    @Test
//    public void findCitizenIdentityTest() {
//        //
//        CitizenIdentityResponse response = citizenGrpcClient.findCitizenIdentity(
//                new FindCitizenIdentityQuery(citizenId));
//        log.debug("\n## FindCitizenIdentityResponse() {}", response.toPrettyJson());
//
//        assertEquals(response.getIdentity().getId(), citizenId);
//    }
//
//    @Test
//    public void findCitizenIdentityByMetroIdAndEmailTest() {
//        //
//        CitizenIdentityResponse response = citizenGrpcClient.findCitizenIdentityByMetroIdAndEmail(
//                new FindCitizenIdentityByMetroIdAndEmailQuery(metroId, email));
//        log.debug("\n## FindCitizenIdentityResponse() {}", response.toPrettyJson());
//
//        assertEquals(response.getIdentity().getId(), citizenId);
//    }
//
//    @Test
//    public void findCitizenIdentitiesByIdsTest() {
//        //
//        CitizenIdentitiesResponse response = citizenGrpcClient.findCitizenIdentitiesByIds(
//                new FindCitizenIdentitiesByIdsQuery(citizenIds));
//        log.debug("\n## FindCitizenIdentitiesByIdsResponse() {}", response.toPrettyJson());
//
//        assertTrue(response.getIdentities().size()>0);
//
//        boolean found = false;
//        for(CitizenIdentity identity : response.getIdentities()) {
//            if( identity.getId().equals(citizenId ) ) {
//                found = true;
//            }
//        }
//        assertTrue(found);
//    }
}
