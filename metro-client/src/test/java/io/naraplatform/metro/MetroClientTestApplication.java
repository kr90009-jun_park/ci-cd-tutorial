package io.naraplatform.metro;

import io.naraplatform.daysman.client.config.EnableDaysman;
import io.naraplatform.envoy.config.NaraDramaApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@NaraDramaApplication
//@SpringBootApplication(scanBasePackages = {
//        "io.naraplatform.metro",
//        "io.naraplatform.checkpoint.client.local",
//        "io.naraplatform.station.client.local",
//        "io.naradrama.pigeon.client.local"
//})
@EnableDaysman
public class MetroClientTestApplication {
    //
    public static void main(String[] args) {
        //
        SpringApplication.run(MetroClientTestApplication.class, args);
    }
}
