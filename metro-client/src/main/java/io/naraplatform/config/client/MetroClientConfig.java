package io.naraplatform.config.client;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan(basePackages = {
        "io.naraplatform.metro.client"
})
@Profile("!default")
public class MetroClientConfig {
    //
    public final static String TARGET_SERVICE_ID = "io.naraplatform.metro";
}
