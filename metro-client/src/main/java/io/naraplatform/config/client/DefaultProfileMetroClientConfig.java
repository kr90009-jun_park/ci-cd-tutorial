package io.naraplatform.config.client;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan(basePackages = {
        "io.naraplatform.metro.client.local"
})
@Profile("default")
public class DefaultProfileMetroClientConfig {
}
