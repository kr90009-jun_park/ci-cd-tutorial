package io.naraplatform.metro.client.local;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naraplatform.metro.client.CitizenGrpcClient;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterPavilionManagerCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.entity.CitizenIdentity;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentitiesByIdsQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityByMetroIdAndEmailQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityQuery;
import io.naraplatform.metro.domain.facade.client.CitizenClient;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
@ConditionalOnMissingBean(CitizenGrpcClient.class)
public class DefaultProfileCitizenClient implements CitizenClient {
    //
    @Override
    public RegisterCitizenCommand registerCitizen(RegisterCitizenCommand command,
                                                  CommandIdentity commandIdentity) {
        //
        log.debug("RegisterActiveCitizenCommand : {}", command.toPrettyJson());
        command.setCommandResponse(new CommandResponse(CitizenIdentity.sample().getId()));
        return command;
    }

    @Override
    public RegisterPavilionManagerCitizenCommand registerPavilionManagerCitizen(RegisterPavilionManagerCitizenCommand command, CommandIdentity callerCommandIdentity) {
        //
        command.setCommandResponse(new CommandResponse(CitizenIdentity.sample().getId()));
        return command;
    }

//    @Override
//    public String registerPreliminaryCitizen(RegisterPreliminaryCitizenCommand command) {
//        //
//        log.debug("RegisterPreliminaryCitizenCommand : {}", command.toPrettyJson());
//        return new CommandStringResponse(CitizenIdentity.sample().getId());
//    }
//
//    @Override
//    public String setupPreliminaryCitizen(SetupPreliminaryCitizenCommand command) {
//        //
//        log.debug("SetupPreliminaryCitizenCommand : {}", command.toPrettyJson());
//        return new CommandStringResponse(CitizenIdentity.sample().getId());
//    }

    @Override
    public FindCitizenIdentityQuery findCitizenIdentity(FindCitizenIdentityQuery query,
                                                        CommandIdentity commandIdentity) {
        //
        log.debug("FindCitizenIdentityQuery : {}", query.toPrettyJson());
        query.setQueryResult(CitizenIdentity.sample());
        return query;
    }

    @Override
    public FindCitizenIdentityByMetroIdAndEmailQuery findCitizenIdentityByMetroIdAndEmail(FindCitizenIdentityByMetroIdAndEmailQuery query,
                                                                                          CommandIdentity commandIdentity) {
        //
        log.debug("FindCitizenIdentityByMetroIdAndEmailQuery : {}", query.toPrettyJson());
        query.setQueryResult(CitizenIdentity.sample());
        return query;
    }

    @Override
    public FindCitizenIdentitiesByIdsQuery findCitizenIdentitiesByIds(FindCitizenIdentitiesByIdsQuery query,
                                                                      CommandIdentity commandIdentity) {
        //
        log.debug("FindCitizenIdentitiesByIdsQuery : {}", query.toPrettyJson());
        query.setQueryResult(Arrays.asList(CitizenIdentity.sample()));
        return query;
    }
}
