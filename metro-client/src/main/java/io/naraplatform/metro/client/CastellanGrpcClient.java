package io.naraplatform.metro.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naraplatform.config.client.MetroClientConfig;
import io.naraplatform.envoy.client.support.GrpcMetadataDecorator;
import io.naraplatform.envoy.helper.CqrsRequesterManager;
import io.naraplatform.metro.client.channel.MetroGrpcChannelSupplier;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailQuery;
import io.naraplatform.metro.domain.facade.client.CastellanClient;
import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
import io.naraplatform.metro.proto.command.CastellanCommandProtoGrpc;
import io.naraplatform.metro.proto.command.RegisterManagerCastellanCommandMsg;
import io.naraplatform.metro.proto.query.CastellanQueryProtoGrpc;
import io.naraplatform.metro.proto.query.FindCastellanEmailByEmailQueryMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CastellanGrpcClient implements CastellanClient {
    //
    @Autowired
    private GrpcMetadataDecorator grpcMetadataDecorator;

    private final MetroGrpcChannelSupplier metroGrpcChannelSupplier;

    public CastellanGrpcClient(MetroGrpcChannelSupplier metroGrpcChannelSupplier) {
        //
        this.metroGrpcChannelSupplier = metroGrpcChannelSupplier;
    }


    @Override
    public RegisterManagerCastellanCommand registerManagerCastellan(RegisterManagerCastellanCommand command, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, command, MetroClientConfig.TARGET_SERVICE_ID);
        RegisterManagerCastellanCommandMsg msg = MetroGrpcMessage.toMessage(command);

        msg = getCommandStub().registerManagerCastellan(msg);

        return MetroGrpcMessage.toCommand(msg);
    }

    @Override
    public RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellan(RegisterPavilionManagerCastellanCommand command, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, command, MetroClientConfig.TARGET_SERVICE_ID);
        throw new RuntimeException("Unimplemented registerPavilionManagerCastellan");
    }

    @Override
    public FindCastellanEmailByEmailQuery findCastellanEmailByEmail(FindCastellanEmailByEmailQuery query, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, query, MetroClientConfig.TARGET_SERVICE_ID);
        FindCastellanEmailByEmailQueryMsg msg =  MetroGrpcMessage.toMessage(query);

        msg = getQueryStub().findCastellanEmailByEmail(msg);

        return MetroGrpcMessage.fromMessage(msg);
    }

    private CastellanCommandProtoGrpc.CastellanCommandProtoBlockingStub getCommandStub() {
        //
        return grpcMetadataDecorator.decorate(buildCommandStub());
    }

    private CastellanQueryProtoGrpc.CastellanQueryProtoBlockingStub getQueryStub() {
        //
        return grpcMetadataDecorator.decorate(buildQueryStub());
    }

    private CastellanCommandProtoGrpc.CastellanCommandProtoBlockingStub buildCommandStub() {
        //
        return CastellanCommandProtoGrpc.newBlockingStub(
                metroGrpcChannelSupplier.makeOrGetCommandChannel()
        );
    }

    private CastellanQueryProtoGrpc.CastellanQueryProtoBlockingStub buildQueryStub() {
        //
        return CastellanQueryProtoGrpc.newBlockingStub(
                metroGrpcChannelSupplier.makeOrGetQueryChannel());
    }

}
