package io.naraplatform.metro.client.local;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naraplatform.metro.client.NaraGrpcClient;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.metro.domain.aggregate.nara.entity.Nara;
import io.naraplatform.metro.domain.facade.client.NaraClient;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@ConditionalOnMissingBean(NaraGrpcClient.class)
public class DefaultProfileNaraClient implements NaraClient {
    //
    @Override
    public RegisterNaraCommand registerNara(RegisterNaraCommand command, CommandIdentity commandIdentity) {
        //
        log.debug("RegisterNaraCommand : {}", command.toPrettyJson());
        command.setCommandResponse(new CommandResponse(Nara.sample().getId()));
        return command;
    }
}
