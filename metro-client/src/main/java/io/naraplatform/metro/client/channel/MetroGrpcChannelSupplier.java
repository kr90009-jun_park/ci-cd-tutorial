package io.naraplatform.metro.client.channel;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@Getter
@Setter
public class MetroGrpcChannelSupplier {
    //
    @Value("${nara.metro.command.hostname:metro}")
    private String commandHostname;
    @Value("${nara.metro.command.port:8090}")
    private int commandPort;
    @Value("${nara.metro.query.hostname:metro}")
    private String queryHostname;
    @Value("${nara.metro.query.port:8090}")
    private int queryPort;

    private boolean isCommandAndQueryUseSameBoot = false;

    private ManagedChannel commandChannel;
    private ManagedChannel queryChannel;

    @PostConstruct
    private void checkConnection() {
        //
        if (commandHostname.equals(queryHostname) && commandPort == queryPort) {
            isCommandAndQueryUseSameBoot = true;
        }
    }

    @PreDestroy
    public void shutdownChannel() {
        //
        if (commandChannel != null || !commandChannel.isShutdown()) {
            try {
                log.debug("before query channel shutdown");
                commandChannel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
                log.debug("after query channel shutdown");
            } catch (InterruptedException e) {
                log.error("exception during query channel shutdown ", e);
                commandChannel.shutdownNow();
            }
        }

        if (isCommandAndQueryUseSameBoot) {
            return;
        }

        if (queryChannel != null || !queryChannel.isShutdown()) {
            try {
                log.debug("before query channel shutdown");
                queryChannel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
                log.debug("after query channel shutdown");
            } catch (InterruptedException e) {
                log.error("exception during query channel shutdown ", e);
                queryChannel.shutdownNow();
            }
        }

    }

    public ManagedChannel makeOrGetCommandChannel() {
        //
        if (commandChannel == null || commandChannel.isTerminated() || commandChannel.isShutdown())
            commandChannel = ManagedChannelBuilder.forAddress(commandHostname, commandPort)
                    .usePlaintext()
                    .build();

        if (isCommandAndQueryUseSameBoot && queryChannel == null)
            queryChannel = commandChannel;

        return commandChannel;
    }

    public ManagedChannel makeOrGetQueryChannel() {
        //
        if (queryChannel == null || queryChannel.isTerminated() || queryChannel.isShutdown())
            queryChannel = ManagedChannelBuilder.forAddress(queryHostname, queryPort)
                    .usePlaintext()
                    .build();

        if (isCommandAndQueryUseSameBoot && commandChannel == null)
            commandChannel = queryChannel;

        return queryChannel;
    }
}
