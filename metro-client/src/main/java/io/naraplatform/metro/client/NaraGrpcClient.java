package io.naraplatform.metro.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naraplatform.config.client.MetroClientConfig;
import io.naraplatform.envoy.client.support.GrpcMetadataDecorator;
import io.naraplatform.envoy.helper.CqrsRequesterManager;
import io.naraplatform.metro.client.channel.MetroGrpcChannelSupplier;
import io.naraplatform.metro.domain.aggregate.nara.command.RegisterNaraCommand;
import io.naraplatform.metro.domain.facade.client.NaraClient;
import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
import io.naraplatform.metro.proto.command.NaraCommandProtoGrpc;
import io.naraplatform.metro.proto.command.RegisterNaraCommandMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NaraGrpcClient implements NaraClient {
    //
    @Autowired
    private GrpcMetadataDecorator grpcMetadataDecorator;

    private final MetroGrpcChannelSupplier metroGrpcChannelSupplier;

    public NaraGrpcClient(MetroGrpcChannelSupplier metroGrpcChannelSupplier) {
        //
        this.metroGrpcChannelSupplier = metroGrpcChannelSupplier;
    }

    @Override
    public RegisterNaraCommand registerNara(RegisterNaraCommand command, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, command, MetroClientConfig.TARGET_SERVICE_ID);
        RegisterNaraCommandMsg msg = MetroGrpcMessage.toMessage(command);

        msg= getCommandStub().registerNara(msg);

        return MetroGrpcMessage.toCommand(msg);
    }

    private NaraCommandProtoGrpc.NaraCommandProtoBlockingStub getCommandStub() {
        //
        return grpcMetadataDecorator.decorate(buildCommandStub());
    }

    private NaraCommandProtoGrpc.NaraCommandProtoBlockingStub buildCommandStub() {
        //
        return NaraCommandProtoGrpc.newBlockingStub(
                metroGrpcChannelSupplier.makeOrGetCommandChannel());
    }
}
