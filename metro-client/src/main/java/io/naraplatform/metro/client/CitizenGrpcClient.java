package io.naraplatform.metro.client;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naraplatform.config.client.MetroClientConfig;
import io.naraplatform.envoy.client.support.GrpcMetadataDecorator;
import io.naraplatform.envoy.helper.CqrsRequesterManager;
import io.naraplatform.metro.client.channel.MetroGrpcChannelSupplier;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.command.RegisterPavilionManagerCitizenCommand;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentitiesByIdsQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityByMetroIdAndEmailQuery;
import io.naraplatform.metro.domain.aggregate.citizen.query.FindCitizenIdentityQuery;
import io.naraplatform.metro.domain.facade.client.CitizenClient;
import io.naraplatform.metro.grpc.util.MetroGrpcMessage;
import io.naraplatform.metro.proto.command.CitizenCommandProtoGrpc;
import io.naraplatform.metro.proto.command.RegisterCitizenCommandMsg;
import io.naraplatform.metro.proto.command.RegisterPavilionManagerCitizenCommandMsg;
import io.naraplatform.metro.proto.query.CitizenQueryProtoGrpc;
import io.naraplatform.metro.proto.query.FindCitizenIdentitiesByIdsQueryMsg;
import io.naraplatform.metro.proto.query.FindCitizenIdentityByMetroIdAndEmailQueryMsg;
import io.naraplatform.metro.proto.query.FindCitizenIdentityQueryMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CitizenGrpcClient implements CitizenClient {
    //
    @Autowired
    private GrpcMetadataDecorator grpcMetadataDecorator;

    private final MetroGrpcChannelSupplier metroGrpcChannelSupplier;

    public CitizenGrpcClient(MetroGrpcChannelSupplier metroGrpcChannelSupplier) {
        //
        this.metroGrpcChannelSupplier = metroGrpcChannelSupplier;
    }

    @Override
    public RegisterCitizenCommand registerCitizen(RegisterCitizenCommand command, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, command, MetroClientConfig.TARGET_SERVICE_ID);
        RegisterCitizenCommandMsg msg =  MetroGrpcMessage.toMessage(command);

        msg = getCommandStub().registerCitizen(msg);

        return MetroGrpcMessage.toCommand(msg);
    }

    @Override
    public RegisterPavilionManagerCitizenCommand registerPavilionManagerCitizen(RegisterPavilionManagerCitizenCommand command, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, command, MetroClientConfig.TARGET_SERVICE_ID);
        RegisterPavilionManagerCitizenCommandMsg msg =  MetroGrpcMessage.toMessage(command);

        msg = getCommandStub().registerPavilionManagerCitizen(msg);

        return MetroGrpcMessage.toCommand(msg);
    }

    @Override
    public FindCitizenIdentityQuery findCitizenIdentity(FindCitizenIdentityQuery query, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, query, MetroClientConfig.TARGET_SERVICE_ID);
        FindCitizenIdentityQueryMsg msg =  MetroGrpcMessage.toMessage(query);

        msg = getQueryStub().findCitizenIdentity(msg);

        return MetroGrpcMessage.fromMessage(msg);
    }

    @Override
    public FindCitizenIdentityByMetroIdAndEmailQuery findCitizenIdentityByMetroIdAndEmail(
            FindCitizenIdentityByMetroIdAndEmailQuery query, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, query, MetroClientConfig.TARGET_SERVICE_ID);
        FindCitizenIdentityByMetroIdAndEmailQueryMsg msg =  MetroGrpcMessage.toMessage(query);

        msg = getQueryStub().findCitizenIdentityByMetroIdAndEmail(msg);

        return MetroGrpcMessage.fromMessage(msg);
    }

    @Override
    public FindCitizenIdentitiesByIdsQuery findCitizenIdentitiesByIds(FindCitizenIdentitiesByIdsQuery query, CommandIdentity callerCommandIdentity) {
        //
        CqrsRequesterManager.setRequester(callerCommandIdentity, query, MetroClientConfig.TARGET_SERVICE_ID);
        FindCitizenIdentitiesByIdsQueryMsg msg =  MetroGrpcMessage.toMessage(query);

        msg = getQueryStub().findCitizenIdentitiesByIds(msg);

        return  MetroGrpcMessage.fromMessage(msg);
    }

    private CitizenCommandProtoGrpc.CitizenCommandProtoBlockingStub getCommandStub() {
        //
        return grpcMetadataDecorator.decorate(buildCommandStub());
    }

    private CitizenQueryProtoGrpc.CitizenQueryProtoBlockingStub getQueryStub() {
        //
        return grpcMetadataDecorator.decorate(buildQueryStub());
    }

    private CitizenCommandProtoGrpc.CitizenCommandProtoBlockingStub buildCommandStub() {
        //
        return CitizenCommandProtoGrpc.newBlockingStub(metroGrpcChannelSupplier.makeOrGetCommandChannel());
    }

    private CitizenQueryProtoGrpc.CitizenQueryProtoBlockingStub buildQueryStub() {
        //
        return CitizenQueryProtoGrpc.newBlockingStub(metroGrpcChannelSupplier.makeOrGetQueryChannel());
    }
}
