package io.naraplatform.metro.client.local;

import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naraplatform.metro.client.CastellanGrpcClient;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.command.RegisterPavilionManagerCastellanCommand;
import io.naraplatform.metro.domain.aggregate.castellan.entity.Castellan;
import io.naraplatform.metro.domain.aggregate.castellan.query.FindCastellanEmailByEmailQuery;
import io.naraplatform.metro.domain.aggregate.castellan.querymodel.CastellanEmail;
import io.naraplatform.metro.domain.facade.client.CastellanClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@ConditionalOnMissingBean(CastellanGrpcClient.class)
public class DefaultProfileCastellanClient implements CastellanClient {
    //
    @Override
    public RegisterManagerCastellanCommand registerManagerCastellan(RegisterManagerCastellanCommand command,
                                                                    CommandIdentity commandIdentity) {
        //
        log.debug("RegisterManagerCastellanCommand : {}", command.toPrettyJson());
        command.setCommandResponse(new CommandResponse(Castellan.sample().getId()));
        return command;
    }

    @Override
    public RegisterPavilionManagerCastellanCommand registerPavilionManagerCastellan(RegisterPavilionManagerCastellanCommand command,
                                                                                    CommandIdentity commandIdentity) {
        //
        log.debug("RegisterPavilionManagerCastellanCommand : {}", command.toPrettyJson());
        command.setCommandResponse(new CommandResponse(Castellan.sample().getId()));
        return command;
    }

    @Override
    public FindCastellanEmailByEmailQuery findCastellanEmailByEmail(FindCastellanEmailByEmailQuery query,
                                                                    CommandIdentity commandIdentity) {
        //
        query.setQueryResult(CastellanEmail.sample());
        return query;
    }
}
